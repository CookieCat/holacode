import {setupUnhandledLogger} from './src/22_unhandledLogger'; // t = n(22),
import {checkSwVersionUpdate, eventHandlerWithContext} from './src/67_initServiceWorker'; // e = n(67);

setupUnhandledLogger();

self.addEventListener('install', function(x) {
 return self.skipWaiting()
});

self.addEventListener('message', eventHandlerWithContext('message'));
self.addEventListener('activate', eventHandlerWithContext('activate'));
self.addEventListener('push', eventHandlerWithContext('push'));
self.addEventListener('notificationclick', eventHandlerWithContext('notificationclick'));
self.addEventListener('notificationclose', eventHandlerWithContext('notificationclose'));
self.addEventListener('pushsubscriptionchange', eventHandlerWithContext('pushsubscriptionchange'));
self.addEventListener('push', checkSwVersionUpdate);
