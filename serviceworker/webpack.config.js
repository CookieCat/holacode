const path = require('path');

module.exports = {
  mode: 'production',
  entry: './serviceworker.js',
  output: {
    filename: 'serviceworker.bundle.js',
    libraryTarget: 'var'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader'
      }
    ]
  }
}
