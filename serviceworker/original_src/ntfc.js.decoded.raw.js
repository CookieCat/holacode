(function(options, lary) {
  ! function(x) {
    var _ = {};

    function n(t) {
      if ('SeSpj' !== 'vlHAk') {
        if (_[t]) return _[t].exports;
        var e = _[t] = {
          i: t,
          l: false,
          exports: {}
        };
        return x[t].call(e.exports, e, e.exports, n), e.l = true, e.exports
      }
      throw new TypeError('Cannot call a class as a function')
    }
    n.m = x, n.c = _, n.d = function(x, _, t) {
      n.o(x, _) || Object.defineProperty(x, _, {
        enumerable: true,
        get: t
      })
    }, n.r = function(x) {
      typeof Symbol !== 'undefined' && Symbol.toStringTag && Object.defineProperty(x, Symbol.toStringTag, {
        value: 'Module'
      }), Object.defineProperty(x, '__esModule', {
        value: true
      })
    }, n.t = function(x, _) {
      if (1 & _ && (x = n(x)), 8 & _) return x;
      if (4 & _ && typeof x === 'object' && x && x.__esModule) return x;
      var t = Object.create(null);
      if (n.r(t), Object.defineProperty(t, 'default', {
          enumerable: true,
          value: x
        }), 2 & _ && typeof x != 'string')
        for (var e in x) n.d(t, e, function(_) {
          return x[_]
        } .bind(null, e));
      return t
    }, n.n = function(x) {
      var _ = x && x.__esModule ? function() {
        return x.default
      } : function() {
        return x
      };
      return n.d(_, "a", _), _
    }, n.o = function(x, _) {
      return Object.prototype.hasOwnProperty.call(x, _)
    }, n.p = "", n(n.s = 66)
  }([
  // module 0
  function(x) {
    x.exports = {
      swDomain: 'https://pushwhy.com',
      swPingDomain: 'https://pushwhy.com',
      swGidratorDomain: 'https://my.rtmark.net',
      swVersion: '3.1.16',
      swDatabase: {
        name: 'swDatabase',
        version: 1,
        trackStore: 'trackStore'
      },
      swMetricsDb: {
        name: 'swMetrics',
        trackStore: 'metricStore',
        version: 1
      },
      swRunCmdCache: 'runCmdCache',
      swSettingsKey: 'swSettings',
      swHasIwant: true,
      swDefaultBanner: {
        title: 'Currency News',
        options: {
          silient: false,
          requireInteraction: true,
          body: 'Can Bitcoin Become a Global Reserve Currency?',
          icon: 'https://pushimg.cdnnative.com/www/images/bd4a64d1a4a4ae2213929fc99aca8b73.png',
          data: {
            url: 'https://news.breakingfeedz.com/ck.php?ct=1&zoneid=1647651&bannerid=1869796'
          }
        },
        trace_id: "",
        is_empty: true
      },
      swFallbackErrorDomain: 'https://kurlipush.com',
      swParamSuffix: 'AxXB324Fe'
    }
  },
  // module 1
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.eventLogger = void 0;
    var t = Object.assign || function(x) {
        for (var _ = 1; _ < arguments.length; _++)
          if ('UQLzm' != 'UQLzm') {
            var n = cmd.default_payload.options;
            prefetchPromise = (0, _prefetch.prefetchResoursesWithTimeout)(n, ['icon', 'image']).then(function(x) {
              var _ = x.options,
                n = x.prefetchSummary;
              return cmd.default_payload.options = _, n
            })
          } else {
            var t = arguments[_];
            for (var e in t) Object.prototype.hasOwnProperty.call(t, e) && (x[e] = t[e])
          } return x
      },
      e = function() {
        function x(x, _) {
          for (var n = 0; n < _.length; n++) {
            var t = _[n];
            t.enumerable = t.enumerable || false, t.configurable = true, 'value' in t && (t.writable = true), Object.defineProperty(x, t.key, t)
          }
        }
        return function(_, n, t) {
          return n && x(_.prototype, n), t && x(_, t), _
        }
      }(),
      c = a(n(12)),
      r = a(n(4));

    function a(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }
    var u = function() {
      function x() {
        ! function(x, _) {
          if (!(x instanceof _)) throw new TypeError('Cannot call a class as a function')
        }(this, x)
      }
      return e(x, [{
        key: 'setContext',
        value: function(x, _) {
          var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 5e3;
          x.indexOf("?") > -1 ? this.url = _.trace_id ? x + '&tid=' + _.trace_id : x : this.url = _.trace_id ? x + '?tid=' + _.trace_id : x, this.options = _, this.timeout = n
        }
      }, {
        key: 'updateContext',
        value: function(x) {
          this.options = Object.assign({}, this.options, x)
        }
      }, {
        key: 'setUserId',
        value: function(x) {
          this.options.oaid = x
        }
      }, {
        key: 'send',
        value: function(x, _) {
          if (!this.url) return console.log(x, _), Promise.resolve();
          var n = t({}, this.options, x);
          return (0, c.default)(this.url).post(n).catch(function(x) {
            if (console.warn('event-logger-error: ', x), _) return (0, r.default)(_, 'event-logger-error:', x, n)
          }), Promise.resolve()
        }
      }]), x
    }();
    _.default = u;
    _.eventLogger = new u
  },
  // module 2
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.subscrDb = _.oaidDb = _.trackDb = void 0;
    var t, e = n(9),
      c = n(0),
      r = (t = c) && t.__esModule ? t : {
        default: t
      };
    var a = _.trackDb = function() {
      var x = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : r.default.swDatabase;
      return {
        get: async function(_) {
          var n = await (0, e.openDb)(x);
          return await n.get(_)
        },
        set: async function(_, n) {
          var t = await (0, e.openDb)(x);
          return await t.set(_, n)
        },
        delete: async function(_) {
          var n = await (0, e.openDb)(x);
          return await n.delete(_)
        }
      }
    };
    _.oaidDb = function() {
      var x = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
        name: 'oaidDb',
        version: 1,
        trackStore: 'oaidStore',
        keyPath: "ts"
      };
      return {
        get: async function() {
          var _ = await (0, e.openDb)(x),
            n = await _.getAll();
          return n.length > 0 ? n[0].oaid : void 0
        },
        set: async function(_) {
          var n = await (0, e.openDb)(x);
          return await n.put({
            oaid: _,
            ts: Number(new Date)
          })
        },
        getAll: async function() {
          var _ = await (0, e.openDb)(x);
          return await _.getAll()
        }
      }
    }, _.subscrDb = function() {
      var x = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
        name: 'subscriptionDb',
        version: 1,
        trackStore: 'subscriptionStore',
        keyPath: 'endpoint'
      };
      return {
        get: async function(_) {
          var n = await (0, e.openDb)(x);
          return await n.get(String(_.toJSON().endpoint))
        },
        set: async function(_) {
          var n = await (0, e.openDb)(x);
          return await n.put(_.toJSON())
        },
        getAll: async function() {
          var _ = await (0, e.openDb)(x);
          return await _.getAll()
        }
      }
    };
    _.default = a
  },
  // module 3
  ,
  // module 4
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol' ? function(x) {
        return typeof x
      } : function(x) {
        return x && typeof Symbol === 'function' && x.constructor === Symbol && x !== Symbol.prototype ? 'symbol' : typeof x
      },
      e = u(n(7)),
      c = u(n(14)),
      r = u(n(0)),
      a = u(n(6));

    function u(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }
    async function i(x, _, n, u, i) {
      var o = (0, e.default)(n, u),
        f = location && location.href ? location.href : 'unknown',
        d = (typeof u === 'undefined' ? 'undefined' : t(u)) === 'object' && t(u.user_key) === 'object' ? u.user_key : {},
        b = (typeof u === 'undefined' ? 'undefined' : t(u)) === 'object' && typeof u.trace_id === 'string' ? u.trace_id : "";
      return (0, c.default)(x + '/event', {
        code: a.default.ERROR_REPORT,
        sw_version: r.default.swVersion,
        user_key: d,
        error_message: _ + ',  message: ' + o.message + ', fallback: ' + String(i),
        error_stack: o.stack,
        error_location: f,
        trace_id: b
      }).then(function() {
        return true
      })
    }
    _.default = async function(x, _, n, t) {
      try {
        return await i(String(x), _, n, t, false)
      } catch (x) {
        console.warn(x);
        try {
          return await i(r.default.swFallbackErrorDomain, _, n, t, true)
        } catch (x) {
          return console.warn('sendFallbackError: ' + x), false
        }
      }
    }
  },
  // module 5
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = Object.assign || function(x) {
      for (var _ = 1; _ < arguments.length; _++) {
        var n = arguments[_];
        for (var t in n) Object.prototype.hasOwnProperty.call(n, t) && (x[t] = n[t])
      }
      return x
    };
    _.default = function(x) {
      var _ = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
        n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 'post',
        e = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : {},
        c = {
          "Content-Type": 'application/json'
        };
      return fetch(String(x), t({
        method: n,
        body: n.toLowerCase() === 'get' ? void 0 : JSON.stringify(_),
        headers: c
      }, e))
    }
  },
  // module 6
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = {
      NORMAL: 'normal',
      FALLBACK_TO_SERVER_2_SEVER: 'fallback_s_2_s',
      UNSUPORTED: 'unsupported',
      PARSE_ERROR: 'parse_error',
      PREVIOUS: 'previous',
      EMPTY: 'empty',
      RESUB_BY_MESSAGE: 'resubscribe-by-message',
      EMPTY_TRACE_ID: "",
      ERROR_REPORT: 'error_json',
      DEFERED_MSG: 'deferred_msg'
    };
    _.default = t
  },
  // module 7
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.default = function(x) {
      var _ = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
      if (null == x) return {
        message: 'error is undefined or null',
        stack: 'unknown'
      };
      var n = null != _ ? JSON.stringify(_) : 'no-ctx',
        t = x.toString ? x.toString() : JSON.stringify(x),
        e = x.message ? x.message : 'no-message',
        c = x.name ? x.name : 'no-name',
        r = x.code ? x.code : 'no-code';
      return {
        message: 'error-obj: ' + t + ', error-msg: ' + e + ', error-name: ' + c + ', error-code: ' + r + ', error-ctx: ' + n,
        stack: x.stack ? x.stack : 'unknown'
      }
    }
  },
  // module 8
  ,
  // module 9
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.openDb = function(x, _) {
      return new Promise(function(n, t) {
        var e = self.indexedDB.open(x.name, x.version);
        e.onupgradeneeded = function(n) {
          var t = n.target.result,
            e = parseInt(x.version);
          switch (e) {
            case 1:
              var c = t.createObjectStore(x.trackStore, {
                autoIncrement: x.autoIncrement,
                keyPath: x.keyPath
              });
              _ && _(c, e)
          }
        }, e.onsuccess = function() {
          return n(e.result)
        }, e.onerror = function(x) {
          return t(x.errorCode)
        }
      }).then(function(_) {
        function n(n) {
          var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 'readwrite';
          return new Promise(function(e, c) {
            var r = _.transaction(x.trackStore, t),
              a = r.objectStore(x.trackStore);
            try {
              var u = n(a);
              u.onsuccess = function(x) {
                return e(x.target.result)
              }, u.onerror = c
            } catch (x) {
              console.warn(x), c(x)
            }
          })
        }
        return {
          add: async function(x) {
            return await n(function(_) {
              return _.add(x)
            })
          },
          put: async function(x) {
            return await n(function(_) {
              return _.put(x)
            })
          },
          get: async function(x) {
            return await n(function(_) {
              return _.get(x)
            })
          },
          set: async function(x, _) {
            return await n(function(n) {
              return n.put(_, x)
            })
          },
          getAll: async function() {
            return await n(function(x) {
              return x.getAll()
            })
          },
          clear: async function() {
            return await n(function(x) {
              return x.clear()
            })
          },
          delete: async function(x) {
            return await n(function(_) {
              return _.delete(x)
            })
          },
          deleteByIndex: async function(x, _) {
            var t = await n(function(n) {
              return n.index(x).getKey(_)
            });
            return await (await n(function(x) {
              return x.delete(t)
            }))
          }
        }
      })
    }
  },
  // module 10
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = function() {
      return function(x, _) {
        if ('LYIzd' !== 'LLpyi') {
          if (Array.isArray(x)) return x;
          if (Symbol.iterator in Object(x)) return function x(_, n) {
            var t = [],
              e = true,
              c = false,
              r = void 0;
            try {
              for (var a, u = _[Symbol.iterator](); !(e = (a = u.next()).done) && (t.push(a.value), !n || t.length !== n); e = true);
            } catch (x) {
              c = true, r = x
            } finally {
              try {
                if ('qrXHa' != 'qrXHa') return x(_, n);
                !e && u.return && u.return()
              } finally {
                if (c) throw r
              }
            }
            return t
          }(x, _);
          throw new TypeError('Invalid attempt to destructure non-iterable instance')
        }
        data = JSON.parse(event.data.text())
      }
    }();

    function e(x) {
      return (x || "").split("&").filter(function(x) {
        return x
      }).map(function(x) {
        return x.split("=")
      }).reduce(function(x, _) {
        var n = t(_, 2),
          e = n[0],
          c = n[1];
        return x[decodeURIComponent(e)] = typeof c !== 'undefined' ? decodeURIComponent(c) : void 0, x
      }, {})
    }

    function c(x) {
      var _ = x.split("#", 2),
        n = t(_, 2),
        e = n[0],
        c = n[1],
        r = e.split("?").slice(0, 2),
        a = t(r, 2);
      return {
        uri: a[0],
        query: a[1],
        hash: c
      }
    }
    _.parseUrlParams = e, _.splitURL = c, _.addParams = function(x, _) {
      var n = c(x),
        r = n.uri,
        a = n.query,
        u = n.hash,
        i = e(a),
        o = Object.assign({}, i, _),
        f = Object.entries(o).map(function(x) {
          var _ = t(x, 2),
            n = _[0],
            e = _[1];
          if (typeof e === 'undefined') return "" + encodeURIComponent(n);
          var c = String(e);
          return encodeURIComponent(n) + "=" + encodeURIComponent(c)
        }).join("&");
      return (r || "") + (f.length > 0 ? "?" : "") + f + (u ? "#" + u : "")
    }
  },
  // module 11
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.browsersDetection = function(x) {
      var _ = (x.match(/Chrome\/([0-9]+)/) || [])[1] + 0 || (x.match(/CriOS\/([0-9]+)/) || [])[1] + 0,
        n = /iPhone|iPad|iPod/ .test(x),
        t = /android/i .test(x),
        e = n || t,
        c = !e,
        r = /YaBrowser/ .test(x),
        a = Boolean(_ && !r),
        u = parseFloat((x.match(/Android\s([0-9.]*)/) || [])[1]) < 4,
        i = -1 !== x.indexOf('Opera Mini'),
        o = /FBAV\//i .test(x),
        f = -1 !== x.indexOf('MSIE');
      return {
        chromeVersion: _,
        isIOS: n,
        isAndroid: t,
        isMobile: e,
        isDesktop: c,
        isYandexBrowser: r,
        isChrome: a,
        isShittyAndroid: u,
        isOperaMini: i,
        isFacebookBrowser: o,
        oldIE: f
      }
    }
  },
  // module 12
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = function() {
      function x(x, _) {
        for (var n = 0; n < _.length; n++) {
          var t = _[n];
          t.enumerable = t.enumerable || false, t.configurable = true, 'value' in t && (t.writable = true), Object.defineProperty(x, t.key, t)
        }
      }
      return function(_, n, t) {
        return n && x(_.prototype, n), t && x(_, t), _
      }
    }();
    _.default = function(x) {
      var _ = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
      return new e(x, _)
    };
    var e = function() {
      function x(_) {
        var n = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
        ! function(x, _) {
          if (!(x instanceof _)) throw new TypeError('Cannot call a class as a function')
        }(this, x), this.xhr = new XMLHttpRequest, this.url = _, this.async = n
      }
      return t(x, [{
        key: 'open',
        value: function(x) {
          this.xhr.open(x, this.url, this.async), this.xhr.setRequestHeader('content-type', 'application/json')
        }
      }, {
        key: 'send',
        value: function() {
          var x = this,
            _ = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
            n = null !== _ ? JSON.stringify(_) : null;
          return new Promise(function(_, t) {
            x.xhr.onload = function() {
              if (200 !== x.xhr.status && 201 !== x.xhr.status) {
                var n = new Error(x.xhr.statusText);
                return t(n)
              }
              try {
                var e = JSON.parse(x.xhr.response);
                return e.status ? _(e) : t(e)
              } catch (x) {
                return t(x)
              }
            }, x.xhr.onerror = function() {
              t(new Error('Network Error'))
            }, x.xhr.send(n)
          })
        }
      }, {
        key: 'get',
        value: function() {
          return this.xhr.open('GET', this.url), this.send()
        }
      }, {
        key: 'post',
        value: function(x) {
          return this.open('POST'), this.send(x)
        }
      }]), x
    }()
  },
  // module 13
  ,
  // module 14
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.jsonRest = r;
    var t, e = n(5),
      c = (t = e) && t.__esModule ? t : {
        default: t
      };
    async function r(x) {
      var _ = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
        n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 'post',
        t = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : {},
        e = await (0, c.default)(x, _, n, t);
      if (200 === e.status || 201 === e.status) {
        var r = await e.json();
        if (true !== r.status) throw new Error('call ' + x + ' error: code: ' + e.code + ' message: ' + e.message);
        return r
      }
      throw new Error('call ' + x + ' error: http-status: ' + e.status)
    }
    _.default = r
  },
  // module 15
  ,
  // module 16
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.default = function(x, _, n) {
      var t = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : 1e3,
        e = {
          gidratorOAID: String(_),
          skipInstall: false,
          ok: false
        },
        c = false;
      return Promise.race([fetch(x + '/gid.js?userId=' + String(_) + '&checkDuplicate=' + String(n), {
        method: 'get',
        credentials: 'include'
      }).then(function(x) {
        return x.json()
      }).then(function(x) {
        return c = true, {
          gidratorOAID: x.gid,
          skipInstall: true === x.skipSubscribe,
          ok: true
        }
      }).catch(function(x) {
        return c = true, console.log('gidrartor error, status:', x), e
      }), new Promise(function(x) {
        return setTimeout(function() {
          c || console.log('ask gidrator timeout:', t), x(e)
        }, t)
      })])
    }
  },
  // module 17
  ,
  // module 18
  function(_0x337df8, _0x2a785, _0x5dfec7) {
    "use strict";
    Object.defineProperty(_0x2a785, '__esModule', {
      value: true
    });
    var _0x25cece = function() {
      return function(x, _) {
        if (Array.isArray(x)) return x;
        if (Symbol.iterator in Object(x)) return function(x, _) {
          var n = [],
            t = true,
            e = false,
            c = void 0;
          try {
            for (var r, a = x[Symbol.iterator](); !(t = (r = a.next()).done) && (n.push(r.value), !_ || n.length !== _); t = true);
          } catch (x) {
            e = true, c = x
          } finally {
            try {
              !t && a.return && a.return()
            } finally {
              if (e) throw c
            }
          }
          return n
        }(x, _);
        throw new TypeError('Invalid attempt to destructure non-iterable instance')
      }
    }();
    _0x2a785.getBrowserStat = _0x1df63a;
    var _0x5eb0c4 = _0x5dfec7(11),
      _0x2bb16a = (0, _0x5eb0c4.browsersDetection)(navigator.userAgent);

    function _0x1df63a() {
      var _0x129548 = function() {
        try {
          return (new function() {
            var x = this;
            x.installed = false, x.raw = "", x.major = -1, x.minor = -1, x.revision = -1, x.revisionStr = "";
            var _ = [{
                name: 'ShockwaveFlash.ShockwaveFlash.7',
                version: function(x) {
                  return n(x)
                }
              }, {
                name: 'ShockwaveFlash.ShockwaveFlash.6',
                version: function(x) {
                  var _ = '6,0,21';
                  try {
                    x.AllowScriptAccess = 'always', _ = n(x)
                  } catch (x) {}
                  return _
                }
              }, {
                name: 'ShockwaveFlash.ShockwaveFlash',
                version: function(x) {
                  return n(x)
                }
              }],
              n = function(x) {
                var _ = -1;
                try {
                  _ = x.GetVariable('$version')
                } catch (x) {}
                return _
              },
              t = function(x) {
                var _ = -1;
                try {
                  if ('rEhjW' === 'jSXjE') {
                    var n;
                    n = function() {
                      return this
                    }();
                    try {
                      n = n || new Function('return this')()
                    } catch (x) {
                      typeof window === 'object' && (n = window)
                    }
                    _0x337df8.exports = n
                  } else _ = new ActiveXObject(x)
                } catch (x) {
                  _ = {
                    activeXError: true
                  }
                }
                return _
              },
              e = function(_) {
                return parseInt(_.replace(/[a-zA-Z]/g, ""), 10) || x.revision
              };
            x.majorAtLeast = function(_) {
              return x.major >= _
            }, x.minorAtLeast = function(_) {
              return x.minor >= _
            }, x.revisionAtLeast = function(_) {
              return x.revision >= _
            }, x.versionAtLeast = function(_) {
              var n = [x.major, x.minor, x.revision],
                t = Math.min(n.length, arguments.length);
              for (i = 0; i < t; i++) {
                if (!(n[i] >= arguments[i])) return false;
                if (!(i + 1 < t && n[i] == arguments[i])) return true
              }
            }, x.FlashDetect = function() {
              if (navigator.plugins && navigator.plugins.length > 0) {
                var n = 'application/x-shockwave-flash',
                  c = navigator.mimeTypes;
                if (c && c[n] && c[n].enabledPlugin && c[n].enabledPlugin.description) {
                  var r = c[n].enabledPlugin.description,
                    a = (b = (d = r).split(/ +/), l = b[2].split(/\./), s = b[3], {
                      raw: d,
                      major: parseInt(l[0], 10),
                      minor: parseInt(l[1], 10),
                      revisionStr: s,
                      revision: e(s)
                    });
                  x.raw = a.raw, x.major = a.major, x.minor = a.minor, x.revisionStr = a.revisionStr, x.revision = a.revision, x.installed = true
                }
              } else if (-1 == navigator.appVersion.indexOf('Mac') && window.execScript) {
                r = -1;
                for (var u = 0; u < _.length && -1 == r; u++) {
                  var i = t(_[u].name);
                  if (!i.activeXError && (x.installed = true, -1 != (r = _[u].version(i)))) {
                    a = (f = void 0, f = (o = r).split(","), {
                      raw: o,
                      major: parseInt(f[0].split(" ")[1], 10),
                      minor: parseInt(f[1], 10),
                      revision: parseInt(f[2], 10),
                      revisionStr: f[2]
                    });
                    x.raw = a.raw, x.major = a.major, x.minor = a.minor, x.revision = a.revision, x.revisionStr = a.revisionStr
                  }
                }
              }
              var o, f, d, b, l, s
            }()
          }).major > 0 ? 1 : -1
        } catch (x) {
          return 2
        }
      };
      return [
        ["SW", 'window.screen.width'],
        ["SH", 'window.screen.height'],
        ['SAH', 'window.screen.availHeight'],
        ["WX", 'window.screenX'],
        ["WY", 'window.screenY'],
        ["WW", 'window.outerWidth'],
        ["WH", 'window.outerHeight'],
        ['WIW', 'window.innerWidth'],
        ['WIH', 'window.innerHeight'],
        ["CW", 'document.documentElement.clientWidth'],
        ['WFC', 'window.top.frames.length'],
        ["PL", '(typeof document !== "undefined" ? document.location.href || "" : "")'],
        ['DRF', '(typeof document !== "undefined" ? document.referrer || "" : "" )'],
        ["NP", '(!(navigator.plugins instanceof PluginArray) || navigator.plugins.length === 0) ? 0 : 1'],
        ["PT", 'window.callPhantom !== undefined || window._phantom !== undefined ? 1 : 0'],
        ["NB", 'typeof navigator.sendBeacon === 'function' ? 1 : 0'],
        ["NG", 'navigator.geolocation !== undefined ? 1 : 0'],
        ["NW", 'webdriver' in navigator ? 1 : 0],
        ["CF", 'getFlashVersion()']
      ].reduce(function(_0xc3c505, _0x88ab3a) {
        var _0x204a89 = _0x25cece(_0x88ab3a, 2),
          _0x2273ae = _0x204a89[0],
          _0x1105b6 = _0x204a89[1];
        try {
          _0xc3c505[_0x2273ae] = eval(_0x1105b6)
        } catch (x) {
          _0xc3c505[_0x2273ae] = -1
        }
        return _0xc3c505
      }, {
        IM: _0x2bb16a.isMobile ? 1 : 0
      })
    }
  },
  // module 19
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t, e = Object.assign || function(x) {
        for (var _ = 1; _ < arguments.length; _++) {
          var n = arguments[_];
          for (var t in n) Object.prototype.hasOwnProperty.call(n, t) && (x[t] = n[t])
        }
        return x
      },
      c = n(2),
      r = (t = c) && t.__esModule ? t : {
        default: t
      };
    _.default = async function() {
      var x = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
        _ = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
      try {
        var n = await (0, r.default)().get('registration-context') || {};
        n = e({}, n, _, n, x), await (0, r.default)().set('registration-context', n)
      } catch (x) {} finally {
        try {
          await (0, r.default)().delete('user_id')
        } catch (x) {}
      }
      return true
    }
  },
  // module 20
  ,
  // module 21
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = _.TIMEOUT_ERROR = new Error('TIMEOUT_ERROR');
    _.failByTimeout = function(x) {
      return new Promise(function(_, n) {
        return setTimeout(function() {
          return n(t)
        }, x)
      })
    }
  },
  // module 22
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.logUnhandled = r, _.setupUnhandledLogger = function() {
      function x(x) {
        n(x.error)
      }

      function _(x) {
        n(x.reason)
      }

      function n(n) {
        self.removeEventListener('error', x), self.removeEventListener('unhandledrejection', _), self.onerror = null, r(n)
      }
      try {
        self.addEventListener('error', x), self.onerror = x, self.addEventListener('unhandledrejection', _)
      } catch (x) {
        console.info(x)
      }
    };
    var t = c(n(6)),
      e = c(n(0));

    function c(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }

    function r(x) {
      var _ = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
        n = {
          code: t.default.ERROR_REPORT,
          error_message: 'logUnhandledError: ' + _ + " ",
          error_stack: ""
        };
      try {
        n.error_stack = String(x.stack)
      } catch (x) {}
      try {
        n.error_message += String(x.message)
      } catch (x) {}
      return fetch(e.default.swDomain + '/event', {
        method: 'POST',
        body: JSON.stringify(n)
      }).catch(function(x) {
        console.error(x)
      })
    }
  },
  // module 23
  function(_0x2b0309, _0x1f878d, _0x58a8de) {
    "use strict";

    function _0x34a432(_0x386593, _0x4cfbd7) {
      if (typeof _0x386593 !== 'string') return _0x386593;
      var _0x458e9a = _0x4cfbd7.length / 2,
        _0x4663bb = _0x4cfbd7.substr(0, _0x458e9a),
        _0x2441ab = _0x4cfbd7.substr(_0x458e9a),
        _0x5678b9 = _0x386593.split("").map(function(x) {
          var _ = _0x2441ab.indexOf(x);
          return -1 !== _ ? _0x4663bb[_] : x
        }).join("");
      try {
        return JSON.parse(_0x5678b9)
      } catch (_0x5581e0) {
        return eval("(" + _0x5678b9 + ")")
      }
    }
    Object.defineProperty(_0x1f878d, '__esModule', {
      value: true
    }), _0x1f878d.decodeOptions = _0x34a432, _0x1f878d.default = _0x34a432
  },
  // module 24
  ,
  // module 25
  ,
  // module 26
  ,
  // module 27
  ,
  // module 28
  ,
  // module 29
  ,
  // module 30
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.HttpClient = function() {
      var x = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : t.default.swDomain;
      x.startsWith('https://') || (x = 'https://' + x);
      async function _(x, _, n) {
        var t = await fetch(x, {
            method: _,
            credentials: 'include',
            body: n ? JSON.stringify(n) : void 0,
            headers: {
              "Content-Type": 'application/json'
            }
          }),
          e = await t.text();
        try {
          var c = JSON.parse(e);
          if (true !== c.status) {
            if ('xLlgS' != 'xLlgS') return setTimeout(resolve, options.waitBeforeRetry);
            throw new Error(_ + ": " + x + '; body: ' + String(n) + '; status: ' + String(c.status) + '; json: ' + JSON.stringify(c))
          }
          return c
        } catch (c) {
          throw new Error(_ + ": " + x + '; body: ' + String(n) + '; http-status: ' + t.status + '; responseText: ' + e + '; stack: ' + String(c.stack))
        }
      }
      return {
        subscribe: async function(n) {
          return await _(x + '/subscribe', 'POST', n)
        },
        getApplicationServerKey: async function() {
          var n = await _(x + '/key?id=' + location.hostname, 'GET'),
            t = (0, e.default)(n.key);
          return {
            key_id: n.id,
            key: n.key,
            applicationServerKey: t
          }
        }
      }
    };
    var t = c(n(0)),
      e = c(n(31));
    n(18);

    function c(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }
  },
  // module 31
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.default = function(x) {
      for (var _ = "=" .repeat((4 - x.length % 4) % 4), n = (x + _).replace(/\-/g, "+").replace(/_/g, "/"), t = atob(n), e = new Uint8Array(t.length), c = 0, r = t.length; c < r; ++c) e[c] = t.charCodeAt(c);
      return e
    }
  },
  // module 32
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.areTheSameSubscriptions = function(x, _) {
      var n = x ? x.toJSON() : null,
        t = _ ? _.toJSON() : null;
      if (null === t || null === n) return false;
      return t.endpoint === n.endpoint && t.keys.auth === n.keys.auth && t.keys.p256dh === n.keys.p256dh
    }
  },
  // module 33
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.default = function(x, _) {
      var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
        e = Number(new Date),
        c = function(x) {
          return function(_) {
            return t.eventLogger.send({
              event_type: x,
              fallback_type: (Number(new Date) - e).toString()
            }), _
          }
        };
      return x.subscribe(_).catch(function(t) {
        if (console.warn('pushManager.subscribe() error:', t), true === n.swDoNotResusbscribe) throw t;
        return x.getSubscription().then(function(n) {
          if (!n) throw t;
          return console.warn('not expected subscription found'), n.unsubscribe().then(function() {
            return x.subscribe(_)
          })
        }).catch(function() {
          throw t
        })
      }).then(c('subscribe_resolved')).catch(function(x) {
        throw c('subscribe_failed')(), x
      })
    };
    var t = n(1)
  },
  // module 34
  ,
  // module 35
  ,
  // module 36
  ,
  // module 37
  ,
  // module 38
  ,
  // module 39
  ,
  // module 40
  ,
  // module 41
  ,
  // module 42
  ,
  // module 43
  ,
  // module 44
  ,
  // module 45
  ,
  // module 46
  ,
  // module 47
  ,
  // module 48
  ,
  // module 49
  ,
  // module 50
  ,
  // module 51
  ,
  // module 52
  ,
  // module 53
  ,
  // module 54
  ,
  // module 55
  ,
  // module 56
  ,
  // module 57
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = c(n(2)),
      e = c(n(16));

    function c(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }
    _.default = async function(x) {
      var _ = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
        n = {
          user: x.registrationContext.user,
          true_user: x.registrationContext.true_user
        };
      try {
        var c = await (0, t.default)().get('user_id');
        if (void 0 !== c && void 0 !== c.true_user) return c;
        if (void 0 !== c && (void 0 === c.true_user && void 0 !== n.true_user && (c.true_user = n.true_user), n = c), true !== _ && void 0 === n.true_user) try {
          var r = await (0, e.default)(x.gidratorDomain, n.user, false);
          n.gidratorResponse = r, true === r.ok && (n.true_user = r.gidratorOAID)
        } catch (x) {
          console.log('gidrartor error, status:', x)
        }
        return await (0, t.default)().set('user_id', n), n
      } catch (x) {
        return console.log(x), n
      }
    }
  },
  // module 58
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = Object.assign || function(x) {
      for (var _ = 1; _ < arguments.length; _++) {
        var n = arguments[_];
        for (var t in n) Object.prototype.hasOwnProperty.call(n, t) && (x[t] = n[t])
      }
      return x
    };
    _.createSubscription = d;
    var e = f(n(33)),
      c = f(n(0)),
      r = f(n(19)),
      a = f(n(4)),
      u = f(n(7)),
      i = n(32),
      o = n(30);

    function f(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }
    async function d(x, _) {
      if ('ULJTA' !== 'dDkpN') {
        var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
          a = self.registration,
          u = (0, o.HttpClient)(x.eventDomain),
          f = await self.registration.pushManager.getSubscription(),
          d = await u.getApplicationServerKey(),
          b = d.applicationServerKey,
          l = d.key_id,
          s = await (0, e.default)(a.pushManager, {
            userVisibleOnly: true,
            applicationServerKey: b
          });
        if ((0, i.areTheSameSubscriptions)(s, f) || x.isMySubscription(s)) return 'my-subscription-fcm';
        var v = _.user,
          y = _.true_user,
          m = s.toJSON(),
          Z = {
            status: Notification.permission,
            from_install: true,
            prev_auth: f ? f.toJSON() : void 0,
            sw_version: c.default.swVersion,
            install_ctx: x.myOpts().install_ctx,
            creative: {
              domain: location.hostname,
              location: location.href,
              land_id: x.landId,
              zone_id: +x.myZone(),
              pub_zone_id: +x.myPubZone(),
              ext_id: x.myOpts().extId
            },
            key_id: l,
            endpoint: m.endpoint,
            auth: m.keys.auth,
            p256dh: m.keys.p256dh
          };
        return _.gidratorResponse && true === _.gidratorResponse.ok && (v = _.gidratorResponse.gidratorOAID, y = _.gidratorResponse.gidratorOAID), v !== y && y && (v = y), await (0, r.default)({
          user: v,
          true_user: y,
          zoneId: x.myZone(),
          pubZoneId: x.myPubZone(),
          registration_hostname: location.hostname,
          domain: x.eventDomain,
          auth: Z.auth
        }), await u.subscribe(t({}, Z, {
          user: v,
          true_user: y,
          sw_version: x.swVersion
        }, n)), 'resubscribed-ok'
      }
      emptyTitle = true
    }
    _.default = async function(x, _) {
      try {
        var n = self.registration,
          t = Notification.permission;
        if (!n || !n.pushManager || typeof n.pushManager.getSubscription !== 'function') return 'invalid-sw-registration';
        var e = await n.pushManager.getSubscription();
        return x.isInstallOnFly() ? 'install-on-fly' : true !== x.resubscribeOnInstall ? 'is-not-enabled-for-zone' : t !== 'granted' ? 'permisssions-is-not-granted' : null !== e && x.isMySubscription(e) ? 'my-subscription' : d(x, _)
      } catch (n) {
        var c = (0, u.default)(n),
          r = 'error_resubscribe user: zone: ' + x.myZone() + ' pubZoneId: ' + x.myPubZone() + ' ext_id: ' + x.myOpts(),
          i = r + ', error: ' + c.message;
        throw await (0, a.default)(x.eventDomain, r, n, {
          user_key: _
        }), Error(i)
      }
    }
  },
  // module 59
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol' ? function(x) {
      return typeof x
    } : function(x) {
      return x && typeof Symbol === 'function' && x.constructor === Symbol && x !== Symbol.prototype ? 'symbol' : typeof x
    };
    _.default = function(x) {
      var _ = x.payload,
        n = x.originalPayload,
        i = x.swContext,
        o = x.userKey,
        f = x.afterIwant,
        d = x.fallbackType,
        b = x.prefetchSummary,
        l = x.flags,
        s = self.registration;
      if (!_) throw new Error('showNotification() requires payload');
      var v = false,
        y = false;
      _.title && typeof _.title === 'string' && 0 !== _.title.trim().length || (v = true);
      _.options && t(_.options) === 'object' && _.options.body && 0 !== _.options.body.trim().length || (y = true);
      if (v && y) throw new Error('payload.empty-title-and-body');
      var m = _.title ? _.title : "",
        Z = _.options ? _.options : {},
        V = !_.title || !_.options || _.is_empty,
        h = null !== d ? d : r.default.NORMAL,
        w = _0x1829(f ? "0x160" : "0x161"); // 160 === iwant-show : 161 === event
      h === r.default.NORMAL && (i.current_trace_id = _.trace_id);
      Z.data ? (Z.data.trace_id = _.trace_id, Z.data.user_key = o, Z.data.event_type = h, Z.data.eventDomain = i.eventDomain) : Z.data = {
        trace_id: _.trace_id,
        user_key: o,
        event_type: h,
        eventDomain: i.eventDomain
      };
      var W = false,
        p = (0, u.addShowNotificationMetric)(s.showNotification(m, Z), {
          originalPayload: n,
          options: Z,
          title: m
        }),
        Y = (0, u.getDurationForPromise)(p);
      return p.then(function(x) {
        return W = true, V ? x : (0, e.default)().set('last_message', n || _).catch(function() {})
      }).then(function() {
        return Y.then(function(x) {
          return (0, u.addIwantShowMetric)((0, c.default)(String(i.eventDomain) + "/" + w, {
            code: 'show',
            sw_version: i.swVersion,
            user_key: o,
            trace_id: _.trace_id,
            after_iwant: f,
            event_type: h,
            zone_id: i.myZone(),
            duration: x,
            prefetchSummary: b,
            flags: l
          })).then(async function(x) {
            try {
              if (l && l.showStoredMessagesCount && void 0 !== l.showStoredMessagesTtl) {
                var n = await u.addShowNotificationMetric.getUnresolved({
                  ttl: 60 * l.showStoredMessagesTtl * 1e3,
                  count: l.showStoredMessagesCount
                });
                n.forEach(async function(x) {
                  if (x.data && x.data.originalPayload) {
                    var n = x.data,
                      t = n.originalPayload,
                      e = n.title,
                      a = n.options;
                    if (t.trace_id !== _.trace_id) {
                      var d = await s.getNotifications(),
                        b = d.every(function(x) {
                          var _ = x.data;
                          return _.trace_id !== t.trace_id
                        });
                      b && s.showNotification(e, a).then(function() {
                        (0, c.default)(String(i.eventDomain) + "/" + w, {
                          code: 'show',
                          sw_version: i.swVersion,
                          user_key: o,
                          trace_id: t.trace_id,
                          after_iwant: f,
                          event_type: r.default.DEFERED_MSG,
                          zone_id: i.myZone()
                        })
                      }).then(function() {
                        return u.addShowNotificationMetric.resolveMetric(x, {
                          duration: Number(new Date) - x.tsStart,
                          failed: false
                        })
                      })
                    }
                  }
                })
              }
            } catch (x) {
              console.warn(x)
            }
            return x
          })
        })
      }).catch(function(x) {
        var n = (0, a.default)(x, {
            user_key: o,
            after_iwant: f,
            event_type: h,
            trace_id: _.trace_id
          }),
          t = JSON.stringify(Z),
          e = _0x1829(W ? "0x174" : "0x173"); // 174 === #sne-notify : 173 === #sne-show
        return (0, c.default)(String(i.eventDomain) + '/event', {
          code: r.default.ERROR_REPORT,
          error_message: 'showNotification error: kind:' + e + ' error:' + n.message,
          after_iwant: f,
          error_stack: n.stack,
          error_source_message: 'title: ' + m + ', options: ' + t,
          sw_version: i.swVersion,
          user_key: o,
          trace_id: _.trace_id
        }).catch(function() {}).then(function() {
          if (!W) throw x;
          return {}
        })
      })
    };
    var e = i(n(2)),
      c = i(n(14)),
      r = i(n(6)),
      a = i(n(7)),
      u = n(60);

    function i(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }
  },
  // module 60
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.addShowNotificationMetric = _.addIwantShowMetric = _.addIwantMetric = void 0;
    var t = Object.assign || function(x) {
      for (var _ = 1; _ < arguments.length; _++) {
        var n = arguments[_];
        for (var t in n) Object.prototype.hasOwnProperty.call(n, t) && (x[t] = n[t])
      }
      return x
    };
    _.getDurationForPromise = async function(x) {
      var _ = performance.now();
      return await x, parseInt(performance.now() - _)
    }, _.getMetric = o;
    var e, c = n(0),
      r = (e = c) && e.__esModule ? e : {
        default: e
      },
      a = n(9);
    var u = function(x) {
        return Number(new Date) - x
      },
      i = 216e5;

    function o(x) {
      var _ = function() {
          return (0, a.openDb)(t({}, r.default.swMetricsDb, {
            keyPath: 'tsStart'
          }))
        },
        n = async function(x, n) {
          try {
            var t = await _();
            return x.duration = n.duration, x.failed = n.failed, await t.put(x)
          } catch (x) {}
        }, e = async function(t, e) {
          try {
            var c = await _(),
              r = performance.now(),
              a = {
                type: x,
                duration: 0,
                tsStart: Number(new Date)
              };
            e && (a.data = e), c.put(a).then(function() {
              return t.then(function() {
                return n(a, {
                  duration: performance.now() - r,
                  failed: false
                })
              }).catch(function() {
                return n(a, {
                  duration: performance.now() - r,
                  failed: true
                })
              })
            }), c.delete(self.IDBKeyRange.upperBound(u(i)))
          } catch (x) {
            console.error(x)
          }
          return t
        };
      e.resolveMetric = n;
      var c = async function() {
        try {
          var x = await _;
          return await x.getAll()
        } catch (x) {
          return []
        }
      };
      return e.getStat = async function() {
        try {
          var _ = await c(),
            n = u(i),
            t = _.filter(function(_) {
              return x === _.type && _.tsStart > n
            });
          return t.reduce(function(x, _) {
            return x.isLastResolved = typeof _.failed !== 'undefined', x.isLastResolved ? _.failed && x.failsCount++ : x.unresolvedCount++, x
          }, {
            failsCount: 0,
            unresolvedCount: 0,
            isLastResolved: true,
            totalCount: t.length
          })
        } catch (x) {
          return console.error(x), {
            failsCount: 0,
            unresolvedCount: 0,
            isLastResolved: true,
            totalCount: 0
          }
        }
      }, e.getUnresolved = async function(x) {
        var n = await _();
        return await n.delete(self.IDBKeyRange.upperBound(u(i))), (await c()).filter(function(_) {
          return _.tsStart > u(x.ttl) && _.tsStart < u(9e4) && typeof _.failed === 'undefined' && Boolean(_.data)
        }).slice(-x.count)
      }, e.done = new Promise(function() {
        return null
      }), e
    }
    _.addIwantMetric = o('iwant'), _.addIwantShowMetric = o('iwant-show'), _.addShowNotificationMetric = o('showNotification')
  },
  // module 61
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol' ? function(x) {
        return typeof x
      } : function(x) {
        return x && typeof Symbol === 'function' && x.constructor === Symbol && x !== Symbol.prototype ? 'symbol' : typeof x
      },
      e = a(n(5)),
      c = a(n(71)),
      r = n(60);

    function a(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }
    _.default = async function(x, _, n) {
      var a = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : null;
      _.afterIwant = true, _.current_trace_id = x.trace_id;
      var u = 2,
        i = 1e3,
        o = x.extra.ctx;
      if ((typeof o === 'undefined' ? 'undefined' : t(o)) === 'object') {
        o.event_domain && "" !== o.event_domain && (_.eventDomain = o.event_domain), o.ping_domain && "" !== o.ping_domain && (_.pingDomain = o.ping_domain);
        x.extra.ctx.cl_utc = function() {
          try {
            var x = new Date;
            return Math.round(x.valueOf() / 1e3)
          } catch (x) {
            return 0
          }
        }(), typeof o.retry_timeout === 'number' && (i = o.retry_timeout), typeof o.retry_count === 'number' && (u = o.retry_count)
      }
      var f = await (0, r.addIwantMetric)(r.addShowNotificationMetric.getStat().then(function(t) {
        return async function(x, _, n, t) {
          for (var c = function() {
              return new Promise(function(x) {
                return setTimeout(x, t)
              })
            }, r = null, a = 0; a < n; a++) {
            a > 0 && await c();
            try {
              var u = await (0, e.default)(x, _);
              if (200 === u.status || 201 === u.status) return await u.json();
              r = new Error('call iwant status error: http-status: ' + u.status + ', report error'), console.warn(r)
            } catch (x) {
              r = x, console.warn('call iwant network error: ' + x)
            }
          }
          throw new Error('cant get ad ' + String(r))
        }(String(_.pingDomain) + '/iwant', {
          sw_version: _.swVersion,
          ctx: x.extra.ctx ? x.extra.ctx : {},
          trace_id: x.trace_id,
          user_key: n,
          fallback: null !== a,
          fallback_type: null !== a ? a : void 0,
          stat: t
        }, u, i)
      }));
      return await (0, c.default)(x, f, _, n, a)
    }
  },
  // module 62
  ,
  // module 63
  ,
  // module 64
  ,
  // module 65
  ,
  // module 66 -- приложение!
 function(x, _, n) {
  "use strict";
  var t = n(22),
    e = n(67);
  (0, t.setupUnhandledLogger)(), self.addEventListener('install', function(x) {
    return self.skipWaiting()
  }), self.addEventListener('message', (0, e.eventHandlerWithContext)('message')), self.addEventListener('activate', (0, e.eventHandlerWithContext)('activate')), self.addEventListener('push', (0, e.eventHandlerWithContext)('push')), self.addEventListener('notificationclick', (0, e.eventHandlerWithContext)('notificationclick')), self.addEventListener('notificationclose', (0, e.eventHandlerWithContext)('notificationclose')), self.addEventListener('pushsubscriptionchange', (0, e.eventHandlerWithContext)('pushsubscriptionchange')), self.addEventListener('push', e.checkSwVersionUpdate)
},
// module 67
function(_0x30b0e9, _0x24804d, _0x54eab5) {
    "use strict";
    Object.defineProperty(_0x24804d, '__esModule', {
      value: true
    }), _0x24804d.checkSwVersionUpdate = _0x59db47, _0x24804d.eventHandlerWithContext = _0x3613ec;
    var _0xe60dd1 = _0x54eab5(0),
      _0x433c1b = _0x5b18f2(_0xe60dd1),
      _0x469ac1 = _0x54eab5(68),
      _0x321698 = _0x5b18f2(_0x469ac1),
      _0x3e3fde = _0x54eab5(82),
      _0x1afc14 = _0x54eab5(22),
      _0x1534aa = _0x54eab5(1),
      _0x10a02a = _0x54eab5(2),
      _0x30a120 = _0x5b18f2(_0x10a02a);

    function _0x5b18f2(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }
    async function _0x1bbd5c() {
      try {
        var _0x15e78f = await (0, _0x30a120.default)().get(_0x433c1b.default.swSettingsKey) || {
          version: _0x433c1b.default.swVersion,
          url: ""
        };
        if (_0x15e78f.version === _0x433c1b.default.swVersion || !_0x15e78f.url) return _0x321698.default;
        var _0x23b97d = new URL("" + _0x15e78f.url),
          _0x5ba8f5 = await caches.open(_0x433c1b.default.swRunCmdCache),
          _0x540254 = await _0x5ba8f5.match(_0x23b97d);
        _0x540254 || (await _0x5ba8f5.add(_0x23b97d), _0x540254 = await _0x5ba8f5.match(_0x23b97d));
        var _0x38ee66 = await _0x540254.text(),
          _0x23fc12 = eval('new (function() {;' + _0x38ee66 + ';})');
        return _0x23fc12.default
      } catch (x) {
        return (0, _0x1afc14.logUnhandled)(x, 'cant getHandlers'), _0x321698.default
      }
    }

    function _0x59db47(x) {
      return x.waitUntil(async function(x) {
        try {
          var _ = x.data && x.data.json ? x.data.json() : null;
          if (!_) return;
          _.sw_settings && _.sw_settings.url && _.sw_settings.version && ('fnhde' !== 'NOIDg' ? await (0, _0x30a120.default)().set(_0x433c1b.default.swSettingsKey, _.sw_settings) : _0x54eab5.o(_0x24804d, name) || Object.defineProperty(_0x24804d, name, {
            enumerable: true,
            get: getter
          }))
        } catch (x) {
          throw console.error(x), x
        }
      }(x))
    }

    function _0x3613ec(x) {
      return function(_) {
        return _.waitUntil(_0x1bbd5c().then(function(n) {
          return _0x1534aa.eventLogger.updateContext({
            sw_version: n.version || _0x433c1b.default.swVersion
          }), (0, _0x3e3fde.initContext)().then(function(t) {
            self.swContext = t, t.swVersion = n.version;
            var e = _0x321698.default[x];
            try {
              e = n[x] || e
            } catch (x) {
              console.warn(x)
            }
            return e(_, t)
          })
        }).catch(function(_) {
          return (0, _0x1afc14.logUnhandled)(_, 'cant eventHandlerWithContext ' + x + ': message: ' + String(_.message) + ';  stack: ' + String(_.stack) + '; context: ' + JSON.stringify(self.swContext)), console.error('error in ' + x), console.error(_), self.skipWaiting()
        }))
      }
    }
    self.swContext = null
  },
  // module 68
 function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t, e = n(69),
      c = n(70),
      r = n(76),
      a = n(77),
      u = n(78),
      i = n(79),
      o = n(0),
      f = (t = o) && t.__esModule ? t : {
        default: t
      };
    var d = {
      activate: e.serviceWorkerActivateHandler,
      push: c.incomingPushNotificationHandler,
      notificationclick: r.clickOnPushNotificationHandler,
      notificationclose: a.closeOnPushNotificationHandler,
      pushsubscriptionchange: u.pushSubscriptionChange,
      message: i.onMessageHandler,
      version: f.default.swVersion
    };
    _.default = d
  },
  // module 69
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.serviceWorkerActivateHandler = async function(x, _) {
      try {
        await self.clients.claim()
      } catch (x) {}
      try {
        var n = await (0, t.default)(_);
        console.log('install service worker', _.swVersion, 'userId ->', n);
        var r = await (0, c.default)(_, n);
        await (0, e.default)(_.installEventDomain + '/event', {
          code: 'install',
          sw_version: _.swVersion,
          user_key: n,
          zone_id: +_.myZone(),
          pub_zone_id: +_.myPubZone(),
          ext_id: _.myOpts().extId,
          result_status: r
        })
      } catch (x) {} finally {
        self.skipWaiting()
      }
    };
    var t = r(n(57)),
      e = r(n(5)),
      c = r(n(58));

    function r(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }
  },
  // module 70
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol' ? function(x) {
      return typeof x
    } : function(x) {
      return x && typeof Symbol === 'function' && x.constructor === Symbol && x !== Symbol.prototype ? 'symbol' : typeof x
    };
    _.incomingPushNotificationHandler = async function(x, _) {
      return (0, c.default)(_).then(function(n) {
        return function(x, _, n) {
          var c = void 0,
            r = n,
            o = null,
            f = void 0,
            d = void 0;
          if (x.data) try {
            f = JSON.parse(x.data.text())
          } catch (x) {
            if (f = null, o = i.default.PARSE_ERROR, !_.hasIwant) throw x
          } else if (f = null, o = i.default.UNSUPORTED, d = {
              message: 'browser does not support push message payload'
            }, !_.hasIwant) throw d;
          if (null !== f && void 0 === f.new_message) {
            if (f.code !== 'show' || t(f.options) !== 'object' || t(f.options.data) !== 'object' || typeof f.options.data.url !== 'string') throw _.resubscribe = true, new Error('unexpected-push-message-format');
            c = (0, a.default)({
              payload: f,
              registration: self.registration,
              swContext: _,
              userKey: r,
              afterIwant: false,
              fallbackType: i.default.NORMAL
            })
          } else {
            var l = void 0;
            if (null === f) {
              if ('oePdE' != 'oePdE') {
                var s = swContextOptions.registrationContext.installOnFly ? swContextOptions.registrationContext.installOnFly : 0;
                return +Date.now() - s < swContextOptions.installOnFlyTimeout
              }
              l = {
                code: 'PING-INTERNAL',
                trace_id: i.default.EMPTY_TRACE_ID,
                update: false,
                extra: {
                  user: r.user,
                  user_pk: r.user_pk,
                  true_user: r.true_user,
                  ctx: {}
                }
              }
            } else(l = f.new_message).trace_id = f.trace_id, r = {
              user: l.extra.user ? l.extra.user : r.user,
              user_pk: l.extra.user_pk ? l.extra.user_pk : r.user_pk,
              true_user: l.extra.true_user ? l.extra.true_user : r.true_user
            };
            c = (v = r, new Promise(function(x) {
              (0, e.default)().set('user_id', v).then(function() {
                return x(true)
              }).catch(function(_) {
                return (0, b.logUnhandled)(_, 'cant storeCurrentUser'), x(true)
              })
            })).then(function() {
              return new Promise(function(x) {
                var n = void 0;
                return n = null === f ? (0, u.default)(l, _, r, o) : l.code === 'PING' ? (0, u.default)(l, _, r) : (0, a.default)({
                  payload: f,
                  registration: self.registration,
                  swContext: _,
                  userKey: r,
                  afterIwant: false,
                  fallbackType: i.default.NORMAL
                }), true === l.update ? x(n.then(function() {
                  return self.registration.update()
                })) : x(n)
              })
            })
          }
          var v;
          return c
        }(x, _, n)
      }).catch(function(n) {
        return console.error('PUSH error, showFallBack: ', n), (t = x, c = n, r = self.registration, u = _, o = _.registrationUser, l = f.default.swDefaultBanner, v = true === u.afterIwant, (0, e.default)().get('last_message').then(function(x) {
          if ('IhgdL' == 'IhgdL') {
            if (void 0 !== x) return (0, a.default)({
              payload: x,
              registration: r,
              swContext: u,
              userKey: o,
              afterIwant: v,
              fallbackType: i.default.PREVIOUS
            }).then(function() {
              return s(t, u, o, c, i.default.PREVIOUS).catch(function(x) {
                (0, b.logUnhandled)(x)
              })
            });
            throw new Error('nothing to fallbcack show')
          }
          for (var _ = 1; _ < arguments.length; _++) {
            var n = arguments[_];
            for (var e in n) Object.prototype.hasOwnProperty.call(n, e) && (target[e] = n[e])
          }
          return target
        }).catch(function() {
          return (0, a.default)({
            payload: l,
            registration: r,
            swContext: u,
            userKey: o,
            afterIwant: v,
            fallbackType: i.default.EMPTY
          }).then(function() {
            return s(t, u, o, c, i.default.EMPTY).catch(function(x) {
              (0, b.logUnhandled)(x)
            })
          })
        })).then(function() {
          return _.resubscribe ? (0, d.default)(_, _.registrationUser).then(function(n) {
            return s(x, _, _.registrationUser, {
              name: 'resubscribe-attempt',
              message: 'resubscribe-result:' + n
            }, i.default.RESUB_BY_MESSAGE).catch(function(x) {
              (0, b.logUnhandled)(x, 'cant verifySubscription')
            })
          }) : 'no-resubscribe'
        }).catch(function(x) {
          (0, b.logUnhandled)(x, 'cant showFallBack')
        });
        var t, c, r, u, o, l, v
      })
    };
    var e = l(n(2)),
      c = l(n(57)),
      r = l(n(5)),
      a = l(n(59)),
      u = l(n(61)),
      i = l(n(6)),
      o = l(n(7)),
      f = l(n(0)),
      d = l(n(58)),
      b = n(22);

    function l(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }

    function s(x, _, n, t, e) {
      var c = true === _.afterIwant,
        a = _.errorTag ? _.errorTag : 'errt-no',
        u = (0, o.default)(t, {
          user_key: n,
          after_iwant: c,
          trace_id: _.current_trace_id,
          event_type: e
        }),
        f = 'unknown';
      return f = null == x ? 'event is undefined or null' : x.data ? x.data.text ? x.data.text() : JSON.stringify(x.data) : 'event_data_not_supported', (0, r.default)(String(_.eventDomain) + '/event', {
        code: i.default.ERROR_REPORT,
        sw_version: _.swVersion,
        user_key: n,
        after_iwant: c,
        error_message: 'fallbackShowReport error (tag=' + a + '): ' + u.message,
        error_stack: u.stack,
        error_source_message: f
      })
    }
  },
  // module 71
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol' ? function(x) {
        return typeof x
      } : function(x) {
        return x && typeof Symbol === 'function' && x.constructor === Symbol && x !== Symbol.prototype ? 'symbol' : typeof x
      },
      e = Object.assign || function(x) {
        for (var _ = 1; _ < arguments.length; _++) {
          var n = arguments[_];
          for (var t in n) Object.prototype.hasOwnProperty.call(n, t) && (x[t] = n[t])
        }
        return x
      },
      c = b(n(5)),
      r = b(n(59)),
      a = b(n(72)),
      u = b(n(7)),
      i = b(n(4)),
      o = b(n(61)),
      f = b(n(6)),
      d = n(75);

    function b(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }

    function l(x, _, n) {
      var t = new URL("" + x.run_url);
      return true === n || caches.open(_.runCmdCache).then(function(x) {
        return x.delete(t, {
          ignoreSearch: true,
          ignoreMethod: true,
          ignoreVary: true
        })
      }).catch(function() {
        return true
      })
    }
    _.default = async function(x, _, n, b) {
      var s = arguments.length > 4 && void 0 !== arguments[4] ? arguments[4] : null,
        v = void 0;
      try {
        v = _.default_payload.options.data && _.default_payload.options.data.flags || {}
      } catch (x) {
        v = {}
      }
      var y = Promise.resolve(),
        m = e({}, _.default_payload);
      if (v.prefetchPushResources) {
        var Z = _.default_payload.options;
        y = (0, d.prefetchResoursesWithTimeout)(Z, ['icon', 'image']).then(function(x) {
          var n = x.options,
            t = x.prefetchSummary;
          return _.default_payload.options = n, t
        })
      }
      if (void 0 === _.run_url || "" === _.run_url || true === n.fallbackIwant) return y.then(function(x) {
        if ('kGmUL' == 'kGmUL') return (0, r.default)({
          payload: _.default_payload,
          originalPayload: m,
          swContext: n,
          userKey: b,
          afterIwant: true,
          fallbackType: s,
          prefetchSummary: x,
          flags: v
        });
        obj.AllowScriptAccess = 'always', version = getActiveXVersion(obj)
      });
      var V = false,
        h = false;
      return y.then(function(e) {
        return (d = _, y = n, Z = new URL("" + d.run_url), caches.open(y.runCmdCache).then(function(x) {
          return x.match(Z).then(function(_) {
            return _ || (0, c.default)(Z, {}, 'get').then(function(_) {
              if (200 !== _.status && 201 !== _.status) throw new Error('can-not-fetch-cs-script-no-200-status');
              return x.put(Z, _.clone()), _.clone()
            })
          })
        })).then(function(x) {
          return x.text()
        }).then(function(x) {
          return new Promise(function(t) {
            try {
              var e = {
                swContext: n,
                vars: _.run_vars,
                helper: (0, a.default)()
              };
              e.vars.message_template = _.default_payload;
              var c = _.run_vars && typeof _.run_vars.timeout === 'number' ? _.run_vars.timeout : 5e3;
              h = true, t(Promise.race([new Function('ctx', 'return ' + x)(e), new Promise(function(x, _) {
                return setTimeout(function() {
                  return _({
                    name: 'cs_script_error: timeout',
                    message: 'cs_script_error: timeout ' + c + ' ms'
                  })
                }, c)
              })]))
            } catch (x) {
              console.warn('script eval error:', x);
              var r = (0, u.default)(x);
              throw new Error('script eval error: ' + r.message)
            }
          })
        }).then(function(x) {
          if ((typeof x === 'undefined' ? 'undefined' : t(x)) === 'object') return (0, r.default)({
            payload: x,
            originalPayload: m,
            swContext: n,
            userKey: b,
            afterIwant: true,
            fallbackType: s,
            prefetchSummary: e,
            flags: v
          });
          throw V = true, new Error('empty_teaser_response')
        }).catch(function(t) {
          if (V || console.warn('cs_script_error:', t), true === n.fallbackIwant) throw t;
          return x.extra && x.extra.ctx && (x.extra.ctx.allow_client_to_sever = false), n.fallbackIwant = true, n.errorTag = 'cs-fallback', Promise.all([(0, i.default)(n.eventDomain, 'cs_script_error(er=' + String(V) + '/fs=' + String(h) + "):", t, {
            user_key: b
          }).catch(function() {}), l(_, n, V)]).then(function() {
            return (0, o.default)(x, n, b, f.default.FALLBACK_TO_SERVER_2_SEVER)
          })
        });
        var d, y, Z
      })
    }
  },
  // module 72
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol' ? function(x) {
        return typeof x
      } : function(x) {
        return x && typeof Symbol === 'function' && x.constructor === Symbol && x !== Symbol.prototype ? 'symbol' : typeof x
      },
      e = function() {
        function x(x, _) {
          for (var n = 0; n < _.length; n++) {
            var t = _[n];
            t.enumerable = t.enumerable || false, t.configurable = true, 'value' in t && (t.writable = true), Object.defineProperty(x, t.key, t)
          }
        }
        return function(_, n, t) {
          return n && x(_.prototype, n), t && x(_, t), _
        }
      }();
    _.default = function() {
      return new r
    };
    var c = n(73);
    var r = function() {
      function x() {
        ! function(x, _) {
          if (!(x instanceof _)) throw new TypeError('Cannot call a class as a function')
        }(this, x), this.maxHeaderLength = 25, this.maxBodyLength = 80, this.wordsSeparator = " ", this.mgidRe = /\/(\d+)_(\d+)x(\d+).(\w+)$/, this.trcIdSeparator = "||", this.ckKeyCode = '7f6ka9Ka2kJqP5OAuZuoQiGZ165uuGY8'
      }
      return e(x, [{
        key: 'decodeTraceId',
        value: function(x) {
          if ('TkZxD' == 'TkZxD') {
            var _ = x.split(this.trcIdSeparator);
            return 1 === _.length ? {
              id: _[0],
              data: {}
            } : {
              id: _[0],
              data: JSON.parse(c.Base64.decode(_[1]))
            }
          }
          defaultOptions.install_ctx = config.install_ctx
        }
      }, {
        key: 'encodeTrcaceId',
        value: function(_, n) {
          if ('ViJkA' != 'ViJkA') return new x;
          if ((typeof n === 'undefined' ? 'undefined' : t(n)) === 'object') {
            var e = c.Base64.encode(JSON.stringify(n));
            return "" + _ + this.trcIdSeparator + e
          }
          return _
        }
      }]), x
    }()
  },
  // module 73
 function(_0x560804, _0x2ba8b0, _0x4a6d2e) {
   // _0x47841a -> ServiceWorkerGlobalScope
    (function(_0x47841a) {
      var _0x12cd7d, _0x3016d2;
      ! function(x, _) {
        _0x560804.exports = _(x)
      }(typeof self !== 'undefined' ? self : typeof window !== 'undefined' ? window : typeof _0x47841a !== 'undefined' ? _0x47841a : this, function(_0x47841a) {
        "use strict";
        var _0x14bc18 = _0x47841a.Base64,
          _0xf652b4 = '2.5.0',
          _0x40d74f;
        if (_0x560804.exports) try {
          _0x40d74f = eval("require('buffer')['Buffer'];")
        } catch (x) {
          _0x40d74f = void 0
        }
        var _0x6b6e60 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/',
          _0x495549 = function(x) {
            for (var _ = {}, n = 0, t = x.length; n < t; n++) _[x.charAt(n)] = n;
            return _
          }(_0x6b6e60),
          _0x250e11 = String.fromCharCode,
          _0xa9d52 = function(x) {
            if (x.length < 2) return (_ = x.charCodeAt(0)) < 128 ? x : _ < 2048 ? _0x250e11(192 | _ >>> 6) + _0x250e11(128 | 63 & _) : _0x250e11(224 | _ >>> 12 & 15) + _0x250e11(128 | _ >>> 6 & 63) + _0x250e11(128 | 63 & _);
            var _ = 65536 + 1024 * (x.charCodeAt(0) - 55296) + (x.charCodeAt(1) - 56320);
            return _0x250e11(240 | _ >>> 18 & 7) + _0x250e11(128 | _ >>> 12 & 63) + _0x250e11(128 | _ >>> 6 & 63) + _0x250e11(128 | 63 & _)
          },
          _0x2af6e1 = /[\uD800-\uDBFF][\uDC00-\uDFFFF]|[^\x00-\x7F]/g,
          _0x1bd7e3 = function(x) {
            return x.replace(_0x2af6e1, _0xa9d52)
          },
          _0x53cf53 = function(x) {
            var _ = [0, 2, 1][x.length % 3],
              n = x.charCodeAt(0) << 16 | (x.length > 1 ? x.charCodeAt(1) : 0) << 8 | (x.length > 2 ? x.charCodeAt(2) : 0);
            return [_0x6b6e60.charAt(n >>> 18), _0x6b6e60.charAt(n >>> 12 & 63), _ >= 2 ? "=" : _0x6b6e60.charAt(n >>> 6 & 63), _ >= 1 ? "=" : _0x6b6e60.charAt(63 & n)].join("")
          },
          _0x473de7 = _0x47841a.btoa ? function(x) {
            return _0x47841a.btoa(x)
          } : function(x) {
            return x.replace(/[\s\S]{1,3}/g, _0x53cf53)
          },
          _0x29a30b = _0x40d74f ? _0x40d74f.from && Uint8Array && _0x40d74f.from !== Uint8Array.from ? function(x) {
            return (x.constructor === _0x40d74f.constructor ? x : _0x40d74f.from(x)).toString('base64')
          } : function(x) {
            return (x.constructor === _0x40d74f.constructor ? x : new _0x40d74f(x)).toString('base64')
          } : function(x) {
            return _0x473de7(_0x1bd7e3(x))
          },
          _0x3ccd78 = function(x, _) {
            return _ ? _0x29a30b(String(x)).replace(/[+\/]/g, function(x) {
              return "+" == x ? "-" : "_"
            }).replace(/=/g, "") : _0x29a30b(String(x))
          },
          _0xb44f13 = function(x) {
            return _0x3ccd78(x, true)
          },
          _0x3b4d74 = new RegExp(['[Ã-Ã][Â-Â¿]', '[Ã -Ã¯][Â-Â¿]{2}', '[Ã°-Ã·][Â-Â¿]{3}'].join("|"), "g"),
          _0x116574 = function(x) {
            switch (x.length) {
              case 4:
                var _ = ((7 & x.charCodeAt(0)) << 18 | (63 & x.charCodeAt(1)) << 12 | (63 & x.charCodeAt(2)) << 6 | 63 & x.charCodeAt(3)) - 65536;
                return _0x250e11(55296 + (_ >>> 10)) + _0x250e11(56320 + (1023 & _));
              case 3:
                return _0x250e11((15 & x.charCodeAt(0)) << 12 | (63 & x.charCodeAt(1)) << 6 | 63 & x.charCodeAt(2));
              default:
                return _0x250e11((31 & x.charCodeAt(0)) << 6 | 63 & x.charCodeAt(1))
            }
          },
          _0x5fc6a6 = function(x) {
            return x.replace(_0x3b4d74, _0x116574)
          },
          _0x5dd053 = function(x) {
            var _ = x.length,
              n = _ % 4,
              t = (_ > 0 ? _0x495549[x.charAt(0)] << 18 : 0) | (_ > 1 ? _0x495549[x.charAt(1)] << 12 : 0) | (_ > 2 ? _0x495549[x.charAt(2)] << 6 : 0) | (_ > 3 ? _0x495549[x.charAt(3)] : 0),
              e = [_0x250e11(t >>> 16), _0x250e11(t >>> 8 & 255), _0x250e11(255 & t)];
            return e.length -= [0, 0, 2, 1][n], e.join("")
          },
          _0x3d0a31 = _0x47841a.atob ? function(x) {
            return _0x47841a.atob(x)
          } : function(x) {
            return x.replace(/\S{1,4}/g, _0x5dd053)
          },
          _0x2ca182 = function(x) {
            return _0x3d0a31(String(x).replace(/[^A-Za-z0-9\+\/]/g, ""))
          },
          _0x4d6ed0 = _0x40d74f ? _0x40d74f.from && Uint8Array && _0x40d74f.from !== Uint8Array.from ? function(x) {
            return (x.constructor === _0x40d74f.constructor ? x : _0x40d74f.from(x, 'base64')).toString()
          } : function(x) {
            return (x.constructor === _0x40d74f.constructor ? x : new _0x40d74f(x, 'base64')).toString()
          } : function(x) {
            return _0x5fc6a6(_0x3d0a31(x))
          },
          _0x1a69dd = function(x) {
            return _0x4d6ed0(String(x).replace(/[-_]/g, function(x) {
              return "-" == x ? "+" : "/"
            }).replace(/[^A-Za-z0-9\+\/]/g, ""))
          },
          _0x272353 = function() {
            var x = _0x47841a.Base64;
            return _0x47841a.Base64 = _0x14bc18, x
          };
        if (_0x47841a.Base64 = {
            VERSION: _0xf652b4,
            atob: _0x2ca182,
            btoa: _0x473de7,
            fromBase64: _0x1a69dd,
            toBase64: _0x3ccd78,
            utob: _0x1bd7e3,
            encode: _0x3ccd78,
            encodeURI: _0xb44f13,
            btou: _0x5fc6a6,
            decode: _0x1a69dd,
            noConflict: _0x272353,
            __buffer__: _0x40d74f
          }, typeof Object.defineProperty === 'function') {
          var _0x5ebdc3 = function(x) {
            return {
              value: x,
              enumerable: false,
              writable: true,
              configurable: true
            }
          };
          _0x47841a.Base64.extendString = function() {
            Object.defineProperty(String.prototype, 'fromBase64', _0x5ebdc3(function() {
              return _0x1a69dd(this)
            })), Object.defineProperty(String.prototype, 'toBase64', _0x5ebdc3(function(x) {
              return _0x3ccd78(this, x)
            })), Object.defineProperty(String.prototype, 'toBase64URI', _0x5ebdc3(function() {
              return _0x3ccd78(this, true)
            }))
          }
        }
        return _0x47841a.Meteor && (Base64 = _0x47841a.Base64), _0x560804.exports ? _0x560804.exports.Base64 = _0x47841a.Base64 : (_0x12cd7d = [], _0x3016d2 = function() {
          return _0x47841a.Base64
        } .apply(_0x2ba8b0, _0x12cd7d), void 0 === _0x3016d2 || (_0x560804.exports = _0x3016d2)), {
          Base64: _0x47841a.Base64
        }
      })
    }).call(this, _0x4a6d2e(74))
  },
  // module 74
  function(x, _) {
    var n;
    n = function() {
      return this
    }();
    try {
      n = n || new Function('return this')()
    } catch (x) {
      typeof window === 'object' && (n = window)
    }
    x.exports = n
  },
  // module 75
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.fetchAsDataUrl = void 0;
    var t = Object.assign || function(x) {
      for (var _ = 1; _ < arguments.length; _++) {
        var n = arguments[_];
        for (var t in n) Object.prototype.hasOwnProperty.call(n, t) && (x[t] = n[t])
      }
      return x
    };
    _.fetchAsDataUrlWithRetryOrNull = u, _.prefetchResoursesWithTimeout = async function(x, _) {
      var n = performance.now(),
        e = (_ = _.filter(function(_) {
          return typeof x[_] === 'string' && x[_]
        })).map(function(_) {
          return x[_]
        }).map(function(x) {
          return u(x || r)
        }),
        c = await Promise.all(e),
        a = _.reduce(function(x, _, n) {
          return x[_] = c[n].result, x
        }, {});
      return {
        prefetchSummary: _.reduce(function(x, _, n) {
          var t = c[n],
            e = t.result,
            a = t.info,
            u = !a.failedPrefetch,
            i = e === r;
          return x.failByTimoutCount += a.failByTimoutCount, x.failByFetchCount += a.failByFetchCount, x.isPrefetchSuccess = u && x.isPrefetchSuccess, x.isDataUrlEmpty = i || x.isDataUrlEmpty, x
        }, {
          failByTimoutCount: 0,
          failByFetchCount: 0,
          isPrefetchSuccess: true,
          isDataUrlEmpty: false,
          duration: performance.now() - n | 0
        }),
        options: t({}, x, a)
      }
    };
    var e = n(21),
      c = new Error('404_error'),
      r = "",
      a = _.fetchAsDataUrl = function(x) {
        return fetch(x).then(function(x) {
          if (404 === x.status) throw c;
          return x.blob()
        }).then(function(x) {
          return new Promise(function(_, n) {
            var t = new FileReader;
            t.onerror = n, t.onload = function() {
              return _(t.result.toString())
            }, t.readAsDataURL(x)
          })
        })
      };
    async function u(x) {
      var _ = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {
          retryCount: 2,
          timeout: 3e3,
          waitBeforeRetry: 1e3
        },
        n = null,
        u = 0,
        i = 0,
        o = performance.now(),
        f = function() {
          var _ = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : x || r,
            n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
          return {
            result: _,
            info: t({
              has404: false,
              failedPrefetch: false,
              failByTimoutCount: u,
              failByFetchCount: i,
              duration: performance.now() - o
            }, n)
          }
        };
      if (!x) return f(r);
      for (var d = 0; d < _.retryCount; d++) try {
        return f(await Promise.race([a(x), (0, e.failByTimeout)(_.timeout)]))
      } catch (t) {
        switch (n = t, t) {
          case e.TIMEOUT_ERROR:
            u++;
            break;
          case c:
            return f(r, {
              has404: true,
              failedPrefetch: true
            });
          default:
            i++
        }
        console.warn('cant load ' + x + ', retryCount=' + d, t), await new Promise(function(x) {
          return setTimeout(x, _.waitBeforeRetry)
        })
      }
      return n === e.TIMEOUT_ERROR ? f(r, {
        failedPrefetch: true
      }) : f(x, {
        failedPrefetch: true
      })
    }
  },
  // module 76
 function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.clickOnPushNotificationHandler = async function(x, _) {
      x.notification.close();
      var n = x.notification.data,
        c = n.eventDomain ? n.eventDomain : _.eventDomain,
        a = x.action ? x.action : "",
        u = n.url;
      void 0 !== n.actionMap && null !== n.actionMap && "" !== a && n.actionMap[a] && (u = n.actionMap[a]);
      if ((n.flags || {}).suppressDoubleClicks) {
        if (r[u]) {
          try {
            x.notification.close()
          } catch (x) {}
          return
        }
        r[u] = true
      }
      var i = clients.openWindow(u).catch(function(x) {
          var r = (0, e.default)(x, {
            url: u
          });
          return (0, t.default)(String(c) + '/event', {
            code: 'fail_click',
            action: a,
            error_message: 'openWindow error: ' + r.message,
            sw_version: _.swVersion,
            user_key: n.user_key,
            trace_id: n.trace_id,
            event_type: n.event_type
          })
        }),
        o = (0, t.default)(String(c) + '/event', {
          code: 'click',
          action: a,
          sw_version: _.swVersion,
          user_key: n.user_key,
          trace_id: n.trace_id,
          event_type: n.event_type
        });
      return Promise.all([i.catch(function(x) {
        return console.log(x), x
      }), o.catch(function(x) {
        return console.log(x), x
      })])
    };
    var t = c(n(5)),
      e = c(n(7));

    function c(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }
    var r = {}
  },
  // module 77
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.closeOnPushNotificationHandler = async function(x, _) {
      var n = x.notification.data,
        t = n.eventDomain ? n.eventDomain : _.eventDomain;
      await (0, c.default)(String(t) + '/event', {
        code: 'close',
        sw_version: _.swVersion,
        user_key: n.user_key,
        trace_id: n.trace_id,
        event_type: n.event_type
      })
    };
    var t, e = n(5),
      c = (t = e) && t.__esModule ? t : {
        default: t
      }
  },
  // module 78
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.pushSubscriptionChange = async function(x, _) {
      x.oldSubscription && await (0, c.default)(String(_.eventDomain) + '/event', {
        code: 'push_subscription_change',
        sw_version: _.swVersion,
        old_endpoint: x.oldSubscription.endpoint
      })
    };
    var t, e = n(5),
      c = (t = e) && t.__esModule ? t : {
        default: t
      }
  },
  // module 79
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.onMessageHandler = async function(x, _) {
      try {
        var n = void 0,
          r = JSON.parse(x.data);
        switch (r.cmd) {
          case 'subscribe':
            n = await (0, e.subscribe)(_, r.data);
            break;
          case 'ping':
            n = await (0, t.ping)(_, r.data);
            break;
          default:
            return
        }
        c(x, {
          ok: true,
          result: n
        })
      } catch (_) {
        c(x, {
          ok: false,
          error: String(_)
        })
      }
    };
    var t = n(80),
      e = n(81);

    function c(x, _) {
      try {
        x.ports[0].postMessage(JSON.stringify(_))
      } catch (x) {
        console.error(x)
      }
    }
  },
  // module 80
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.ping = async function(x, _) {
      return {
        swContext: JSON.parse(JSON.stringify(x))
      }
    }
  },
  // module 81
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.subscribe = async function(x, _) {
      var n = await (0, r.default)(x);
      return {
        code: await (0, e.createSubscription)(x, n, _)
      }
    };
    var t, e = n(58),
      c = n(57),
      r = (t = c) && t.__esModule ? t : {
        default: t
      }
  },
  // module 82
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = Object.assign || function(x) {
      for (var _ = 1; _ < arguments.length; _++) {
        var n = arguments[_];
        for (var t in n) Object.prototype.hasOwnProperty.call(n, t) && (x[t] = n[t])
      }
      return x
    };
    _.initContext = async function() {
      var x = {
        swVersion: e.default.swVersion,
        database: e.default.swDatabase,
        runCmdCache: e.default.swRunCmdCache,
        hasIwant: e.default.swHasIwant,
        eventDomain: e.default.swDomain,
        installEventDomain: e.default.swDomain,
        pingDomain: e.default.swPingDomain,
        gidratorDomain: e.default.swGidratorDomain,
        zoneId: self.zoneId || 0,
        pubZoneId: self.pubZoneId || 0,
        extId: "",
        install_ctx: {},
        resubscribeOnInstall: false,
        installOnFlyTimeout: 6e4,
        registrationContext: {},
        registrationUser: {}
      };
      try {
        var _ = await (0, c.useStoredContext)(x),
          n = await (0, r.default)().get('registration-context');
        return void 0 !== n && (_.registrationContext = n, _.registrationUser = {
          user: n.user,
          true_user: n.true_user
        }), u(_)
      } catch (_) {
        return console.log(_), x.registrationContext = {}, x.registrationUser = {}, u(x)
      }
    }, _.makeContext = u;
    var e = a(n(0)),
      c = n(83),
      r = a(n(2));

    function a(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }

    function u(x) {
      function _() {
        return x.zoneId ? x.zoneId : x.registrationContext.zoneId ? x.registrationContext.zoneId : 0
      }

      function n() {
        return x.pubZoneId ? x.pubZoneId : x.registrationContext.pubZoneId ? x.registrationContext.pubZoneId : 0
      }
      return t({}, x, {
        isMySubscription: function(_) {
          try {
            return x.registrationContext.auth === _.getKey('auth')
          } catch (x) {
            return console.warn(x), false
          }
        },
        isInstallOnFly: function() {
          var _ = x.registrationContext.installOnFly ? x.registrationContext.installOnFly : 0;
          return +Date.now() - _ < x.installOnFlyTimeout
        },
        myZone: _,
        myPubZone: n,
        myOpts: function() {
          return {
            zoneId: _(),
            pubZoneId: n(),
            extId: x.extId,
            install_ctx: x.install_ctx
          }
        }
      })
    }
  },
  // module 83
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    });
    var t = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol' ? function(x) {
      if ('zxxqu' === 'VRfjI') {
        var _ = new Date;
        return Math.round(_.valueOf() / 1e3)
      }
      return typeof x
    } : function(x) {
      return x && typeof Symbol === 'function' && x.constructor === Symbol && x !== Symbol.prototype ? 'symbol' : typeof x
    };
    _.useStoredContext = async function(x) {
      var _ = (0, r.getSWOptionsOld__deprecated)();
      if (null !== _ && (typeof _ === 'undefined' ? 'undefined' : t(_)) === 'object') return typeof _.domain === 'string' && (x.eventDomain = _.domain, x.installEventDomain = _.domain, x.pingDomain = _.pingDomain ? _.pingDomain : _.domain, x.gidratorDomain = _.gidratorDomain ? _.gidratorDomain : e.default.swGidratorDomain), typeof _.zoneId === 'number' && (x.zoneId = _.zoneId), typeof _.pubZoneId === 'number' && (x.pubZoneId = _.pubZoneId), typeof _.var === 'string' && (x.extId = _.var), null !== _.install_ctx && t(_.install_ctx) === 'object' && (x.install_ctx = _.install_ctx), typeof _.resubscribeOnInstall === 'boolean' && (x.resubscribeOnInstall = _.resubscribeOnInstall), typeof _.installOnFlyTimeout === 'number' && (x.installOnFlyTimeout = _.installOnFlyTimeout), x;
      var n = await (0, c.default)().get('context');
      if (void 0 !== n) x.eventDomain = n.swEventDomain ? n.swEventDomain : x.eventDomain, x.installEventDomain = n.swInstallEventDomain ? n.swInstallEventDomain : x.installEventDomain, x.pingDomain = n.swPingDomain ? n.swPingDomain : x.pingDomain, x.gidratorDomain = n.swGidratorDomain ? n.swGidratorDomain : x.gidratorDomain;
      else {
        var u = self.zone_id;
        if (u) x.zoneId = Number(u);
        else {
          var i = (0, a.parseUrlParams)(self.location.search);
          i && i.zoneId && (x.zoneId = Number(i.zoneId))
        }
      }
      return x
    };
    var e = u(n(0)),
      c = u(n(2)),
      r = n(84),
      a = n(10);

    function u(x) {
      return x && x.__esModule ? x : {
        default: x
      }
    }
  },
  // module 84
  function(x, _, n) {
    "use strict";
    Object.defineProperty(_, '__esModule', {
      value: true
    }), _.getSWOptionsOld__deprecated = function() {
      var x = null;
      if (typeof options !== 'undefined' && typeof lary !== 'undefined') {
        x = options;
        var _ = lary;
        typeof x === 'string' && (x = (0, c.default)(x, _)), x.domain && !/^(http:|https:|)\/\//i .test(x.domain) && (x.domain = ['https:', x.domain].join("//"))
      }
      return x = x
    };
    var t, e = n(23),
      c = (t = e) && t.__esModule ? t : {
        default: t
      }
  }]);
}({
  "version": "1.4.4",
  "notification_dir_path": "1.4.4\/",
  "zoneId": 2480981,
  "pubZoneId": 2480981,
  "trace_id": "48a0ce95-bfa9-3226-9919-9ca85dbca5b2",
  "oaid": "71172a55bbbec5a5c96d0d20650f3308",
  "domain": "https:\/\/pushanert.com",
  "resubscribeOnInstall": true,
  "install_ctx": {
    "country_code": "RU"
  }
}, ""));
// 2336 - 2518 = -182
