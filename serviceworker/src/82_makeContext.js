/*
** Module 82
*/

import props from './00_options'; // e = a(n(0)),
import {default as trackDb} from './02_namedIndexedDB'; // r = a(n(2));
import useStoredContext from './83_useStoredContext'; // c = n(83),

export async function initContext() {
  var context = {
    swVersion: props.swVersion,
    database: props.swDatabase,
    runCmdCache: props.swRunCmdCache,
    hasIwant: props.swHasIwant,
    eventDomain: props.swDomain,
    installEventDomain: props.swDomain,
    pingDomain: props.swPingDomain,
    gidratorDomain: props.swGidratorDomain,
    zoneId: self.zoneId || 0,
    pubZoneId: self.pubZoneId || 0,
    extId: "",
    install_ctx: {},
    resubscribeOnInstall: false,
    installOnFlyTimeout: 6e4,
    registrationContext: {},
    registrationUser: {}
  };

  try {
    const storedContext = await useStoredContext(context);
    const savedDBContext = await trackDb().get('registration-context');

    if (savedDBContext) {
      storedContext.registrationContext = savedDBContext;
      storedContext.registrationUser = {
        user: savedDBContext.user,
        true_user: savedDBContext.true_user
      };
    }

    return makeContext(storedContext);
  } catch (error) {
    console.log(error);

    context.registrationContext = {};
    context.registrationUser = {};

    return makeContext(context);
  }
};

export function makeContext(context) {
  function getZoneId() {
    return context.zoneId || context.registrationContext.zoneId || 0;
  }

  function getPubZoneId() {
    return context.pubZoneId || context.registrationContext.pubZoneId || 0;
  }

  return Object.assign({}, context, {
    isMySubscription: function(subscription) {
      try {
        return context.registrationContext.auth === subscription.getKey('auth');
      } catch (error) {
        console.warn(error);

        return false;
      }
    },
    isInstallOnFly: function() {
      const installOnFly = context.registrationContext.installOnFly ? context.registrationContext.installOnFly : 0;

      return +Date.now() - installOnFly < context.installOnFlyTimeout;
    },
    myZone: getZoneId,
    myPubZone: getPubZoneId,
    myOpts: function() {
      return {
        zoneId: getZoneId(),
        pubZoneId: getPubZoneId(),
        extId: context.extId,
        install_ctx: context.install_ctx
      };
    }
  })
};
