/*
** Module 10
*/

// полифилл деструктуризации массива, оставлю так. что бы меньше код править =)
let expandArr = function() {
  return function(x, c) {
    if (Array.isArray(x))
      return x;
    if (Symbol.iterator in Object(x))
      return function(x, c) {
        var a = [],
          _ = true,
          t = false,
          n = void 0;
        try {
          for (var e, r = x[Symbol.iterator](); !(_ = (e = r.next()).done) && (a.push(e.value),
              !c || a.length !== c); _ = true)
          ;
        } catch (x) {
          t = true,
            n = x
        } finally {
          try {
            !_ && r.return && r.return()
          } finally {
            if (t)
              throw n
          }
        }
        return a
      }(x, c);
    throw new TypeError('Invalid attempt to destructure non-iterable instance')
  }
}();

export function parseUrlParams(url) {
  return (url || '')
    .split('&')
    .filter(Boolean)
    .map(function(x) {
      return x.split('=')
    })
    .reduce(function(x, c) {
      var a = expandArr(c, 2), // destructor?
        t = a[0],
        n = a[1];
      return x[decodeURIComponent(t)] = typeof(n) !== 'undefined' ? decodeURIComponent(n) : void 0,
        x
    }, {});
};

// парсит URL в формате http(s)://blablabla.com/context/id?q1=1&q2=asdasd#1243959303
export function splitURL(url) {
  var c = url.split('#', 2),
    a = expandArr(c, 2), // destructor?
    t = a[0],
    n = a[1],
    e = t.split('?').slice(0, 2),
    r = expandArr(e, 2); // destructor?
  return {
    uri: r[0],
    query: r[1],
    hash: n
  }
};

export function addParams(url, params) {
  const parsedURL = splitURL(url);
  const uri = parsedURL.uri;
  const query = parsedURL.query;
  const hash = parsedURL.hash;
  const parseUrlParams = parseUrlParams(query);

  const entries = Object.assign({}, parseUrlParams, params);

  const newQuery = Object.entries(entries).map(function(entry) {
    var c = expandArr(entry, 2),
      pName = c[0],
      pValue = c[1];
    if (typeof(pValue) === 'undefined')
      return '' + encodeURIComponent(pName);
    var str = String(pValue);
    return encodeURIComponent(pName) + '=' + encodeURIComponent(str);
  }).join('&');

  return (uri || '') + (newQuery.length > 0 ? '?' : '') + newQuery + (hash ? '#' + hash : '')
}
