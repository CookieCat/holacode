/*
** Module 11
*/

export default function browsersDetection(userAgent) {
  var c = (userAgent.match(/Chrome\/([0-9]+)/) || [])[1] + 0 || (userAgent.match(/CriOS\/([0-9]+)/) || [])[1] + 0,
    a = /iPhone|iPad|iPod/.test(userAgent),
    _ = /android/i.test(userAgent),
    t = a || _,
    n = !t,
    e = /YaBrowser/.test(userAgent),
    r = Boolean(c && !e),
    i = parseFloat((userAgent.match(/Android\s([0-9.]*)/) || [])[1]) < 4,
    o = -1 !== userAgent.indexOf('Opera Mini'),
    u = /FBAV\//i.test(userAgent),
    d = -1 !== userAgent.indexOf('MSIE');
  return {
    chromeVersion: c,
    isIOS: a,
    isAndroid: _,
    isMobile: t,
    isDesktop: n,
    isYandexBrowser: e,
    isChrome: r,
    isShittyAndroid: i,
    isOperaMini: o,
    isFacebookBrowser: u,
    oldIE: d
  };
};
