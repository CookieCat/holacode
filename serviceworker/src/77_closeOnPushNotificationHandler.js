/*
** Module 77
*/

import fetch from './05_fetch'; // c

export default async function closeOnPushNotificationHandler(x, options) {
  const notificationData = x.notification.data;
  const eventDomain = n.eventDomain || options.eventDomain;

  await fetch(String(eventDomain) + '/event', {
    code: 'close',
    sw_version: options.swVersion,
    user_key: notificationData.user_key,
    trace_id: notificationData.trace_id,
    event_type: notificationData.event_type
  });
};
