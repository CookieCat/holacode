noTitle/*
** Module 59
*/

import {default as trackDB} from './02_namedIndexedDB'; // e = i(n(2)),
import constants from './06_constants'; // r = i(n(6)),
import parseErrorMessage from './07_parseErrorMessage'; // a = i(n(7)),
import {default as jsonRestFn} from './14_fetchJson'; // c = i(n(14)),
import {
  addShowNotificationMetric,
  getDurationForPromise,
  addIwantShowMetric
} from './60_metricsHelper'; // u = n(60);

export default function(data) {
  var payload = data.payload,
    parsedError = data.originalPayload,
    swContext = data.swContext,
    userKey = data.userKey,
    afterIwant = data.afterIwant,
    fallbackType = data.fallbackType,
    prefetchSummary = data.prefetchSummary,
    flags = data.flags,
    registration = self.registration;

  if (!payload)
    throw new Error('showNotification() requires payload');

  var noTitle = false,
    noBody = false;

  typeof(payload.title) === 'string' && payload.title.trim().length || (noTitle = true);
  typeof(payload.options) === 'object' && payload.options.body && payload.options.body.trim().length || (noBody = true);

  if (noTitle && noBody)
    throw new Error('payload.empty-title-and-body');

  var title = payload.title ? payload.title : "",
    options = payload.options ? payload.options : {},
    noLastMessage = !payload.title || !payload.options || payload.is_empty,
    event_type = null !== fallbackType ? fallbackType : constants.NORMAL,
    w = afterIwant ? "iwant-show" : "event"; // wtf?!

  if (event_type === constants.NORMAL) {
    swContext.current_trace_id = payload.trace_id;
  }

  options.data
    ? (
      options.data.trace_id = payload.trace_id,
      options.data.user_key = userKey,
      options.data.event_type = event_type,
      options.data.eventDomain = swContext.eventDomain
    )
    : options.data = {
      trace_id: payload.trace_id,
      user_key: userKey,
      event_type: event_type,
      eventDomain: swContext.eventDomain
    };

  var addShowNotificationMetricIsResolved = false,
    p = addShowNotificationMetric(registration.showNotification(title, options), {
      originalPayload: parsedError,
      options: options,
      title: title
    }),
    Y = getDurationForPromise(p);

  return p
    .then(function(x) {
      addShowNotificationMetricIsResolved = true;
      return noLastMessage
        ? x
        : trackDB().set('last_message', parsedError || payload).catch(function() {})
    })
    .then(function() {
      return Y.then(function(duration) {
        return addIwantShowMetric(jsonRestFn(String(swContext.eventDomain) + "/" + w, {
          code: 'show',
          sw_version: swContext.swVersion,
          user_key: userKey,
          trace_id: payload.trace_id,
          after_iwant: afterIwant,
          event_type: event_type,
          zone_id: swContext.myZone(),
          duration: duration,
          prefetchSummary: prefetchSummary,
          flags: flags
        })).then(async function(metrics) {
          try {
            if (flags && flags.showStoredMessagesCount && void 0 !== flags.showStoredMessagesTtl) {
              var unresolvedMetrics = await addShowNotificationMetric.getUnresolved({
                ttl: 60 * flags.showStoredMessagesTtl * 1e3,
                count: flags.showStoredMessagesCount
              });

              unresolvedMetrics.forEach(async function(metric) {
                if (metric.data && metric.data.originalPayload) {
                  var metricsData = metric.data,
                    originalPayload = metricsData.originalPayload,
                    title = metricsData.title,
                    options = metricsData.options;

                  if (originalPayload.trace_id !== payload.trace_id) {
                    var notifications = await registration.getNotifications(),
                      traceIDs = notifications.every(function(notification) {
                        const data = notification.data;
                        return data.trace_id !== originalPayload.trace_id;
                      });

                    if (traceIDs) {
                      registration
                        .showNotification(title, options)
                        .then(function() {
                          jsonRestFn(String(swContext.eventDomain) + "/" + w, {
                            code: 'show',
                            sw_version: swContext.swVersion,
                            user_key: userKey,
                            trace_id: originalPayload.trace_id,
                            after_iwant: afterIwant,
                            event_type: constants.DEFERED_MSG,
                            zone_id: swContext.myZone()
                          })
                        })
                        .then(function() {
                          return addShowNotificationMetric.resolveMetric(metric, {
                            duration: Number(new Date) - metric.tsStart,
                            failed: false
                          })
                        });
                    }
                  }
                }
              })
            }
          } catch (error) {
            console.warn(error)
          }

          return metrics;
        })
      })
    }).catch(function(error) {
      const parsedError = parseErrorMessage(error, {
        user_key: userKey,
        after_iwant: afterIwant,
        event_type: event_type,
        trace_id: payload.trace_id
      }),
      errorKind = addShowNotificationMetricIsResolved ? "#sne-notify" : "#sne-show";

      return jsonRestFn(String(swContext.eventDomain) + '/event', {
        code: constants.ERROR_REPORT,
        error_message: 'showNotification error: kind:' + errorKind + ' error:' + parsedError.message,
        after_iwant: afterIwant,
        error_stack: parsedError.stack,
        error_source_message: 'title: ' + title + ', options: ' + JSON.stringify(options),
        sw_version: swContext.swVersion,
        user_key: userKey,
        trace_id: payload.trace_id
      })
      .catch(function() {})
      .then(function() {
        if (!addShowNotificationMetricIsResolved) throw error;
        return {};
      });
    });
};
