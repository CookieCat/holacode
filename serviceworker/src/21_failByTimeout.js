/*
** Module 21
*/

export const TIMEOUT_ERROR = new Error('TIMEOUT_ERROR');

export default function failByTimeout(timeout) {
  return new Promise(function(resolve, reject) {
    return setTimeout(function() {
      return reject(TIMEOUT_ERROR)
    }, timeout);
  })
};
