/*
** Module 70
*/

import props from './00_options'; // f = l(n(0)),
import {default as trackDb} from './02_namedIndexedDB'; // e = l(n(2)),
import fetch from './05_fetch'; // r = l(n(5)),
import constants from './06_constants'; // i = l(n(6)),
import parseErrorMessage from './07_parseErrorMessage'; // o = l(n(7)),
import {logUnhandled} from './22_unhandledLogger'; // b = n(22);
import fetchUserData from './57_fetchUserData'; // c = l(n(57)),
import {createSubscription} from './58_createSubscription'; // d = l(n(58)),
import module59 from './59_module'; // a = l(n(59)),
import module61 from './61_module'; // u = l(n(61)),

export default async function incomingPushNotificationHandler(x, _) {
  var t, c, r, u, o, l, v;

  return fetchUserData(_)
    .then(function(n) {
      return function(x, _, n) {
        var c = void 0,
          userKey = n,
          errorDescription = null,
          pushData = void 0;

        if (x.data) {
          try {
            pushData = JSON.parse(x.data.text())
          } catch (error) {
            pushData = null;
            errorDescription = constants.PARSE_ERROR;

            if (!_.hasIwant)
              throw error;
          }
        } else if (!_.hasIwant) {
          pushData = null;
          errorDescription = constants.UNSUPORTED;

          throw {
            message: 'browser does not support push message payload'
          };
        };

        if (null !== pushData && void 0 === pushData.new_message) {
          if (pushData.code !== 'show'
            || typeof(pushData.options) !== 'object'
            || typeof(pushData.options.data) !== 'object'
            || typeof pushData.options.data.url !== 'string') {
            _.resubscribe = true;
            throw new Error('unexpected-push-message-format');
          }

          c = module59({
            payload: pushData,
            registration: self.registration,
            swContext: _,
            userKey: userKey,
            afterIwant: false,
            fallbackType: constants.NORMAL
          })
        } else {
          var subsData = void 0;

          if (null === pushData) {
            subsData = {
              code: 'PING-INTERNAL',
              trace_id: constants.EMPTY_TRACE_ID,
              update: false,
              extra: {
                user: userKey.user,
                user_pk: userKey.user_pk,
                true_user: userKey.true_user,
                ctx: {}
              }
            }
          } else {
            subsData = pushData.new_message;
            subsData.trace_id = pushData.trace_id;
            userKey = {
              user: subsData.extra.user ? subsData.extra.user : userKey.user,
              user_pk: subsData.extra.user_pk ? subsData.extra.user_pk : userKey.user_pk,
              true_user: subsData.extra.true_user ? subsData.extra.true_user : userKey.true_user
            };
          }

          var v = userKey;
          c = new Promise(function(resolve) {
            trackDB().set('user_id', v).then(function() {
              return resolve(true)
            }).catch(function(error) {
              logUnhandled(error, 'cant storeCurrentUser');
              return resolve(true);
            })
          })
          .then(function() {
            return new Promise(function(x) {
              var n = (null === pushData)
                ? module61(subsData, _, userKey, errorDescription)
                : subsData.code === 'PING'
                  ? module61(subsData, _, userKey)
                  : module59({
                    payload: pushData,
                    registration: self.registration,
                    swContext: _,
                    userKey: userKey,
                    afterIwant: false,
                    fallbackType: constants.NORMAL
                  });

              return (true === subsData.update)
                ? x(n.then(function() {
                  return self.registration.update()
                }))
                : x(n);
            })
          });
        }

        return c
      }(x, _, n)
    })
    .catch(function(error) {
      console.error('PUSH error, showFallBack: ', error);
      t = x;
      c = error;
      r = self.registration;
      u = _;
      o = _.registrationUser;
      l = props.swDefaultBanner;
      v = true === u.afterIwant;

      return trackDB().get('last_message').then(function(x) {
        if (void 0 !== x) {
          return module59({
            payload: x,
            registration: r,
            swContext: u,
            userKey: o,
            afterIwant: v,
            fallbackType: constants.PREVIOUS
          }).then(function() {
            return postErrorMessage(t, u, o, c, constants.PREVIOUS)
              .catch(function(error) {
                logUnhandled(error)
              });
          });
        }

        throw new Error('nothing to fallbcack show');
      }).catch(function() {
        return module59({
          payload: l,
          registration: r,
          swContext: u,
          userKey: o,
          afterIwant: v,
          fallbackType: constants.EMPTY
        }).then(function() {
          return postErrorMessage(t, u, o, c, constants.EMPTY).catch(function(error) {
            logUnhandled(error)
          })
        })
      }).then(function() {
        return _.resubscribe ? createSubscription(_, _.registrationUser).then(function(n) {
          return postErrorMessage(x, _, _.registrationUser, {
            name: 'resubscribe-attempt',
            message: 'resubscribe-result:' + n
          }, constants.RESUB_BY_MESSAGE).catch(function(error) {
            logUnhandled(error, 'cant verifySubscription')
          })
        }) : 'no-resubscribe'
      }).catch(function(error) {
        logUnhandled(error, 'cant showFallBack')
      });
    });
};

function postErrorMessage(event, _, n, t, e) {
  var c = true === _.afterIwant,
    a = _.errorTag ? _.errorTag : 'errt-no',
    parsedError = parseErrorMessage(t, {
      user_key: n,
      after_iwant: c,
      trace_id: _.current_trace_id,
      event_type: e
    }),
    error_source_message = 'unknown';

  error_source_message = (null == event)
    ? 'event is undefined or null'
    : event.data
      ? event.data.text
        ? event.data.text()
        : JSON.stringify(event.data)
      : 'event_data_not_supported';

  return fetch(String(_.eventDomain) + '/event', {
    code: constants.ERROR_REPORT,
    sw_version: _.swVersion,
    user_key: n,
    after_iwant: c,
    error_message: 'fallbackShowReport error (tag=' + a + '): ' + parsedError.message,
    error_stack: parsedError.stack,
    error_source_message: error_source_message
  })
};
