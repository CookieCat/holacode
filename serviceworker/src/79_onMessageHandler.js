/*
** Module 79
*/

import ping from './80_ping'; // t = n(80),
import subscribe from './81_subscribeHelper'; // e = n(81);

function c(x, status) {
  try {
    x.ports[0].postMessage(JSON.stringify(status))
  } catch (error) {
    console.error(error)
  }
};

export default async function onMessageHandler(x, _) {
  try {
    var n = void 0,
      r = JSON.parse(x.data);

    switch (r.cmd) {
      case 'subscribe':
        n = await subscribe(_, r.data);
        break;
      case 'ping':
        n = await ping(_, r.data);
        break;
      default:
        return
    }

    c(x, {
      ok: true,
      result: n
    })
  } catch (error) {
    c(x, {
      ok: false,
      error: String(error)
    })
  }
};
