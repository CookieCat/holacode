/*
** Module 58
*/

import props from './00_options'; // c = f(n(0)),
import {default as getData} from './04_getData'; // a = f(n(4)),
import parseErrorMessage from './07_parseErrorMessage'; // u = f(n(7)),
import {default as setRegistrationContext} from './19_setRegistrationContext'; // r = f(n(19)),
import {HttpClient} from './30_httpClient'; // o = n(30);
import areTheSameSubscriptions from './32_areTheSameSubscriptions'; // i = n(32),
import pushManagerSubscriptionsHelper from './33_pushManagerSubscriptionsHelper'; // e = f(n(33)),

export async function createSubscription(subsData, _) {
  var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
    a = self.registration,
    u = httpClient(subsData.eventDomain),
    f = await self.registration.pushManager.getSubscription(),
    d = await u.getApplicationServerKey(),
    b = d.applicationServerKey,
    l = d.key_id,
    s = await pushManagerSubscriptionsHelper(a.pushManager, {
      userVisibleOnly: true,
      applicationServerKey: b
    });

  if (areTheSameSubscriptions(s, f) || subsData.isMySubscription(s))
    return 'my-subscription-fcm';

  var user = _.user,
    true_user = _.true_user,
    m = s.toJSON(),
    Z = {
      status: Notification.permission,
      from_install: true,
      prev_auth: f ? f.toJSON() : void 0,
      sw_version: props.swVersion,
      install_ctx: subsData.myOpts().install_ctx,
      creative: {
        domain: location.hostname,
        location: location.href,
        land_id: subsData.landId,
        zone_id: +subsData.myZone(),
        pub_zone_id: +subsData.myPubZone(),
        ext_id: subsData.myOpts().extId
      },
      key_id: l,
      endpoint: m.endpoint,
      auth: m.keys.auth,
      p256dh: m.keys.p256dh
    };

  if (_.gidratorResponse && true === _.gidratorResponse.ok) {
    user = _.gidratorResponse.gidratorOAID;
    true_user = _.gidratorResponse.gidratorOAID;
  }

  if (user !== true_user && true_user) { // wtf?!
    user = true_user
  }

  await setRegistrationContext({
    user: user,
    true_user: true_user,
    zoneId: subsData.myZone(),
    pubZoneId: subsData.myPubZone(),
    registration_hostname: location.hostname,
    domain: subsData.eventDomain,
    auth: Z.auth
  });

  await u.subscribe(Object.assign({}, Z, {
    user: user,
    true_user: true_user,
    sw_version: subsData.swVersion
  }, n));

  return 'resubscribed-ok';
};

export default async function(data, user_key) {
  try {
    var n = self.registration,
      t = Notification.permission;

    if (!n || !n.pushManager || typeof n.pushManager.getSubscription !== 'function')
      return 'invalid-sw-registration';

    var e = await n.pushManager.getSubscription();

    return data.isInstallOnFly()
      ? 'install-on-fly'
      : true !== data.resubscribeOnInstall
        ? 'is-not-enabled-for-zone'
        : t !== 'granted'
          ? 'permisssions-is-not-granted'
          : null !== e && data.isMySubscription(e)
            ? 'my-subscription'
            : createSubscription(data, user_key);
  } catch (error) {
    var c = parseErrorMessage(error),
      description = 'error_resubscribe user: zone: ' + data.myZone() + ' pubZoneId: ' + data.myPubZone() + ' ext_id: ' + data.myOpts(),
      errorDescription = description + ', error: ' + c.message;

    await getData(data.eventDomain, description, error, {
      user_key: user_key
    });

    throw Error(errorDescription)
  }
};
