/*
** Модуль 1
*/
import xhrReqFactory from './12_xhrReqFactory';
import {default as getData} from './04_getData';

export default class Logger {
  setContext(url, options) {
      let timeout = (arguments.length > 2 && arguments[2] !== void 0) ? arguments[2] : 5000;

      if (url.indexOf('?') > -1) {
        this.url = options.trace_id ? url + '&tid=' + options.trace_id : url;
      } else {
        this.url = options.trace_id ? url + '?tid=' + options.trace_id : url;
      }

      this.options = options,
      this.timeout = timeout
  }

  updateContext(option) {
    this.options = Object.assign({}, this.options, option)
  }

  setUserId(oaid) {
    this.options.oaid = oaid
  }

  send(addOptions, url) {
    if (!this.url) {
      console.log(addOptions, url);
      return Promise.resolve();
    }

    var context = Object.assign({}, this.options, addOptions);

    xhrReqFactory(this.url).post(context).catch(function(error) {
      console.warn('event-logger-error: ', error)
      if (url)
        return getData(url, 'event-logger-error:', error, context)
    });

    return Promise.resolve();
  }
};

export const eventLogger = new Logger();

// объект options
//
// domain: "f.robotcaptcha.info"
// geo: "RU"
// install_ctx:
// country_code: "RU"
// __proto__: Object
// ip: "178.219.43.36"
// location: "https://f.robotcaptcha.info/player/index.html?cid={click_id"
// oaid: "8ef84686c562190eadc307445da36c6d"
// popup_id: ""
// pub_zone_id: 2480981
// request_var: "null"
// skin_id: ""
// sw_version: "3.1.14"
// trace_id: "8aa75e93-9757-3a4d-95e3-f40f806e1b6e"
// ymid: "{click_id}"
// zone_id: 2480981
