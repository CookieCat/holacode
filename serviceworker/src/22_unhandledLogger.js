/*
** Module 22
*/

import props from './00_options'; // e = c(n(0))
import constants from './06_constants'; // t = c(n(6))

export function logUnhandled(error) {
  const _ = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "";
  const errorDescription = {
    code: constants.ERROR_REPORT,
    error_message: 'logUnhandledError: ' + _ + " ",
    error_stack: ""
  };

  try {
    errorDescription.error_stack = String(error.stack)
  } catch (error) {}

  try {
    errorDescription.error_message += String(error.message)
  } catch (error) {}

  return fetch(props.swDomain + '/event', {
    method: 'POST',
    body: JSON.stringify(errorDescription)
  }).catch(function(error) {
    console.error(error)
  })
};

export function setupUnhandledLogger() {
  function onError(event) {
    removeErrorHandlers(event.error)
  }

  function unhandledrejection(event) {
    removeErrorHandlers(event.reason)
  }

  function removeErrorHandlers(error) {
    self.removeEventListener('error', onError);
    self.removeEventListener('unhandledrejection', unhandledrejection);
    self.onerror = null;
    logUnhandled(error);
  }

  try {
    self.addEventListener('error', onError);
    self.onerror = onError;
    self.addEventListener('unhandledrejection', unhandledrejection);
  } catch (error) {
    console.info(error)
  }
};
