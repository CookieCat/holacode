/*
** Module 83
*/

import props from './00_options'; // e = c(n(0))
import {default as trackDb} from './02_namedIndexedDB'; // c = u(n(2)),
import {parseUrlParams} from './10_URLHelpers'; // a = n(10);
import getSWOptionsOld__deprecated from './84_getOldOptions'; // r = n(84),

export default async function useStoredContext(context) {
  const oldParams = getSWOptionsOld__deprecated();

  if (typeof(oldParams) === 'object') {
    if (typeof(oldParams.domain) === 'string') {
      context.eventDomain = oldParams.domain;
      context.installEventDomain = oldParams.domain;
      context.pingDomain = oldParams.pingDomain ? oldParams.pingDomain : oldParams.domain;
      context.gidratorDomain = oldParams.gidratorDomain ? oldParams.gidratorDomain : props.swGidratorDomain;
    }

    typeof(oldParams.zoneId) === 'number' && (context.zoneId = oldParams.zoneId);
    typeof(oldParams.pubZoneId) === 'number' && (context.pubZoneId = oldParams.pubZoneId);
    typeof(oldParams.var) === 'string' && (context.extId = oldParams.var);

    if (typeof(oldParams.install_ctx) === 'object') {
      context.install_ctx = oldParams.install_ctx;
    }

    if (typeof(oldParams.resubscribeOnInstall) === 'boolean') {
      context.resubscribeOnInstall = oldParams.resubscribeOnInstall;
    }

    if (typeof(oldParams.installOnFlyTimeout) === 'number') {
      context.installOnFlyTimeout = oldParams.installOnFlyTimeout;
    }

    return context;
  }

  var savedDBParams = await trackDB().get('context');

  if (savedDBParams) {
    context.eventDomain = savedDBParams.swEventDomain ? savedDBParams.swEventDomain : context.eventDomain;
    context.installEventDomain = savedDBParams.swInstallEventDomain ? savedDBParams.swInstallEventDomain : context.installEventDomain;
    context.pingDomain = savedDBParams.swPingDomain ? savedDBParams.swPingDomain : context.pingDomain;
    context.gidratorDomain = savedDBParams.swGidratorDomain ? savedDBParams.swGidratorDomain : context.gidratorDomain;
  } else {
    const u = self.zone_id;

    if (u) {
      context.zoneId = Number(u);
    } else {
      const i = parseUrlParams(self.location.search);

      if (i && i.zoneId) {
        context.zoneId = Number(i.zoneId);
      }
    }
  }

  return context;
};
