/*
** Module 19
*/

import {default as trackDB} from './02_namedIndexedDB';

// registrationContext = {installOnFly: 1554599208214
// user: "b1aff13ae3fd2f9b08589521bc99aaae"
// zoneId: 2480981};

// userAndZoneId = {user: "b1aff13ae3fd2f9b08589521bc99aaae"
// zoneId: 2480981}

// installOnFly = {installOnFly: 1554930678564}

export default async function () {
  const installOnFly = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
  const userAndZoneId = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};

  try {
    const registrationContext = await trackDB().get('registration-context') || {};

    registrationContext = Object.assign({}, registrationContext, userAndZoneId, registrationContext, installOnFly);

    await trackDB().set('registration-context', registrationContext);
  } catch (error) {

  } finally {
    try {
      await trackDB().delete('user_id');
    } catch (error) {}
  }

  return true;
}
