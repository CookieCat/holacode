/*
** Module 60
*/

import props from './00_options'; // r
import {default as openDb} from './09_iDB'; // a = n(9);

const getDeltaTime = function(x) {
  return Number(new Date) - x;
}
const i = 216e5; // wtf?!


export async function getDurationForPromise(promise) {
  var timestamp = performance.now();
  await promise;
  return parseInt(performance.now() - timestamp);
};


export function getMetric(x) {
  var db = function() {
      return openDb(Object.assign({}, props.swMetricsDb, {
        keyPath: 'tsStart'
      }));
    },
    n = async function(x, n) {
      try {
        var t = await db();

        x.duration = n.duration;
        x.failed = n.failed;

        return await t.put(x)
      } catch (error) {}
    },
    e = async function(t, e) {
      try {
        var c = await db(),
          r = performance.now(),
          a = {
            type: x,
            duration: 0,
            tsStart: Number(new Date)
          };
        e && (a.data = e), c.put(a).then(function() {
          return t.then(function() {
            return n(a, {
              duration: performance.now() - r,
              failed: false
            })
          }).catch(function() {
            return n(a, {
              duration: performance.now() - r,
              failed: true
            })
          })
        });
        c.delete(self.IDBKeyRange.upperBound(getDeltaTime(i)));
      } catch (error) {
        console.error(error);
      }

      return t;
    };

  e.resolveMetric = n;

  var c = async function() {
    try {
      var records = await db; // возможно, тут ошибка в коде - db не вызывается как функция?
      return await records.getAll()
    } catch (error) {
      return []
    }
  };

  e.getStat = async function() {
    try {
      var _ = await c(),
        n = getDeltaTime(i),
        metrics = _.filter(function(_) {
          return x === _.type && _.tsStart > n
        });

      return t.reduce(function(acc, metric) {
        acc.isLastResolved = typeof(metric.failed) !== 'undefined';
        acc.isLastResolved ? metric.failed && acc.failsCount++ : acc.unresolvedCount++;
        return acc;
      }, {
        failsCount: 0,
        unresolvedCount: 0,
        isLastResolved: true,
        totalCount: metrics.length
      });
    } catch (error) {
      console.error(error);

      return {
        failsCount: 0,
        unresolvedCount: 0,
        isLastResolved: true,
        totalCount: 0
      };
    }
  };
  e.getUnresolved = async function(x) {
    var n = await db();
    await n.delete(self.IDBKeyRange.upperBound(getDeltaTime(i)));
    return (await c())
      .filter(function(_) {
        return _.tsStart > getDeltaTime(x.ttl) && _.tsStart < getDeltaTime(9e4) && typeof _.failed === 'undefined' && Boolean(_.data)
      })
      .slice(-x.count);
  };
  e.done = new Promise(function() {
    return null;
  });

  return e;
}

export const addIwantMetric = getMetric('iwant');
export const addIwantShowMetric = getMetric('iwant-show');
export const addShowNotificationMetric = getMetric('showNotification');
