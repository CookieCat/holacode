/*
** Module 68
*/

import props from './00_options'; // f -> o = n(0),
import serviceWorkerActivateHandler from './69_serviceWorkerActivateHandler'; // e = n(69),
import incomingPushNotificationHandler from './70_incomingPushNotificationHandler'; // c = n(70),
import clickOnPushNotificationHandler from './76_clickOnPushNotificationHandler'; // r = n(76),
import closeOnPushNotificationHandler from './77_closeOnPushNotificationHandler'; // a = n(77),
import pushSubscriptionChange from './78_pushSubscriptionChange'; // u = n(78),
import onMessageHandler from './79_onMessageHandler'; // i = n(79),

export const swEventsHandlers = {
  activate: serviceWorkerActivateHandler,
  push: incomingPushNotificationHandler,
  notificationclick: clickOnPushNotificationHandler,
  notificationclose: closeOnPushNotificationHandler,
  pushsubscriptionchange: pushSubscriptionChange,
  message: onMessageHandler,
  version: props.swVersion
};
