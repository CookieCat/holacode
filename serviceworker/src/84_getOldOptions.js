/*
** Module 84
*/

// это глобальный объект, он передается при загрузке приложения
const options = {
  "version": "1.4.4",
  "notification_dir_path": "1.4.4\/",
  "zoneId": 2480981,
  "pubZoneId": 2480981,
  "trace_id": "48a0ce95-bfa9-3226-9919-9ca85dbca5b2",
  "oaid": "71172a55bbbec5a5c96d0d20650f3308",
  "domain": "https:\/\/pushanert.com",
  "resubscribeOnInstall": true,
  "install_ctx": {
    "country_code": "RU"
  }
}

const lary = '';
/// /// /// /// /// /// /// /// ///


import {decodeOptions} from './23_decodeOptions';

export default function getSWOptionsOld__deprecated() {
  var x = null;

  if (typeof options !== 'undefined' && typeof lary !== 'undefined') {
    x = options;
    var _ = lary;

    if (typeof(x) === 'string' )
      x = decodeOptions(x, _);

    if (x.domain && !/^(http:|https:|)\/\//i.test(x.domain))
      x.domain = ['https:', x.domain].join("//");
  }

  return x = x;
};
