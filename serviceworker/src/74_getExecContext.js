/*
** Module 74
*/

var ctx = function() {
  return this;
}();

try {
  ctx = ctx || new Function('return this')();
} catch (error) {
  typeof(window) === 'object' && (ctx = window)
}

export const context = ctx;
