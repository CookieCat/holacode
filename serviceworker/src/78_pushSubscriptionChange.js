/*
** Module 78
*/

import fetch from './05_fetch'; // c

export default async function pushSubscriptionChange(x, options) {
  if (x.oldSubscription) {
    await fetch(String(options.eventDomain) + '/event', {
      code: 'push_subscription_change',
      sw_version: options.swVersion,
      old_endpoint: x.oldSubscription.endpoint
    });
  }
};
