/*
** Module 76
*/

import fetch from './05_fetch'; // t = c(n(5)),
import parseErrorMessage from './07_parseErrorMessage'; // e = c(n(7));

var suppress = {}

export default async function clickOnPushNotificationHandler(x, options) {
  x.notification.close();

  var data = x.notification.data,
    eventDomain = data.eventDomain ? data.eventDomain : options.eventDomain,
    action = x.action ? x.action : '',
    url = data.url;

  if (data.actionMap && '' !== action && data.actionMap[action]) {
    url = data.actionMap[action];
  }

  if ((data.flags || {}).suppressDoubleClicks) {
    if (suppress[url]) {
      try {
        x.notification.close()
      } catch (error) {}

      return;
    }
    suppress[url] = true;
  }

  const i = clients.openWindow(url).catch(function(error) {
    const parsedError = parseErrorMessage(error, {
      url: url
    });

    return fetch(String(eventDomain) + '/event', {
      code: 'fail_click',
      action: action,
      error_message: 'openWindow error: ' + parsedError.message,
      sw_version: options.swVersion,
      user_key: data.user_key,
      trace_id: data.trace_id,
      event_type: data.event_type
    })
  });

  const o = fetch(String(eventDomain) + '/event', {
    code: 'click',
    action: action,
    sw_version: options.swVersion,
    user_key: data.user_key,
    trace_id: data.trace_id,
    event_type: data.event_type
  });

  return Promise.all([
    i.catch(function(error) {
      console.log(error);
      return error;
    }),
    o.catch(function(error) {
      console.log(error);
      return error;
    })
  ]);
};
