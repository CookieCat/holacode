/*
** Module 81
*/
import fetchUserData from './57_fetchUserData'; // r -> c = n(57),
import {createSubscription} from './58_createSubscription'; // e = n(58),

export default async function subscribe(subsData, _) {
  const userId = await fetchUserData(subsData);

  return {
    code: await createSubscription(subsData, userId, _)
  }
};
