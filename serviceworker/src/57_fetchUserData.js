/*
** Module 57
*/

import {default as trackDB} from './02_namedIndexedDB'; // t = c(n(2)),
import fetchGidratorResponse from './16_fetchGidratorResponse'; // e = c(n(16));

export default async function(ctx) {
  const isTrueUser = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
  const data = {
    user: ctx.registrationContext.user,
    true_user: ctx.registrationContext.true_user
  };

  try {
    var userFromDB = await trackDB().get('user_id');

    if (userFromDB && userFromDB.true_user)
      return userFromDB;

    if (userFromDB && userFromDB.true_user === void 0 && data.true_user) {
      userFromDB.true_user = data.true_user;
      data = userFromDB;
    }

    if (true !== isTrueUser && void 0 === data.true_user) {
      try {
        const response = await fetchGidratorResponse(ctx.gidratorDomain, data.user, false);

        data.gidratorResponse = response;

        if (response.ok) {
          data.true_user = response.gidratorOAID;
        }
      } catch (error) {
        console.log('gidrartor error, status:', error)
      }
    }

    await trackDB().set('user_id', data);

    return data;
  } catch (error) {
    console.log(error);
    return data;
  }
}
