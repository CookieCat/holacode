/*
** Module 12
*/

export default function (url, async) {
  return new xhrReqFn(url, async);
}

class xhrReqFn {
  constructor(url) {
    const async = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];

    this.xhr = new XMLHttpRequest;
    this.url = url;
    this.async = async;
  }

  open(method) {
    this.xhr.open(method, this.url, this.async);
    this.xhr.setRequestHeader('content-type', 'application/json');
  }

  send() {
    const that = this;
    const data = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null;
    const body = null !== data ? JSON.stringify(data) : null;

    return new Promise(function(resolve, reject) {
      that.xhr.onload = function() {
          if (200 !== that.xhr.status && 201 !== that.xhr.status) {
            var error = new Error(that.xhr.statusText);
            return reject(error);
          }
          try {
            var response = JSON.parse(that.xhr.response);
            return response.status ? resolve(response) : reject(response);
          } catch (error) {
            return reject(error);
          }
        },

        that.xhr.onerror = function() {
          reject(new Error('Network Error'));
        },

        that.xhr.send(body)
    });
  }

  get() {
    this.xhr.open('GET', this.url);

    return this.send();
  }

  post(data) {
    this.open('POST');

    return this.send(data);
  }
};
