/*
** Module 69
*/

import fetch from './05_fetch'; // e = r(n(5)),
import fetchUserData from './57_fetchUserData'; // t = r(n(57)),
import {default as createSubscription} from './58_createSubscription'; // c = r(n(58));

export default async function serviceWorkerActivateHandler(event, ctx) {
  try {
    await self.clients.claim();
  } catch (error) {}

  try {
    var userId = await fetchUserData(ctx);

    console.log('install service worker', ctx.swVersion, 'userId ->', userId);

    var r = await createSubscription(ctx, userId);

    await fetch(ctx.installEventDomain + '/event', {
      code: 'install',
      sw_version: ctx.swVersion,
      user_key: userId,
      zone_id: +ctx.myZone(),
      pub_zone_id: +ctx.myPubZone(),
      ext_id: ctx.myOpts().extId,
      result_status: r
    })
  } catch (error) {

  } finally {
    self.skipWaiting();
  }
};
