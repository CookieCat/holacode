/*
** Module 73
*/

import {context} from './74_getExecContext';

//
// функция возвращает объект следующей структуры:
//
// Base64: {
//   VERSION: version,
//   atob: atob,
//   btoa: btoa,
//   fromBase64: fromBase64,
//   toBase64: toBase64,
//   utob: utob,
//   encode: toBase64,
//   encodeURI: encodeURI,
//   btou: btou,
//   decode: fromBase64,
//   noConflict: noConflict,
//   __buffer__: Buffer
// };
//

// _0x47841a | thisContext -> ServiceWorkerGlobalScope
(function(thisContext) {
 var _0x12cd7d, _0x3016d2;
 ! function(context, _) {
   exports.exports = _(context);
 }(
  typeof self !== 'undefined'
    ? self
    : typeof window !== 'undefined'
      ? window
      : typeof thisContext !== 'undefined'
        ? thisContext
        : this
  ,
  function(thisContext) {
    "use strict";
    var Base64 = thisContext.Base64,
      version = '2.5.0',
      Buffer;

    if (exports.exports) try {
      Buffer = eval("require('buffer')['Buffer'];");
    } catch (error) {
      Buffer = void 0;
    }

    var _0x6b6e60 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/',
      _0x495549 = function(x) {
        for (var _ = {}, n = 0, t = x.length; n < t; n++) _[x.charAt(n)] = n;
        return _
      }(_0x6b6e60),
      fromCharCode = String.fromCharCode,
      _0xa9d52 = function(x) {
        if (x.length < 2) return (_ = x.charCodeAt(0)) < 128 ? x : _ < 2048 ? fromCharCode(192 | _ >>> 6) + fromCharCode(128 | 63 & _) : fromCharCode(224 | _ >>> 12 & 15) + fromCharCode(128 | _ >>> 6 & 63) + fromCharCode(128 | 63 & _);
        var _ = 65536 + 1024 * (x.charCodeAt(0) - 55296) + (x.charCodeAt(1) - 56320);
        return fromCharCode(240 | _ >>> 18 & 7) + fromCharCode(128 | _ >>> 12 & 63) + fromCharCode(128 | _ >>> 6 & 63) + fromCharCode(128 | 63 & _)
      },
      _0x2af6e1 = /[\uD800-\uDBFF][\uDC00-\uDFFFF]|[^\x00-\x7F]/g,
      utob = function(x) {
        return x.replace(_0x2af6e1, _0xa9d52)
      },
      _0x53cf53 = function(x) {
        var _ = [0, 2, 1][x.length % 3],
        n = x.charCodeAt(0) << 16 | (x.length > 1 ? x.charCodeAt(1) : 0) << 8 | (x.length > 2 ? x.charCodeAt(2) : 0);
        return [_0x6b6e60.charAt(n >>> 18), _0x6b6e60.charAt(n >>> 12 & 63), _ >= 2 ? "=" : _0x6b6e60.charAt(n >>> 6 & 63), _ >= 1 ? "=" : _0x6b6e60.charAt(63 & n)].join("")
      },
      btoa = thisContext.btoa
        ? function(x) {
          return thisContext.btoa(x)
        }
        : function(x) {
          return x.replace(/[\s\S]{1,3}/g, _0x53cf53)
        },
      _0x29a30b = Buffer
        ? Buffer.from && Uint8Array && Buffer.from !== Uint8Array.from
          ? function(x) {
            return (x.constructor === Buffer.constructor ? x : Buffer.from(x)).toString('base64')
          }
          : function(x) {
            return (x.constructor === Buffer.constructor ? x : new Buffer(x)).toString('base64')
          }
        : function(x) {
          return btoa(utob(x))
        },
      toBase64 = function(x, _) {
        return _
          ? _0x29a30b(String(x)).replace(/[+\/]/g, function(x) {
            return "+" == x ? "-" : "_"
          }).replace(/=/g, "")
          : _0x29a30b(String(x))
      },
      encodeURI = function(x) {
        return toBase64(x, true)
      },
      _0x3b4d74 = new RegExp(['[Ã-Ã][Â-Â¿]', '[Ã -Ã¯][Â-Â¿]{2}', '[Ã°-Ã·][Â-Â¿]{3}'].join("|"), "g"),
      _0x116574 = function(x) {
        switch (x.length) {
          case 4:
            var _ = ((7 & x.charCodeAt(0)) << 18 | (63 & x.charCodeAt(1)) << 12 | (63 & x.charCodeAt(2)) << 6 | 63 & x.charCodeAt(3)) - 65536;
            return fromCharCode(55296 + (_ >>> 10)) + fromCharCode(56320 + (1023 & _));
          case 3:
            return fromCharCode((15 & x.charCodeAt(0)) << 12 | (63 & x.charCodeAt(1)) << 6 | 63 & x.charCodeAt(2));
          default:
            return fromCharCode((31 & x.charCodeAt(0)) << 6 | 63 & x.charCodeAt(1))
        }
      },
      btou = function(x) {
        return x.replace(_0x3b4d74, _0x116574)
      },
      _0x5dd053 = function(x) {
        var _ = x.length,
          n = _ % 4,
          t = (_ > 0 ? _0x495549[x.charAt(0)] << 18 : 0) | (_ > 1 ? _0x495549[x.charAt(1)] << 12 : 0) | (_ > 2 ? _0x495549[x.charAt(2)] << 6 : 0) | (_ > 3 ? _0x495549[x.charAt(3)] : 0),
          e = [fromCharCode(t >>> 16), fromCharCode(t >>> 8 & 255), fromCharCode(255 & t)];
        return e.length -= [0, 0, 2, 1][n], e.join("")
      },
      _0x3d0a31 = thisContext.atob
        ? function(x) {
          return thisContext.atob(x)
        }
        : function(x) {
          return x.replace(/\S{1,4}/g, _0x5dd053)
        },
        atob = function(x) {
          return _0x3d0a31(String(x).replace(/[^A-Za-z0-9\+\/]/g, ""))
        },
        _0x4d6ed0 = Buffer
          ? Buffer.from && Uint8Array && Buffer.from !== Uint8Array.from
            ? function(x) {
              return (x.constructor === Buffer.constructor ? x : Buffer.from(x, 'base64')).toString()
            }
            : function(x) {
              return (x.constructor === Buffer.constructor ? x : new Buffer(x, 'base64')).toString()
            }
          : function(x) {
            return btou(_0x3d0a31(x))
          },
      fromBase64 = function(x) {
        return _0x4d6ed0(String(x).replace(/[-_]/g, function(x) {
          return "-" == x ? "+" : "/"
        }).replace(/[^A-Za-z0-9\+\/]/g, ""))
      },
      noConflict = function() {
        var x = thisContext.Base64;
        thisContext.Base64 = Base64;
        return x;
      };

    thisContext.Base64 = {
      VERSION: version,
      atob: atob,
      btoa: btoa,
      fromBase64: fromBase64,
      toBase64: toBase64,
      utob: utob,
      encode: toBase64,
      encodeURI: encodeURI,
      btou: btou,
      decode: fromBase64,
      noConflict: noConflict,
      __buffer__: Buffer
    };

    if (typeof Object.defineProperty === 'function') {
      var defineProp = function(x) {
        return {
          value: x,
          enumerable: false,
          writable: true,
          configurable: true
        }
      };
      thisContext.Base64.extendString = function() {
        Object.defineProperty(String.prototype, 'fromBase64', defineProp(function() {
          return fromBase64(this)
        }));
        Object.defineProperty(String.prototype, 'toBase64', defineProp(function(x) {
          return toBase64(this, x)
        }));
        Object.defineProperty(String.prototype, 'toBase64URI', defineProp(function() {
          return toBase64(this, true)
        }));
      }
    }

    thisContext.Meteor && (Base64 = thisContext.Base64);

    exports.exports
      ? exports.exports.Base64 = thisContext.Base64
      : (_0x12cd7d = [], _0x3016d2 = function() {
          return thisContext.Base64
        }.apply(module, _0x12cd7d), void 0 === _0x3016d2 || (exports.exports = _0x3016d2))

    return {
      Base64: thisContext.Base64
    };
  });
}).call(this, context);
