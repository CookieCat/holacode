/*
** Module 30
*/

import props from './00_options'; // t = c(n(0)),
import toUInt8Array from './31_toUInt8Array'; // e = c(n(31));

export function HttpClient() {
  var swDomain = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : props.swDomain;

  swDomain.startsWith('https://') || (swDomain = 'https://' + swDomain);

  async function executeReq(url, method, payload) {
    const response = await fetch(url, {
        method: method,
        credentials: 'include',
        body: payload ? JSON.stringify(payload) : void 0,
        headers: {
          'Content-Type': 'application/json'
        }
      });
    const text = await response.text();

    try {
      var json = JSON.parse(text);
      if (true !== json.status) {
        throw new Error(method + ': ' + url + '; body: ' + String(payload) + '; status: ' + String(c.status) + '; json: ' + JSON.stringify(json))
      }
      return json;
    } catch (c) {
      throw new Error(method + ': ' + url + '; body: ' + String(payload) + '; http-status: ' + response.status + '; responseText: ' + text + '; stack: ' + String(c.stack))
    }
  }

  return {
    subscribe: async function(payload) {
      return await executeReq(swDomain + '/subscribe', 'POST', payload)
    },
    getApplicationServerKey: async function() {
      const data = await executeReq(swDomain + '/key?id=' + location.hostname, 'GET');
      const applicationServerKey = toUInt8Array(data.key);

      return {
        key_id: data.id,
        key: data.key,
        applicationServerKey: applicationServerKey
      }
    }
  };
};
