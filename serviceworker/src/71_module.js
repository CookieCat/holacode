/*
** Module 71
*/

import {default as getData} from './04_getData'; // i = b(n(4)),
import fetch from './05_fetch'; // c = b(n(5)),
import constants from './06_constants'; // f = b(n(6)),
import parseErrorMessage from './07_parseErrorMessage'; // u = b(n(7)),
import {default as module59} from './59_module'; // r = b(n(59)),
import module61 from './61_module'; // o = b(n(61)),
import traceIdEncodeDecode from './72_traceIdEncodeDecode'; // a = b(n(72)),
import {prefetchResoursesWithTimeout} from './75_prefetchDataHelpers'; // d = n(75);

function l(payload, context, searchInCache) {
  var reqURL = new URL("" + payload.run_url);
  return true === searchInCache || caches.open(context.runCmdCache).then(function(cachedResponse) {
    return cachedResponse.delete(reqURL, {
      ignoreSearch: true,
      ignoreMethod: true,
      ignoreVary: true
    })
  }).catch(function() {
    return true;
  })
};

export default async function(x, payload, context, userKey) {
  var fallbackType = arguments.length > 4 && void 0 !== arguments[4] ? arguments[4] : null,
    flags = void 0;

  try {
    flags = payload.default_payload.options.data && payload.default_payload.options.data.flags || {};
  } catch (error) {
    flags = {}
  }

  var y = Promise.resolve(),
    originalPayload = Object.assign({}, payload.default_payload);

  if (flags.prefetchPushResources) {
    var options = payload.default_payload.options;
    y = prefetchResoursesWithTimeout(options, ['icon', 'image']).then(function(x) {
      var context = x.options,
        t = x.prefetchSummary;

      payload.default_payload.options = context;

      return t;
    });
  }

  if (!payload.run_url || !context.fallbackIwant){
    return y.then(function(prefetchSumary) {
      return module59({
        payload: payload.default_payload,
        originalPayload: originalPayload,
        swContext: context,
        userKey: userKey,
        afterIwant: true,
        fallbackType: fallbackType,
        prefetchSummary: prefetchSumary,
        flags: flags
      });
    });
  }

  var searchInCache = false,
    h = false;

  return y.then(function(prefetchSummary) {
    var d = payload,
      y = context,
      reqURL = new URL("" + d.run_url);

    return (caches.open(y.runCmdCache).then(function(cachedResponse) {
      return cachedResponse.match(reqURL).then(function(response) {
        return response || fetch(reqURL, {}, 'get').then(function(res) {
          if (200 !== res.status && 201 !== res.status)
            throw new Error('can-not-fetch-cs-script-no-200-status');

          cachedResponse.put(reqURL, res.clone());

          return res.clone();
        });
      })
    }))
    .then(function(response) {
      return response.text()
    })
    .then(function(responseFunctionContent) {
      return new Promise(function(resolve) {
        try {
          var ctx = {
            swContext: context,
            vars: payload.run_vars,
            helper: traceIdEncodeDecode()
          };
          ctx.vars.message_template = payload.default_payload;

          var timeout = payload.run_vars && typeof payload.run_vars.timeout === 'number'
            ? payload.run_vars.timeout
            : 5e3;

          h = true;

          resolve(Promise.race([
            new Function('ctx', 'return ' + responseFunctionContent)(ctx),
            new Promise(function(resolve, reject) {
              return setTimeout(function() {
                return reject({
                  name: 'cs_script_error: timeout',
                  message: 'cs_script_error: timeout ' + timeout + ' ms'
                })
              }, timeout)
            })
          ]))
        } catch (error) {
          console.warn('script eval error:', error);
          var parsedError = parseErrorMessage(error);
          throw new Error('script eval error: ' + parsedError.message)
        }
      })
    })
    .then(function(payload) {
      if (typeof(payload) === 'object') {
        return module59({
          payload: payload,
          originalPayload: originalPayload,
          swContext: context,
          userKey: userKey,
          afterIwant: true,
          fallbackType: fallbackType,
          prefetchSummary: prefetchSummary,
          flags: flags
        });
      }

      searchInCache = true;

      throw new Error('empty_teaser_response');
    })
    .catch(function(error) {
      console.warn('cs_script_error:', error)

      if (searchInCache || context.fallbackIwant)
        throw error;

      if (x.extra && x.extra.ctx)
        x.extra.ctx.allow_client_to_sever = false;

      context.fallbackIwant = true;
      context.errorTag = 'cs-fallback';

      return Promise.all([
        getData(context.eventDomain, 'cs_script_error(er=' + String(searchInCache) + '/fs=' + String(h) + "):", error, {
          user_key: userKey
        }).catch(function() {}),
        l(payload, context, searchInCache)
      ]).then(function() {
        return module61(x, context, userKey, constants.FALLBACK_TO_SERVER_2_SEVER)
      });
    });
  })
};
