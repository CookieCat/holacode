/*
** Module 2
*/

import props from './00_options';
import {default as openDb} from './09_iDB';

export default function trackDb() {
  const db = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : props.swDatabase;

  return {
    get: async function(key) {
      var db = await openDb(db);
      return await db.get(key);
    },
    set: async function(key, val) {
      var db = await openDb(db);
      return await db.set(key, val);
    },
    delete: async function(c) {
      var db = await openDb(db);
      return await db.delete(key);
    }
  }
};

export function oaidDb() {
  const db = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
    name: 'oaidDb',
    version: 1,
    trackStore: 'oaidStore',
    keyPath: "ts"
  };

  return {
    get: async function() {
      var c = await (0, t.openDb)(db),
        a = await c.getAll();
      return a.length > 0 ? a[0].oaid : void 0
    },
    set: async function(oaid) {
      var a = await (0, t.openDb)(db);
      return await a.put({
        oaid: oaid,
        ts: Number(new Date)
      })
    },
    getAll: async function() {
      var c = await (0, t.openDb)(db);
      return await c.getAll()
    }
  }
};

export function subscrDb() {
  const db = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
    name: 'subscriptionDb',
    version: 1,
    trackStore: 'subscriptionStore',
    keyPath: 'endpoint'
  };

  return {
    get: async function(obj) {
      var a = await (0, t.openDb)(db);
      return await a.get(String(obj.toJSON().endpoint))
    },
    set: async function(obj) {
      var a = await (0, t.openDb)(db);
      return await a.put(obj.toJSON())
    },
    getAll: async function() {
      var c = await (0, t.openDb)(db);
      return await c.getAll()
    }
  }
};
