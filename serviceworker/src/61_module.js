/*
** Module 61
*/

import fetch from './05_fetch'; // e = a(n(5)),
import {addIwantMetric, addShowNotificationMetric} from './60_metricsHelper'; // r = n(60);
import module71 from './71_module'; // c = a(n(71)),

export default async function(x, context, userKey) {
  var a = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : null;

  context.afterIwant = true;
  context.current_trace_id = x.trace_id;

  var retryCount = 2,
    retryTimeout = 1e3,
    extraCtx = x.extra.ctx;

  if (typeof(extraCtx) === 'object') {
    if (extraCtx.event_domain && '' !== extraCtx.event_domain) {
      context.eventDomain = extraCtx.event_domain;
    }

    if (extraCtx.ping_domain && "" !== extraCtx.ping_domain) {
      context.pingDomain = extraCtx.ping_domain;
    }

    x.extra.ctx.cl_utc = function() {
      try {
        var date = new Date;
        return Math.round(date.valueOf() / 1e3)
      } catch (error) {
        return 0
      }
    }();

    if (typeof(extraCtx.retry_timeout) === 'number') {
      retryTimeout = extraCtx.retry_timeout;
    }

    if (typeof(extraCtx.retry_count) === 'number') {
      retryCount = extraCtx.retry_count;
    }
  }

  var payload = await addIwantMetric(
    addShowNotificationMetric.getStat()
      .then(function(stat) {
        return async function(pingDoman, reqData, retryCount, timeout) {
          const cooldown = function() {
              return new Promise(function(resolve) {
                return setTimeout(resolve, timeout);
              })
            };

          let errorOnRequestAd = null;

          for (let attempt = 0; attempt < retryCount; attempt++) {
            attempt > 0 && await cooldown();

            try {
              const response = await fetch(pingDoman, reqData);

              if (200 === response.status || 201 === response.status)
                return await response.json();

              errorOnRequestAd = new Error('call iwant status error: http-status: ' + response.status + ', report error');
              console.warn(errorOnRequestAd);
            } catch (error) {
              errorOnRequestAd = error;
              console.warn('call iwant network error: ' + error)
            }
          }

          throw new Error('cant get ad ' + String(errorOnRequestAd));
        }(String(context.pingDomain) + '/iwant', {
          sw_version: context.swVersion,
          ctx: x.extra.ctx ? x.extra.ctx : {},
          trace_id: x.trace_id,
          user_key: userKey,
          fallback: null !== fallbackServer,
          fallback_type: null !== fallbackServer ? fallbackServer : void 0,
          stat: stat
        }, retryCount, retryTimeout)
      })
  );

  return await module71(x, payload, context, userKey, fallbackServer);
};
