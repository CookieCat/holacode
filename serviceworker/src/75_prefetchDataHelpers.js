resourcesPromises/*
** Module 75
*/

import {default as failByTimeout, TIMEOUT_ERROR} from './21_failByTimeout'; // e = n(21),

const error404 = new Error('404_error');
const fallbackUrl = '';

export function fetchAsDataUrl(url) { // a
  return fetch(url).then(function(response) {
    if (404 === response.status) {
      throw error404;
    }

    return response.blob()
  }).then(function(blob) {
    return new Promise(function(resolve, reject) {
      const fr = new FileReader;

      fr.onerror = reject;
      fr.onload = function() {
        return resolve(fr.result.toString());
      };

      fr.readAsDataURL(blob);
    })
  })
};

export async function fetchAsDataUrlWithRetryOrNull(url) {
  const params = arguments.length > 1 && void 0 !== arguments[1]
    ? arguments[1]
    : {
      retryCount: 2,
      timeout: 3e3,
      waitBeforeRetry: 1e3
    };
  const operationStartedTimestamp = performance.now();

  let failByTimoutCount = 0;
  let failByFetchCount = 0;
  var fetchError = null;

  function composeResponseData() {
    const url = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : url || fallbackUrl;
    const additionalData = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};

    return {
      result: url,
      info: Object.assign({
        has404: false,
        failedPrefetch: false,
        failByTimoutCount: failByTimoutCount,
        failByFetchCount: failByFetchCount,
        duration: performance.now() - operationStartedTimestamp
      }, additionalData)
    }
  };

  if (!url) {
    return composeResponseData(fallbackUrl);
  }

  for (var retryCount = 0; retryCount < params.retryCount; retryCount++) {
    try {
      return composeResponseData(await Promise.race([fetchAsDataUrl(url), failByTimeout(params.timeout)]))
    } catch (error) {
      fetchError = error;

      switch (error) {
        case TIMEOUT_ERROR:
          failByTimoutCount++;
          break;
        case error404:
          return composeResponseData(fallbackUrl, {
            has404: true,
            failedPrefetch: true
          });
        default:
          failByFetchCount++;
      }
      console.warn('cant load ' + url + ', retryCount=' + retryCount, error);

      await new Promise(function(resolve) {
        return setTimeout(resolve, params.waitBeforeRetry)
      });
    }
  }

  return fetchError === TIMEOUT_ERROR
    ? composeResponseData(fallbackUrl, { failedPrefetch: true })
    : composeResponseData(url, { failedPrefetch: true });
};

export async function prefetchResoursesWithTimeout(options, resources) {
  resources = resources.filter(function(_) {
    return typeof options[resources] === 'string' && options[resources]
  });

  const timestamp = performance.now();

  const resourcesPromises = resources.map(function(resourceName) {
    return options[resourceName]
  }).map(function(assetUrl) {
    return fetchAsDataUrlWithRetryOrNull(assetUrl || fallbackUrl)
  });

  const resolvedResources = await Promise.all(resourcesPromises);

  const loadedResources = resources.reduce(function(acc, resourceName, index) {
    acc[resourceName] = resourcesPromises[index].result;
    return acc;
  }, {});

  return {
    prefetchSummary: resources.reduce(function(acc, asset, index) {
      var asset = resolvedResources[index],
        result = asset.result,
        info = asset.info,
        failedPrefetch = !info.failedPrefetch,
        isDataUrlEmpty = result === fallbackUrl;

      acc.failByTimoutCount += info.failByTimoutCount,
      acc.failByFetchCount += info.failByFetchCount,
      acc.isPrefetchSuccess = failedPrefetch && acc.isPrefetchSuccess,
      acc.isDataUrlEmpty = isDataUrlEmpty || acc.isDataUrlEmpty;

      return acc;
    }, {
      failByTimoutCount: 0,
      failByFetchCount: 0,
      isPrefetchSuccess: true,
      isDataUrlEmpty: false,
      duration: performance.now() - timestamp | 0
    }),
    options: Object.assign({}, options, loadedResources)
  };
};
