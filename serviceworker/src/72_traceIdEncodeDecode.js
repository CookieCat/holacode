/*
** Module 72
*/

const Base64 = require('./73_getBase64'); // var c = n(73);

export default function() {
  return new EncodeDecode();
};

class EncodeDecode {
  constructor() {
    this.maxHeaderLength = 25,
    this.maxBodyLength = 80,
    this.wordsSeparator = " ",
    this.mgidRe = /\/(\d+)_(\d+)x(\d+).(\w+)$/,
    this.trcIdSeparator = "||",
    this.ckKeyCode = '7f6ka9Ka2kJqP5OAuZuoQiGZ165uuGY8'
  }

  decodeTraceId(encoded) {
    var arr = encoded.split(this.trcIdSeparator);
    return 1 === arr.length ? {
      id: arr[0],
      data: {}
    } : {
      id: arr[0],
      data: JSON.parse(Base64.decode(arr[1]))
    }
  }

  encodeTrcaceId(key, data) {
    if (typeof(data) === 'object') {
      const encoded = Base64.encode(JSON.stringify(data));

      return '' + key + this.trcIdSeparator + encoded;
    }
    return key;
  }
};
