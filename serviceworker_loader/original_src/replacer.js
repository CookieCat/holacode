const file = require('fs');
const verbatims = require('./verbatims');

let data = file.readFileSync('./afjhjk7647gfgaj.js', {encoding: 'UTF8'});

verbatims.forEach(o => {
  const key = Object.keys(o)[0];
  const val = "'" + Object.values(o)[0] + "'";

  const reg = new RegExp(key, 'g');

  data = data.replace(reg, val);
})

data = data.replace(/\[\'\w+\'\]/g, a => `.${a.slice(2,-2)}`);
data = data.replace(/!0/g, 'true');
data = data.replace(/!1/g, 'false');

file.writeFileSync('./afjhjk7647gfgaj.decoded.raw.js', data, {encoding: 'UTF8'})
