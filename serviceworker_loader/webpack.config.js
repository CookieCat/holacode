const path = require('path');

module.exports = {
  mode: 'production',
  entry: './loader.js',
  output: {
    filename: 'loader.bundle.js',
    libraryTarget: 'var'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader'
      }
    ]
  }
}
