/*
** Module 27
*/

export default function uuid4() {
  return '00000000-0000-4000-0000-000000000000'.replace(/[^-4]/g, function(x) {
    return ((x = Number(x)) ^ self.crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> x / 4).toString(16)
  });
};
