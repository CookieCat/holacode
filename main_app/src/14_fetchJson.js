/*
** Module 14
*/

import fetch from './05_fetch';

const jsonRestFn = async function(url) {
  const body = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
  const method = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 'post';
  const addParams = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : {};
  const request = await fetch(url, body, method, addParams);

  if (200 === request.status || 201 === request.status) {
      const json = await request.json();

			if (true !== json.status)
        throw new Error('call ' + url + ' error: code: ' + request.code + ' message: ' + request.message);

      return json;
  }

  throw new Error('call ' + url + ' error: http-status: ' + request.status)
};

export default jsonRestFn;
export const jsonRest = jsonRestFn;
