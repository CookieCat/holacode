/*
** Module 17
*/

export default function(node) {
  try {
    if (node) {
      var parent = node.parentNode;
      parent && parent.removeChild(node)
    }
  } catch (error) {
    console.warn(error)
  }
};
