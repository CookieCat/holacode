/*
** Module 43
*/

import props from './00_options'; // n = d(a(0)),
import {eventLogger} from './01_logger'; // u = a(1);
import {default as trackDb, subscrDb} from './02_namedIndexedDB'; // t = a(2),
import {SDKCall} from './03_sdkRegistration'; // o = a(3),
import notify from './13_notify'; // i = a(13),
import setRegistrationContext from './18_setRegistrationContext'; // _ = d(a(18)),
import fetchGidratorResponse from './16_fetchGidratorResponse'; // e = d(a(16)),
import cookieHelper from './08_cookieHelper'; // r = d(a(8)),

async function checkHasSubscriptionRec(options, swName) {
  if (!navigator.serviceWorker)
    throw Error('service worker failed');

  const serviceWorker = navigator.serviceWorker;

  try {
    await setRegistrationContext({
      installOnFly: +Date.now()
    }, {
      user: options.oaid,
      zoneId: options.zoneId
    });

    await async function(o) {
      try {
        await trackDb().set('context', {
          swEventDomain: o.domain,
          swInstallEventDomain: o.domain,
          swPingDomain: o.pingDomain ? o.pingDomain : o.domain,
          swGidratorDomain: o.gidratorDomain ? o.gidratorDomain : props.swGidratorDomain
        })
      } catch (error) {}
      return true
    }(options);

    var result = await serviceWorker.register(swName);

    if (!result.showNotification)
      throw new Error('showNotification() function is unsupported by browser');

    const subs = await e.pushManager.getSubscription();
    const hasSubscriptionRec = await async function(subs) {
      if (!subs)
        return false;

      try {
        var c = await subscrDb().get(subs);
        return Boolean(c);
      } catch (error) {
        console.warn('check sub error:', error);
        return false;
      }
    }(subs);

    Boolean(hasSubscriptionRec)
      ? (SDKCall.alreadySubscribed(), notify('skip_already_subscribed'))
      : subs && (options.current_fcm_live_auth = subs.toJSON().keys.auth);

    return hasSubscriptionRec;
  } catch (error) {
    console.warn(error);
    throw error;
  }
};

export default async function checkRedundantScriptRegistrationFn(options) {
  if (!('serviceWorker' in navigator && 'Notification' in window)) {
    notify('skip_sw_disabled');
    return true;
  }

  var swName = options.popup ? options.swName : "/" + options.swName;

  console.debug('Service Worker is supported, service worker path:', swName, 'event domain:', options.domain);

  const gidratorTimeout = options.gidratorTimeout ? options.gidratorTimeout : 1e3;
  const gidratorDomain = options.gidratorDomain ? options.gidratorDomain : props.swGidratorDomain;

  if (true === await checkHasSubscriptionRec(options, swName))
    return true;

  if (true === options.useRtMarkUser && void 0 !== cookieHelper().get('_PN_SBSCRBR_SKIP_BY_GIDRATOR').val) {
    SDKCall.alreadySubscribed();
    notify('skip_by_gid_cookie');

    return true;
  }

  var gidratorResponse = await fetchGidratorResponse(gidratorDomain, options.oaid, true, gidratorTimeout);

  if (options.gidratorResponse = gidratorResponse, true === gidratorResponse.ok && (options.true_user = gidratorResponse.gidratorOAID),
    true === options.useRtMarkUser && (true === gidratorResponse.ok && (options.oaid = gidratorResponse.gidratorOAID),
    true === gidratorResponse.skipInstall)) {
      cookieHelper().set('_PN_SBSCRBR_SKIP_BY_GIDRATOR', 1, 1),
      notify('skip_by_gid'),
      SDKCall.alreadySubscribed();

      try {
        await eventLogger.send({
          event_type: 'skip_by_gid',
          installer_type: true === options.popup ? 'http' : 'https'
        }, options.domain);
      } catch(error) {}

      return true;
  }

  return false;
};

export const checkRedundantScriptRegistration = checkRedundantScriptRegistrationFn;
