/*
** Module 7
*/

export default function(error) {
  const context = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};

  if (null == error)
    return {
      message: 'error is undefined or null',
      stack: 'unknown'
    };

  const errContext = null != context ? JSON.stringify(context) : 'no-ctx';
  const obj = error.toString ? error.toString() : JSON.stringify(error);
  const message = error.message ? error.message : 'no-message';
  const name = error.name ? error.name : 'no-name';
  const code = error.code ? error.code : 'no-code';

  return {
		message: 'error-obj: ' + obj + ', error-msg: ' + message + ', error-name: ' + name + ', error-code: ' + code + ', error-ctx: ' + errContext,
    stack: error.stack ? error.stack : 'unknown'
  }
};
