/*
** Module 32
*/

import {SKDCall} from './03_sdkRegistration';

export default function() {
  try {
    window.postMessage('subscriptiondone', "*");
    console.info('send: subscriptiondone');
  } catch (error) {
    console.warn('win.postMessage("subscriptiondone" , "*") error:', error);
  }
  Notification.permission === 'granted' && SDKCall.permissionAllowed()
};
