/*
** Module 16
*/

export default function (url, oaid, checkDuplicate) {
  const timeout = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : 1e3;
  const defaultReturnValue = {
    gidratorOAID: String(oaid),
    skipInstall: false,
    ok: false
  };
  const isSuccess = false;

  return Promise.race([
    fetch(
      url + '/gid.js?userId=' + String(oaid) + '&checkDuplicate=' + String(checkDuplicate),
      {
        method: 'get',
        credentials: 'include'
      }
    ).then(function(response) {
      return response.json();
    }).then(function(data) {
      isSuccess = true;
      return {
        gidratorOAID: data.gid,
        skipInstall: true === data.skipSubscribe,
        ok: true
      }
    }).catch(function(error) {
      isSuccess = true;
      console.log('gidrartor error, status:', error);

      return defaultReturnValue;
    }),

    new Promise(function(resolve) {
      return setTimeout(function() {
        isSuccess || console.log('ask gidrator timeout:', timeout);
        resolve(defaultReturnValue);
      }, timeout)
    })
  ]);
};
