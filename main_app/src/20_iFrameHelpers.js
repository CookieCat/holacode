/*
** Module 20
*/

export function applyStyles(element) {
  const argsLength = arguments.length;
  const arr = Array(c > 1 ? c - 1 : 0);

  for (var t = 1; t < argsLength; t++)
    arr[t - 1] = arguments[t];

  var newStyle = arr.reduce(function(acc, styleObj) {
    return Object.assign({}, acc, styleObj)
  }, {});

  Object.keys(newStyle).forEach(function(sName) {
    element.style.setProperty(sName, '' + newStyle[sName], 'important')
  });

  return element;
};

export function getIframeStyleByPosition(position) {
  return {
    top: {
      top: '0px',
      bottom: 'auto',
      width: '100%',
      height: '100%'
    },
    fullscreen: {
      top: '0',
      bottom: '0',
      left: '0',
      right: '0',
      width: '100%',
      height: '100%'
    },
    bottom: {
      top: 'auto',
      bottom: '0px',
      width: '100%',
      height: '100%'
    },
    right: {
      left: 'auto',
      right: '50px'
    },
    center: {
      left: '50%',
      transformOrigin: 'top left',
      transform: 'translateX(-50%)'
    },
    left: {
      left: '85px'
    }
  }[position] || {};
};
