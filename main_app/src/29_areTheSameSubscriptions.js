/*
** Module 29
*/

export default function areTheSameSubscriptions(s1, s2) {
  const ss1 = s1 ? s1.toJSON() : null;
  const ss2 = s2 ? s2.toJSON() : null;

  if (null === ss2 || null === ss1)
    return false;
  return ss2.endpoint === ss1.endpoint && ss2.keys.auth === ss1.keys.auth && ss2.keys.p256dh === ss1.keys.p256dh
};
