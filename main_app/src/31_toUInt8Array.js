/*
** Module 31
*/

// входная строка типа такой:
// BM96gISZFEBtvcw8As0D_u8wQbRSFtNJv5mX1zKnBu6AGq5xXlt4VqJk22fupxalcLQMbvG-sREN2LSXB1MWoW8
export default function(str) {
  const inset = "=".repeat((4 - str.length % 4) % 4);
  const formatted = (str + inset).replace(/\-/g, "+").replace(/_/g, "/");
  const conv = atob(formatted);
  const arr = new Uint8Array(conv.length);
  const length = conv.length;

  for (let n = 0; n < length; ++n) {
    arr[n] = conv.charCodeAt(n);
  }

  return arr;
}
