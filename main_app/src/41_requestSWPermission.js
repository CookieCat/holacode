/*
** Module 41
*/

import props from './00_options'; // d = y(a(0)),
import {eventLogger} from './01_logger'; // i = a(1),
import {subscrDb} from './02_namedIndexedDB'; // v = a(2),
import {SDKCall} from './03_sdkRegistration'; // r = a(3),
import {parseUrlParams} from './10_URLHelpers'; // o = a(10),
import notify from './13_notify'; // e = a(13),
import {appLock} from './15_dynamicLockers'; // m = a(15);
import setRegistrationContext from './18_setRegistrationContext'; // n = y(a(18)),
import getBrowserStat from './19_browserStat'; // u = a(19),
import {finishInstall} from './25_finishInstall' // t = a(25),
import pushManagerSubscriptionsHelper from './28_pushManagerSubscriptionsHelper'; // f = y(a(28)),
import areTheSameSubscriptions from './29_areTheSameSubscriptions'; // b = a(29),
import HttpClient from './30_httpClient'; // s = a(30),
import waitPromiseSync from './42_waitPromiseSync'; // l = a(42),

async function Z(options, permissionStatus) {
  let trackConversion;
  let query;

  const httpClient = HttpClient(options.domain);
  const subscriptionProps = {
    status: permissionStatus,
    trace_id: options.trace_id,
    version: options.version,
    sw_version: props.swVersion,
    user: options.oaid,
    true_user: options.true_user,
    popup: options.popup,
    install_ctx: options.install_ctx,
    need_click_track: options.need_click_track,
    browser_stat: getBrowserStat(),
    gidratorResponse: options.gidratorResponse,
    creative: {
      in_iframe: !(window.self === window.top),
      domain: location.hostname,
      location: location.href,
      land_id: options.landId,
      zone_id: +options.zoneId,
      pub_zone_id: +options.pubZoneId,
      ext_id: options.var,
      ymid: options.ymid
    }
  };
  const serviceWorker = navigator.serviceWorker;

  if (permissionStatus === 'granted' && serviceWorker) {
    let conversionId = void 0;
    let from_install = void 0;
    let prev_auth = void 0;

    if(options.conversionId)
      conversionId = options.conversionId.toString();

    if (options.trackConversion) {
      trackConversion = options.trackConversion;
      query = window.location.search.substring(1);

      var G = parseUrlParams(query)[trackConversion] || false;

      if (G)
        conversionId = String(options.conversionId) + "," + G;
    }

    typeof options.current_fcm_live_auth === 'string' && (from_install = true, prev_auth = options.current_fcm_live_auth);

    try {
      await appLock.runReleaseOnFail(async function() {
        var c = await serviceWorker.ready;

        console.info('Service Worker is ready :^)', c);

        const {applicationServerKey, key_id} = await httpClient.getApplicationServerKey();
        const subs1 = await c.pushManager.getSubscription();
        const subs2 = await pushManagerSubscriptionsHelper(c.pushManager, {
          userVisibleOnly: true,
          applicationServerKey: applicationServerKey
        });

        if (areTheSameSubscriptions(subs2, subs1))
          return true;

        await subscrDb().set(subs2);

        const subs2Data = subs2.toJSON();
        const d = Object.assign({}, subscriptionProps, {
          conversion_id: conversionId,
          prev_auth: prev_auth,
          from_install: from_install,
          key_id: key_id,
          endpoint: subs2Data.endpoint,
          auth: String(subs2Data.keys.auth),
          p256dh: String(subs2Data.keys.p256dh),
          user: options.oaid
        });

        await setRegistrationContext({
          registration_hostname: location.hostname,
          user: d.user,
          true_user: d.true_user,
          auth: d.auth,
          zoneId: options.zoneId,
          pubZoneId: options.pubZoneId,
          domain: options.domain
        })

        if (true !== (await (await waitPromiseSync(httpClient.subscribe(d)))).status)
          throw Error('subscribe error');
      });

      finishInstall(options, true);

      return true;
    } catch (error) {
      if (error === appLock.LOCK_FAILED) {
        eventLogger.send({
            event_type: 'skip_by_lock'
        }, options.domain);

        SDKCall.alreadySubscribed();

        return true;
      }

      throw error;
    }
  }
  else {
    try {
      await httpClient.subscribe(subscriptionProps);
    } catch (error) {
      console.warn('unsusbscribe error: ', error)
    }
  }

  switch (permissionStatus) {
    case 'default':
      SDKCall.permissionDefault();
      break;
    case 'denied':
      SDKCall.permissionDenied()
  }

  finishInstall(options, true);
  return true;
}

export default async function(options) {
  if (!navigator.serviceWorker)
    throw Error('navigator.serviceWorker is not defined');

  SDKCall.beforePermissionPrompt();

  await async function(options) {
    await eventLogger.send({
      event_type: true === options.popup ? 'popup_perm_show' : 'https_perm_show',
      installer_type: true === options.popup ? 'http' : 'https'
    }, options.domain)
  }(options);

  var permissionStatus = await Notification.requestPermission();

  switch (permissionStatus) {
    case 'default':
      notify('permission_default');
      break;
    case 'granted':
      notify('permission_granted');
      break;
    case 'denied':
      notify('permission_denied')
  }

  return await Z(options, permissionStatus)
};
