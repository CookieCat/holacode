/*
** Module 24
*/

export default function onDomReady(context, callback) {
  if (document.readyState === 'loading')
    document.addEventListener('DOMContentLoaded', function() {
      if (document.body && document.head)
        return callback(document.body, document.head)
    });
  else if (document.body && document.head)
    return callback(document.body, document.head)
};
