/*
** Module 42
*/

const xhrReqFactory = require('./12_xhrReqFactory');
const props = require('./00_options');

export default function waitPromiseSync(promise) {
  var args = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {
    timeoutMs: 3000,
    context: {},
    domain: props.swDomain
  };

  try {
    var onbeforeunload = self.onbeforeunload;
    window.onbeforeunload = function() {
      try {
        for (let now = performance.now(); performance.now() - now < args.timeoutMs;) {
          new xhrReqFactory(args.domain + '/wait4sub', false).post(args.context);
        }
      } catch (error) { }
    },
    promise.finally(function() {
      window.onbeforeunload = onbeforeunload
    });
  } finally {
    return promise;
  }
};
