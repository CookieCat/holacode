/*
** Module 31
*/


// тут наконец-то ясно что за кастомные Lary =)

const options = {
  version : '1.4.4',
  notification_dir_path : '1.4.4/',
  zoneId : 2480981,
  pubZoneId : 2480981,
  oaid : '8ef84686c562190eadc307445da36c6d',
  domain : 'https://pushanert.com',
  swName : 'afjhjk7647gfgaj.js',
  popup : false,
  popupShow : true,
  firebaseLog : false,
  desktopXPosition : '',
  desktopXPositionZone : '',
  mobileVPosition : '',
  mobileVPositionZone : '',
  trackConversion : '',
  showBackground : false,
  allowPopupIfHttpsDenied : false,
  customParamsIp : '178.219.43.36',
  customParamsGeo : 'RU',
  customParamsSkin1 : '',
  customParamsSkin2 : '',
  customParamsLoggerUrl : 'https://pushanert.com/custom',
  swTimeout : 5000,
  disableSwSanity : false,
  swSanity404only : true,
  install_ctx : {
    country_code : 'RU'
  },
  openInTab : false,
  useRtMarkUser : true,
  gidratorTimeout : 2000,
  var : null,
  ymid : '{click_id',
  notificationsDelayHttps : 0,
  notificationsDelayHttp : 0,
  afterCloseDelay : 0,
  showFrequency : 1,
  showCapping : 0,
  trace_id : 'd5eb9a7f-91c9-3232-a2da-f9af0c713e5f',
  forcePopup : false,
  notificationsDelay : 0
};

const lary = 'abcdefghijklmnopqrstuvwxyz0123456789qxs7kd5oiz1w4vet3rmpc98hgblau602yfjn';

import props from './00_options';
import {decodeOptions} from './21_decodeOptions';

export default function getOldOptions() {
  const x = 'options' + props.swParamSuffix;
  const c = 'lary' + props.swParamSuffix;
  const oldOptions = typeof self[x] !== 'undefined' ? self[x] : typeof options !== 'undefined' ? options : null;
  const lary = typeof self[c] !== 'undefined' ? self[c] : typeof lary !== 'undefined' ? lary : "";

  if (!oldOptions)
    return null;

  const obj = decodeOptions(oldOptions, lary);

  if (obj.domain && !/^(http:|https:|)\/\//i.test(t.domain)) {
    obj.domain = ['https:', t.domain].join("//");
  }

  return obj;
};
