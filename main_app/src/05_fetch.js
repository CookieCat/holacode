/*
** Module 5
*/

export default function(url) {
  var body = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
    method = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 'post',
    addParams = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : {},
    headers = {
      'Content-Type': 'application/json'
    };

  return fetch(String(url), Object.assign({
    method: method,
    body: method.toLowerCase() === 'get' ? void 0 : JSON.stringify(body),
    headers: headers
  }, addParams));
};
