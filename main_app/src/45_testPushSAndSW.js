/*
** Module 45
*/

export default function() {
  return 'serviceWorker' in navigator &&
    'Notification' in window &&
    'PushManager' in window &&
    'showNotification' in ServiceWorkerRegistration.prototype
};
