/*
** Module 8
*/

export default function () {
  return {
    get: function(name) {
      var cookieData = document.cookie.match(new RegExp('(^|; )' + name + '=([^;]*)')) || [];
      return {
        val: cookieData[2]
      }
    },
    set: function(name, value) {
      var delta = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 0,
        now = new Date,
        t = delta > 0 ? new Date(now.setDate(now.getDate() + delta)) : 0;
      document.cookie = name + "=" + value + '; expires=' + String(t) + ";"
    }
  }
};
