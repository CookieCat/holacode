/*
** Module 36
*/

// ('pusher-https', props.swVersion, zoneId, document.currentScript)
export default function(format, version, zoneId, script) {
  const domain = void 0;

  try {
    script && (domain = script.src.split('/')[2])
  } catch (error) {}

  window.zfgformats
    ? window.zfgformats.forEach(function(format) {
      if (format.zoneId === zoneId && format.sourceZoneId) {
        zoneId = format.sourceZoneId;
        domain = format.domain;
      }
    })
    : window.zfgformats = [];

  window.zfgformats.push({
    format: format,
    version: version,
    zoneId: zoneId,
    domain: domain
  })
};
