/*
** Module 21
*/

export default function decodeFn(encoded, secret) {
  if (typeof(encoded) !== 'string')
    return encoded;
  var length_2 = secret.length / 2,
    leftPart = secret.substr(0, length_2),
    rightPart = secret.substr(length_2),
    decoded = encoded.split("").map(function(x) {
      var c = rightPart.indexOf(x);
      return -1 !== c ? leftPart[c] : x
    }).join("");

	try {
    return JSON.parse(decoded)
  } catch (error) {
    return eval("(" + decoded + ")")
  }
}

export const decodeOptions = decodeFn;
