/*
** Module 13
*/

export default function notify(message) {
  console.log('I: ', message);
  try {
    window.opener ? window.opener.postMessage(message, "*") : window.postMessage(message, "*")
  } catch (error) {
    console.error(error)
  }
};
