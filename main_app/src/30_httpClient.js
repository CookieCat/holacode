/*
** Module 31
*/

import props from './00_options';
import toUInt8Array from './31_toUInt8Array';
import getBrowserStat from './19_browserStat'; // не понятно зачем импортируется, но в коде не используется

export default function HttpClient() {
  let swDomainUrl = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : props.swDomain;

  swDomainUrl.startsWith('https://') || (swDomainUrl = 'https://' + swDomainUrl);

  async function executeReq(url, method, body) {
    const response = await fetch(url, {
      method: method,
      credentials: 'include',
      body: body ? JSON.stringify(body) : void 0,
      headers: {
        "Content-Type": 'application/json'
      }
    });
    const json = await response.json();

    if (true !== json.status)
      throw new Error('call ' + url + ' error: code: ' + json.code + ' message: ' + json.message);

    return json;
  };

  return {
    subscribe: async function(payload) {
      return await executeReq(swDomainUrl + '/subscribe', 'POST', payload);
    },
    getApplicationServerKey: async function() {
      const data = await executeReq(swDomainUrl + '/key?id=' + location.hostname, 'GET');
      const applicationServerKey = toUInt8Array(data.key);

      return {
        key_id: data.id,
        key: data.key,
        applicationServerKey: applicationServerKey
      };
    }
  }
};
