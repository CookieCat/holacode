/*
** Module 28
*/

import {eventLogger} from './01_logger';

export default function (pushMngr, event) {
  const options = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
  const timestamp = Number(new Date);
  const log = function(event_type) {
    return function(data) {
      eventLogger.send({
          event_type: event_type,
          fallback_type: (Number(new Date) - timestamp).toString()
      });

      return data;
    }
  };

  return pushMngr
    .subscribe(event)
    .catch(function(error) {
      console.warn('pushManager.subscribe() error:', error);

      if (true === options.swDoNotResusbscribe)
        throw error;

      return pushMngr.getSubscription().then(function(oldSubscription) {
        if (!oldSubscription)
          throw error;

        console.warn('not expected subscription found');

        return oldSubscription.unsubscribe().then(function() {
          return pushMngr.subscribe(event)
        })
      }).catch(function() {
        throw error;
      })
    })
    .then(log('subscribe_resolved'))
    .catch(function(error) {
      log('subscribe_failed')();
      throw error;
    });
};
