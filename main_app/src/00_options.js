/*
** Module 0
*/

module.exports = {
    swDomain: 'https://pushwhy.com',
    swPingDomain: 'https://pushwhy.com',
    swGidratorDomain: 'https://my.rtmark.net',
    swVersion: '3.1.14',
    swDatabase: {
        name: 'swDatabase',
        version: 1,
        trackStore: 'trackStore'
    },
    swMetricsDb: {
        name: 'swMetrics',
        trackStore: 'metricStore',
        version: 1
    },
    swRunCmdCache: 'runCmdCache',
    swSettingsKey: 'swSettings',
    swHasIwant: true,
    swDefaultBanner: {
        title: 'Currency News',
        options: {
            silient: false,
            requireInteraction: true,
            body: 'Can Bitcoin Become a Global Reserve Currency?',
            icon: 'https://pushimg.cdnnative.com/www/images/bd4a64d1a4a4ae2213929fc99aca8b73.png',
            data: {
                url: 'https://news.breakingfeedz.com/ck.php?ct=1&zoneid=1647651&bannerid=1869796'
            }
        },
        trace_id: "",
        is_empty: true
    },
    swFallbackErrorDomain: 'https://kurlipush.com',
    swParamSuffix: 'AxXB324Fe'
};
