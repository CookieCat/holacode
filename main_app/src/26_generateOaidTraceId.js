/*
** Module 26
*/

import uuid4 from './27_getUUID';
import {oaidDb} from './02_namedIndexedDB';

export default async function generateOaidTraceId() {
  const oaid = uuid4().replace(/-/g, "");
  const traceId = uuid4();

  try {
    const recOaid = await oaidDb().get();

    if (!recOaid) {
      await oaidDb().set(oaid);
    }

    recOaid = await oaidDb().get();

    return {
      trace_id: traceId,
      oaid: recOaid || oaid
    };
  } catch (error) {
    console.warn(error);

    return {
      oaid: oaid,
      trace_id: traceId
    };
  }
};
