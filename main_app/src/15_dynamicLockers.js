/*
** Module 15
*/

import {default as openDB} from './06_iDB';

const tLockError = new Error('Lock failed');

function lockFn() {
  const interval = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 36e5; // default === 1 minute
  const timestamp = Number(new Date) / interval | 0; //округляем интервал
  const dbConfig = {
    name: 'blockDb',
    version: 1,
    trackStore: 'blockStore',
    autoIncrement: true,
    keyPath: 'key'
  };
  const openDBIndex = async function() {
    return await openDb(dbConfig, function(dbObj) {
      dbObj.createIndex('key', 'key', {
        unique: true
      })
    })
  };

  async function createTimestampRec() {
    const dbKeyRecord = await openDBIndex();

    try {
      await dbKeyRecord.add({
        key: timestamp
      });

      return true;
    } catch (error) {
      console.error(error);

      return typeof(IDBRequest) === 'function' && error.target instanceof IDBRequest
        ? false
        : (console.warn(error), true);
    }
  }

  async function deleteTimestampRec() {
    const dbKeyRecord = await openDBIndex();

    try {
      await dbKeyRecord.deleteByIndex("ts", timestamp)
    } catch (error) {
      console.warn(error)
    }
  }

  return {
    isLocked: async function() {
      try {
        var dbKeyRecord = await openDBIndex();

        return Boolean(await dbKeyRecord.get(timestamp))
      } catch (error) {
        return false
      }
    },
    request: createTimestampRec,
    release: deleteTimestampRec,
    runReleaseOnFail: async function(observedFunction) {
      if (!await createTimestampRec()) {
        throw tLockError;
      }

      const c = observedFunction();

      c.catch(deleteTimestampRec);

      return c;
    },
    LOCK_FAILED: tLockError
  }
};

export let getLock = lockFn;
export let appLock = lockFn();
