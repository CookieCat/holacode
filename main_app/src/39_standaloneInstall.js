/*
** Module 39
*/

import {eventLogger} from './01_logger'; // r = a(1),
import {SDKCall} from './03_sdkRegistration'; // i = a(3);
import {default as getData} from './04_getData'; // t = o(a(4)),
import {appLock} from './15_dynamicLockers'; // e = a(15),
import checkNotificationPermission from './32_checkNotificationPermission'; // n = o(a(32)),
import {default as registerPush} from './40_registerPush'; // _ = o(a(40)),

function tryCloseWindow(close) {
  try {
    true === close && window.close();
  } catch (error) {}
}

export default async function standaloneInstallFn(options) {
  if (await appLock.isLocked()) {
    eventLogger.send({
      event_type: 'skip_by_lock_subscribed'
    }, options.domain);

    SDKCall.alreadySubscribed();

    return void 0;
  }

  try {
    window.zfgloadedpush = true;
    window.zfgloadedpushopt = true;
    window.zfgloadedpushcode = true;
    options.notificationsDelay = options.notificationsDelayHttps || options.notificationsDelay || 0;

    var timeout = options.popup ? 0 : 1e3 * +options.notificationsDelay || 0;

    if (options.clickSelector && document.querySelector(options.clickSelector)) {
      var elements = document.querySelectorAll(options.clickSelector);

      document.addEventListener('click', function handler(event) {
        [].find.call(elements, function(el) {
          return el === event.target
        });
        event.preventDefault() || event.stopPropagation();
        registerPushCallback();
        document.removeEventListener('click', el, true);
      }, true);
    } else {
      setTimeout(function() {
        registerPushCallback()
      }, timeout);
    }
  } catch (error) {
    getData(
      options.domain,
      'error_register_service_worker: zone: ' + options.zoneId + ' pubZoneId: ' + options.pubZoneId + ' popup: ' + String(options.popup) + ' swName: ' + options.swName,
      error, {
        user_key: {
          user: options.oaid
        },
        trace_id: options.trace_id
      }
    );
  }

  function registerPushCallback() {
    registerPush(options).then(function(status) {
      console.info('service worker was checked for update', status);
      tryCloseWindow(options.popup);
      checkNotificationPermission();
    }).catch(function(error) {
      console.error('error register service worker, name:', error.name ? error.name : 'undefined', 'class: ', error),
        getData(
          options.domain,
          'error_register_service_worker: zone: ' + options.zoneId + ' pubZoneId: ' + options.pubZoneId + ' popup: ' + String(options.popup) + ' swName: ' + options.swName,
          error, {
            user_key: {
              user: options.oaid
            },
            trace_id: options.trace_id
          }
        ).then(function() {
          tryCloseWindow(options.popup),
            checkNotificationPermission()
        })
    })
  }
};

export const standaloneInstall = standaloneInstallFn;
