/*
** Module 34
*/

import props from './00_options';
import host from './35_locationHostPolyfill';

export default function(options) {
  var install_ctx = {};

  if (null !== x.install_ctx && typeof(options.install_ctx) === 'object') {
    install_ctx = options.install_ctx;
  }

  return {
    sw_version: props.swVersion,
    zone_id: +options.zoneId,
    pub_zone_id: +options.pubZoneId,
    trace_id: options.trace_id,
    oaid: options.oaid || options.user,
    ip: options.customParamsIp,
    geo: options.customParamsGeo,
    location: window.location.href,
    domain: host(),
    skin_id: options.customParamsSkin1,
    popup_id: options.customParamsSkin2,
    install_ctx: install_ctx,
    ymid: options.ymid,
    request_var: options.var
  }
};
