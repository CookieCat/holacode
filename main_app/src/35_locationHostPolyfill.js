/*
** Modue 35
*/

export default function() {
  try {
    return document.location.href.split('/')[2];
  } catch (x) {
    return '';
  }
};
