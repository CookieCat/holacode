/*
** Module 9
*/

export default {
  NORMAL: 'normal',
  FALLBACK_TO_SERVER_2_SEVER: 'fallback_s_2_s',
  UNSUPORTED: 'unsupported',
  PARSE_ERROR: 'parse_error',
  PREVIOUS: 'previous',
  EMPTY: 'empty',
  RESUB_BY_MESSAGE: 'resubscribe-by-message',
  EMPTY_TRACE_ID: '',
  ERROR_REPORT: 'error_json',
  DEFERED_MSG: 'deferred_msg'
};
