/*
** Module 25
*/

import cookieHelper from './08_cookieHelper';

export default function fn(options, cookieViaImage) {
  if (options.popup) {
    try {
      window.installFinisned = true
    } catch (error) {}

    if (window.opener)
      try {
        window.opener.postMessage({
          name: 'popupClosedAuto',
          acceptedStatus: x.acceptedStatus,
          cookieViaImage: cookieViaImage
        }, "*");
      } catch (error) {}
  } else {
    options.acceptedStatus &&
    options.acceptedStatus !== 'default' &&
    cookieHelper().set('_PN_SBSCRBR_FALLBACK_DENIED', 1, 1);
  }

  return true
}

export const finishInstall = fn;
