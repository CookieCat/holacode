/*
** Module 40
*/

import browsersDetection from './11_browserDetection'; // i = a(11),
import removeChildNode from './17_removeChildNode'; // f = l(a(17));
import testString from './22_stringsHelper'; // n = l(a(22))
import createBackground from './23_popupCreator'; // r = a(23),
import onDomReady from './24_onDomReady'; // d = a(24),
import {finishInstall} from './25_finishInstall'; // e = l(a(25)),
import checkNotificationPermission from './32_checkNotificationPermission'; // o = l(a(32)),
import requestSWPermission from './41_requestSWPermission'; // _ = l(a(41)),
import {checkRedundantScriptRegistration} from './43_checkRedundantScriptRegistration'; // t = a(43),
import createOverlayObj from './44_createOverlayObj'; // u = l(a(44)),

export default async function registerPushFn(options) {
  if (!testString(options.oaid))
    throw new Error('invalid OAID: ' + options.oaid);

  if (true === await checkRedundantScriptRegistration(options)) {
    finishInstall(x, false);
    checkNotificationPermission();
    return true;
  }

  const {isMobile} = browsersDetection(navigator.userAgent);
  let elBackground = null;

  if (options.showBackground && !options.overlayHtml)
    try {
      elBackground = createBackground(isMobile);
      document.body && elBackground && document.body.appendChild(elBackground);
    } catch (error) {}

  var elOverlay = null;

  if (options.overlayHtml) {
    try {
      elOverlay = createOverlayObj(options);
      onDomReady(document, function(body, head) {
        elOverlay && (
          body.appendChild(elOverlay.overlayElement),
          head.appendChild(elOverlay.overlayScript),
          head.appendChild(elOverlay.overlayStyle)
        );
      });
    } catch (error) {}
  }

  try {
    var registrationSWStatus = await requestSWPermission(options);

    console.log('registartion done, status:', registrationSWStatus)
    checkNotificationPermission();

    return registrationSWStatus;
  } catch (error) {
    console.log('registartion error, status:', error)
    throw error;
  } finally {
    elBackground && removeChildNode(elBackground);
    elOverlay && (
      removeChildNode(elOverlay.overlayElement),
      removeChildNode(elOverlay.overlayStyle),
      removeChildNode(elOverlay.overlayScript)
    );
  }
};

export const registerPush = registerPushFn;
