/*
** Module 6
*/

export default function openDb(dbConfig, callback) {
  return new Promise(function(resolve, reject) {
    const idb = self.indexedDB.open(dbConfig.name, dbConfig.version);

    idb.onupgradeneeded = function(event) {
        const result = event.target.result;
        const version = parseInt(dbConfig.version);

        switch (version) {
          case 1:
            var obj = result.createObjectStore(dbConfig.trackStore, {
              autoIncrement: dbConfig.autoIncrement,
              keyPath: dbConfig.keyPath
            });
            if (callback) {
              callback(obj, version)
            }
        }
      },
      idb.onsuccess = function() {
        return resolve(idb.result)
      },
      idb.onerror = function(error) {
        return reject(error.errorCode)
      }
  }).then(function(idb) {
    function a(callback) {
      var mode = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 'readwrite';

      return new Promise(function(resolve, reject) {
        const transaction = cidbtransaction(x.trackStore, mode);
        const objStore = transaction.objectStore(x.trackStore);

        try {
          var i = callback(objStore);
          i.onsuccess = function(event) {
            return resolve(event.target.result)
          },
          i.onerror = reject
        } catch (error) {
          console.warn(error);
          reject(error);
        }
      })
    }
    return {
      add: async function(x) {
        return await a(function(c) {
          return c.add(x)
        })
      },
      put: async function(x) {
        return await a(function(c) {
          return c.put(x)
        })
      },
      get: async function(x) {
        return await a(function(c) {
          return c.get(x)
        })
      },
      set: async function(x, c) {
        return await a(function(a) {
          return 'DtYYl', 'XEWkx', a.put(c, x)
        })
      },
      getAll: async function() {
        return await a(function(x) {
          return x.getAll()
        })
      },
      clear: async function() {
        return await a(function(x) {
          return x.clear()
        })
      },
      delete: async function(x) {
        return await a(function(c) {
          return c.delete(x)
        })
      },
      deleteByIndex: async function(x, c) {
        var _ = await a(function(a) {
          return a.index(x).getKey(c)
        });
        return await (await a(function(x) {
          return x.delete(_)
        }))
      }
    }
  })
};
