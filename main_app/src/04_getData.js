/*
** Module 4
*/

import props from './00_options';
import parseErrorMessage from './07_parseErrorMessage';
import constants from './09_constants';
import {default as jsonRestFn} from './14_fetchJson';

async function getData(url, message, error, context, fallbackFlag) {
	var errorMessage = parseErrorMessage(error, context),
		location = location && location.href ? location.href : 'unknown',
		user_key = typeof(context) === 'object' && typeof(context.user_key) === 'object' ? context.user_key : {},
		trace_id = typeof(context) === 'object' && typeof(context.trace_id) === 'string' ? context.trace_id : "";

	return jsonRestFn(
		url + '/event',
		{
			code: constants.ERROR_REPORT,
			sw_version: props.swVersion,
			user_key: user_key,
			error_message: message + ',  message: ' + errorMessage.message + ', fallback: ' + String(fallbackFlag),
			error_stack: errorMessage.stack,
			error_location: location,
			trace_id: trace_id
		}
	).then(function() {
		return true
	})
}

export default async function(url, message, error, context) {
	try {
		return await getData(String(url), message, error, context, false)
	} catch (error) {
		console.warn(error);

		try {
			return await getData(props.swFallbackErrorDomain, message, a, context, true);
		} catch (error) {
			console.warn('sendFallbackError: ' + error);
			return false;
		}
	}
};
