/*
** Module 3
*/

import generateOaidTraceId from './26_generateOaidTraceId'; // t

let executionHandlers = {};

const executeEventAndRemoveHandler = function(x) {
  return function() {
    Promise.resolve().then(function() {
      if (executionHandlers[x] instanceof Function)
        try {
          executionHandlers[x]();
        } catch (error) {
          console.warn(error);
        } finally {
          delete executionHandlers[x];
        }
    })
  }
};

const registerEvent = function(x) {
  return function(c) {
    executionHandlers[x] = c;
  }
};

let runner = null;
let options = null;
let d = self.document && document.currentScript
  ? document.currentScript.dataset
  : { sdk: 'ntfcSDK' };

const oSDKCall = {
  beforePermissionPrompt: executeEventAndRemoveHandler('beforePermissionPrompt'),
  permissionDefault: executeEventAndRemoveHandler('permissionDefault'),
  permissionAllowed: executeEventAndRemoveHandler('permissionAllowed'),
  permissionDenied: executeEventAndRemoveHandler('permissionDenied'),
  alreadySubscribed: executeEventAndRemoveHandler('alreadySubscribed'),
  notificationUnsupported: executeEventAndRemoveHandler('notificationUnsupported'),
  requestPermissionHandlerDone: executeEventAndRemoveHandler('requestPermissionHandlerDone'),
  setRunner: function(newRunner) {
    runner = newRunner;
  },
  loadOptions: function(url) {
    return fetch(url).then(function(data) {
      return data.json();
    }).then(function(options) {
      oSDKCall.overrideOptions(options);
      return options;
    })
  },
  overrideOptions: function(newOptions) {
    options = newOptions;
  }
};

const oSDK = {
  onBeforePermissionPrompt: registerEvent('beforePermissionPrompt'),
  onPermissionDefault: registerEvent('permissionDefault'),
  onPermissionAllowed: registerEvent('permissionAllowed'),
  onPermissionDenied: registerEvent('permissionDenied'),
  onAlreadySubscribed: registerEvent('alreadySubscribed'),
  onNotificationUnsupported: registerEvent('notificationUnsupported'),
  onRequestPermissionHandlerDone: registerEvent('requestPermissionHandlerDone'),
  runInstall: function(overrideOptions) {
    const c = {
      showCapping: 0,
      useRtMarkUser: true
    };
    const resolvedOptions = overrideOptions || options;

    if (!resolvedOptions)
      throw Error('options is not set');

    if (!(runner instanceof Function))
      throw Error('runner is not set');

    return generateOaidTraceId().then(function(trace) {
      const oaid = trace.oaid;
      const trace_id = trace.trace_id;
      if (runner instanceof Function)
        return runner(Object.assign({}, c, resolvedOptions, {
          oaid: oaid,
          trace_id: trace_id
        }));
    }).finally(function() {
      return runner = null;
    })
  }
};

self[d.sdk || 'ntfcSDK'] = oSDK

export const SDK = oSDK;
export const SDKCall = oSDKCall;
