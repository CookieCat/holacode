/*
** Module 23
*/

export const DESKTOP_BACKGROUND_OPACITY = .3;
export const MOBILE_BACKGROUND_OPACITY = .6;

import {applyStyles} from './20_iFrameHelpers';

export default function createBackground(isMobile) {
  const doc = document;
  const opacity = isMobile ? MOBILE_BACKGROUND_OPACITY : DESKTOP_BACKGROUND_OPACITY;

  if (!doc)
    return null;

  const el = doc.createElement('div');

  applyStyles(el, {
    position: 'absolute',
    top: "0",
    left: "0",
    right: "0",
    bottom: "0",
    background: '#000',
    opacity: opacity,
    zIndex: 9999999,
    overflow: 'hidden'
  });

  return el;
};
