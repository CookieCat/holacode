/*
** Module 22
*/

export default function(str) {
  return typeof(str) === 'string' && str.length < 64 && str.length > 0
}
