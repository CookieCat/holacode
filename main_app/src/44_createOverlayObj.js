/*
** Module 44
*/

export default function(options) {
  var el = document.createElement('div'),
    style = document.createElement('style'),
    script = document.createElement('script');

  script.type = 'text/javascript';
  script.appendChild(document.createTextNode(options.overlayScript || ''));

  style.type = 'text/css';
  style.appendChild(document.createTextNode(options.overlayStyle || ''));

  el.insertAdjacentHTML('afterbegin', options.overlayHtml || '');

  return {
    overlayElement: el,
    overlayStyle: style,
    overlayScript: script
  }
};
