/*
** Module 19
*/

import browserDetection from './11_browserDetection';

let browserDetectedObj = browserDetection(navigator.userAgent);

export default function getBrowserStat() {
  return [
    ["SW", 'window.screen.width'],
    ["SH", 'window.screen.height'],
    ['SAH', 'window.screen.availHeight'],
    ["WX", 'window.screenX'],
    ["WY", 'window.screenY'],
    ["WW", 'window.outerWidth'],
    ["WH", 'window.outerHeight'],
    ['WIW', 'window.innerWidth'],
    ['WIH', 'window.innerHeight'],
    ["CW", 'document.documentElement.clientWidth'],
    ['WFC', 'window.top.frames.length'],
    ["PL", `(typeof document !== 'undefined' ? document.location.href || '' : '')`],
    ['DRF', `(typeof document !== 'undefined' ? document.referrer || '' : '' )`],
    ["NP", `(!(navigator.plugins instanceof PluginArray) || navigator.plugins.length === 0) ? 0 : 1`],
    ["PT", `window.callPhantom !== undefined || window._phantom !== undefined ? 1 : 0`],
    ["NB", `typeof navigator.sendBeacon === 'function' ? 1 : 0`],
    ["NG", `navigator.geolocation !== undefined ? 1 : 0`],
    ["NW", `'webdriver' in navigator ? 1 : 0`],
    ["CF", 'getFlashVersion()']
  ].reduce(function(acc, value) {
    const [prop, val] = value;

    try {
      acc[prop] = eval(val)
    } catch (x) {
      acc[prop] = -1
    }

    return acc;
  }, {
    IM: browserDetectedObj.isMobile ? 1 : 0
  })
};
