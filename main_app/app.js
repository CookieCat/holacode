/*
** Module 59 - app!
*/

import props from './src/00_options'; // o = u(a(0));
import {eventLogger} from './src/01_logger'; // e = a(1),
import getOldOptions from './src/33_getOldOptions'; // n = a(33),
import appWorkContext from './src/34_appWorkContext'; // r = u(a(34)),
import zfgformats from './src/36_zfgformats'; // i = u(a(36)),
import {default as standaloneInstall} from './src/39_standaloneInstall'; // _ = u(a(39)),
import testPushSAndSW from './src/45_testPushSAndSW'; // t = u(a(45)),

function d(x) {
  try {
    window.opener && window.opener.postMessage('popupRunStage#' + x, "*")
  } catch (error) {}
}

d("I");

try {
  if (!window.installOnFly) {
    window.installOnFly = true;

    const oldOptions = getOldOptions();

    if (oldOptions && testPushSAndSW()) {
      zfgformats('pusher-https', props.swVersion, oldOptions.zoneId, document.currentScript);

      eventLogger.setContext(
        oldOptions.domain + '/custom',
        appWorkContext(oldOptions)
      );

      eventLogger.send({
        event_type: 'hit_page',
        installer_type: 'https'
      }, oldOptions.domain);

      standaloneInstall(oldOptions);
    }
  }
} catch (error) {
  d("E");
}
