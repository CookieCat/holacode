var translation = {
	source: {
		title: 'Video',
		text1: 'Click "Allow" button to watch video'
	},
	en: {
		title: 'Video',
		text1: 'Click "Allow" button to watch video'
	},
	ar: {
		title: 'الفيديو',
		text1: 'انقر على زر "اسمح" لمشاهدة الفيديو',
	},
	bg: {
		title: 'Видео клип',
		text1: 'Натисни бутона "Разреши", за да гледаш видео клипа',
	},	
	cs: {
		title: 'Video',
		text1: 'Klikněte na „Povolit“ a podívejte se na video',
	},
	nl: {
		title: 'Video',
		text1: 'Klik op de "Toestaan"-knop om video te bekijken',
	},	
	fr: {
		title: 'Vidéo',
		text1: 'Cliquez sur le bouton « autoriser » pour regarder la vidéo',
	},
	de: {
		title: 'Video',
		text1: 'Klicken Sie auf die Schaltfläche „Erlauben“, um das Video anzusehen.',
	},	
	hu: {
		title: 'Videó',
		text1: 'Kattints az „Engedélyez” gombra a videó megtekintéséhez',
	},
	it: {
		title: 'Video',
		text1: 'Clicca sul pulsante "Consenti" per guardare il video',
	},
	ja: {
		title: '動画',
		text1: '[許可] ボタンをクリックして動画を視聴する',
	},
	ko: {
		title: '비디오',
		text1: '비디오를 보시려면 "허락" 버튼을 클릭하세요',
	},	
	pl: {
		title: 'Film',
		text1: 'Kliknij przycisk „Zezwól”, by obejrzeć film',
	},
	ro: {
		title: 'Videoclip',
		text1: 'Dați clic pe „Permite” pentru a urmări videoclipul',
	},	
	ru: {
		title: 'Видео',
		text1: 'Нажмите "Разрешить" чтобы посмотреть видео',
	},
	sr: {
		title: 'Видео',
		text1: 'Притисните дугме "Дозвољено" како бисте гледали видео',
	},	
	es: {
		title: 'Vídeo',
		text1: 'Haz clic en el botón "Permitir" para ver el vídeo',
	},
	th: {
		title: 'วิดีโอ',
		text1: 'คลิกปุ่ม "อนุญาต" เพื่อชมวิดีโอ',
	},
	uk: {
		title: 'Відео',
		text1: 'Щоб переглянути відео, натисніть кнопку "Дозволити"',

	},
	zh: {
		title: '视频',
		text1: '点击“允许”按钮观看视频',
	},	
	fa: {
		title: 'ویدئو',
		text1: 'برای تماشای ویدئو دکمه "پذیرش" را فشار دهید',
	},
	pt: {
		title: 'Vídeo',
		text1: 'Clique no botão "Permitir" para ver o vídeo',
	},
	no: {
		title: 'Video',
		text1: 'Klikk på "Tillat" -knappen for å se video',
	},
	sk: {
		title: 'Video',
		text1: 'Kliknutím na tlačidlo "Povoliť" môžete sledovať video',
	},	
	el: {
		title: 'βίντεο',
		text1: 'Κάντε κλικ στο κουμπί "Να επιτρέπεται" για να παρακολουθήσετε βίντεο',
	},
	lv: {
		title: 'Video',
		text1: 'Jei norite žiūrėti vaizdo įrašą, spustelėkite mygtuką „Leisti“',
	},
	lt: {
		title: 'Video',
		text1: 'Noklikšķiniet uz pogas "Atļaut", lai skatītu video',
	},
	ee: {
		title: 'Video',
		text1: 'Video vaatamiseks klõpsake nuppu "Luba"',
	},
	hi: {
		title: 'वीडियो',
		text1: 'वीडियो देखने के लिए "अनुमति दें" बटन पर क्लिक करें',
	},	
	my: {
		title: 'Video',
		text1: 'Klik butang "Benarkan" untuk menonton video',
	},
	hr: {
		title: 'Video',
		text1: 'Kliknite gumb "Dopusti" za gledanje videozapisa',
	},
	bn: {
		title: 'ভিডিও',
		text1: 'ভিডিও দেখার জন্য "অনুমতি দিন" বোতামে ক্লিক করুন'
	},	
	tr: {
		title: 'Video',
		text1: 'Videoyu izlemek için "İzin Ver" düğmesine tıkla',
	}
};

var rtlLangs = ['ar', 'fa'];

var browserLang = detect_language();
var siteLang = getParameterByName("siteLang", false);
var extTpl = getParameterByName("extTpl", 1);

/** docReady provides a method of scheduling one or more javascript functions to run at some later point when the DOM has finished loading. */
!function(t,e){"use strict";function n(){if(!a){a=!0;for(var t=0;t<o.length;t++)o[t].fn.call(window,o[t].ctx);o=[]}}function d(){"complete"===document.readyState&&n()}t=t||"docReady",e=e||window;var o=[],a=!1,c=!1;e[t]=function(t,e){return a?void setTimeout(function(){t(e)},1):(o.push({fn:t,ctx:e}),void("complete"===document.readyState||!document.attachEvent&&"interactive"===document.readyState?setTimeout(n,1):c||(document.addEventListener?(document.addEventListener("DOMContentLoaded",n,!1),window.addEventListener("load",n,!1)):(document.attachEvent("onreadystatechange",d),window.attachEvent("onload",n)),c=!0)))}}("docReady",window);

function detect_language() {
	var userLang = navigator.languages && navigator.languages[0] || navigator.language || navigator.userLanguage;
	if (userLang == "zh-CN" || userLang == "zh-SG" || userLang == "zh-MY" || userLang == "zh-CHS") {
		userLang = "zh-Hans";
	} else if (userLang == "zh-HK" || userLang == "zh-MO" || userLang == "zh-TW" || userLang == "zh-CHT") {
		userLang = "zh-Hant";
	} else if (userLang.length > 2) {
		userLang = userLang[0] + userLang[1];
	}
	return userLang;
}

function replace_text(lang, text, objTrls) {
	var y = document.querySelectorAll("#"+text);

	if (y.length > 0) {
		for (var j=0; j < y.length; j++) {

			if(y[j].placeholder != undefined) {
				y[j].placeholder = objTrls[lang][text];
			} else {
				y[j].innerHTML = objTrls[lang][text];
			}
		}
	}  else {
		console.log("element not Found: " + text);
	}
}

function translation_available(objTrls, lang) {
	if (objTrls[lang]) {
		return lang;
	} else {
		console.log("Translation not Found: " + lang + ". Used default");
		return "source";
	}
}

function translate(siteLang, objTrls) {
	var lang = siteLang ? siteLang : browserLang;
	var availableLang = translation_available(objTrls, lang);

	var body = document.getElementsByTagName('body')[0];
	body.classList.add(lang);
	body.classList.add('style' + extTpl);

	if(rtlLangs.indexOf(lang) !== -1) {
		body.classList.add('rtl');
		body.style.direction = 'rtl';
	}

	if (availableLang) {
		for (var x in objTrls["source"]) {
			replace_text(availableLang, x, objTrls);
		}
	}
}

function getParameterByName(name, defaultValue) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search),
		value = defaultValue ? defaultValue : "";
	return results === null ? value : decodeURIComponent(results[1].replace(/\+/g, " "));
}

docReady(function() {
	translate(siteLang, translation);
});